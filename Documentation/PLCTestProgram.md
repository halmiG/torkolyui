# Változók
Helyük:

`Revo/PLC/PLC1/PLC1 Project/ GVL/ GVL_HOIST`
## stHoistPlc
Ezt figyelem. Ezeket lehet olvasni.

| Változó    | Mit tartalmaz                       | Type            |
|------------|------------------------------------:|-----------------|
| ActPos     | az aktuális pozciót mutatja, mm-ben | REAL            |
| ActVel     | az aktuális  sebesség               | REAL            |
| ActState   | aktuális állapot, jelzőbitek        | DWORD           |
| ActGroup   | aktuális beválasztott csoport       | INT             |
| EndPosUp   | véghelyzet fent                     | BOOL (jelzőbit) |
| EndPosDown | véghelyzet lent                     | BOOL (jelzőbit) |
| bHOK       | hajtás OK                           | BOOL (jelzőbit) |
| bVA        | végállás                            | BOOL (jelzőbit) | 
| bTH        | túlterhelés                         | BOOL (jelzőbit) |
| bFNY       | fék nyitva                          | BOOL (jelzőbit) |  
| bMozog     | mozog                               | BOOL (jelzőbit) |
| bKL        | kötél laza                          | BOOL (jelzőbit) |
| bMF        | a gép mehet fel                     | BOOL (jelzőbit) |
| bML        | a gép mehet le                      | BOOL (jelzőbit) |

### ActState, jelzőbitek sorrend
	EndPosUp	:	BIT;	//0
	EndPosDown	: 	BIT;	//1
	bHOK 		: 	BIT;	//2
	bVA 		: 	BIT;	//3
	bTH 		: 	BIT;	//4
	bFNY 		: 	BIT;	//5
	bMozog 		:	BIT;	//6
	bKL 		: 	BIT;	//7
	bMF 		: 	BIT;	//8
	bML 		: 	BIT;	//9
## stHoistOpc
Ebbe lehet írni az értékeket.

| Változó    | Mit tartalmaz                       | Type            |
|------------|------------------------------------:|-----------------|
| DestPos    | cél pozciót, mm-ben                 | REAL            |
| DestVel    | cél sebesség (egyelőre nem működik) | REAL            | 
| DestGroup  | cél csoport                         | REAL            | 


