const electron = require("electron");
const { ipcMain } = require("electron");
const ads = require("./tools/ADS/adsHandlers");
const PlayAPI = require("./ipcHandlers/PlayAPI");
const GroupAPI = require("./ipcHandlers/groupAPI");
const DeadAPI = require("./ipcHandlers/deadAPI");
const ServiceAPI = require("./ipcHandlers/serviceAPI");


//type in the consol to enable devtron: 
//require('devtron').install()

const { app, BrowserWindow } = electron;

let mainWindow;

app.on("ready", () => {
    mainWindow = new BrowserWindow({ show: false, width: 1920, height: 1080 });
    mainWindow.loadURL(`file://${__dirname}/src/index.html`); // How would this look like on Windows ??? SAME! so it is not an open question anymore I think
    //mainWindow.loadURL(`file://${__dirname}/dist/index.html   `); // How would this look like on Windows ???
    console.log("app ready");
    mainWindow.once("ready-to-show", () => {
        console.log("app si ready to show!!!!!!!!!");
        mainWindow.show();
    });
    // ad React dev tool extenison
    BrowserWindow.addDevToolsExtension("/Users/gergohalmi/Library/Application Support/Google/Chrome/Default/Extensions/fmkadmapgofadopljbjfkapdkoienihi/3.3.1_0");
    BrowserWindow.addDevToolsExtension("/Users/gergohalmi/Library/Application Support/Google/Chrome/Default/Extensions/lmhkpmbekcpmknklioeibfkpmmfibljd/2.15.3_0");

    let playAPI = new PlayAPI(mainWindow);
    playAPI.setListeners();
    let groupAPI = new GroupAPI(mainWindow);
    groupAPI.setListeners();
    let deadAPI = new DeadAPI(mainWindow);
    deadAPI.setListeners();
    let serviceAPI = new ServiceAPI(mainWindow);
    serviceAPI.setListeners();
});


let globalRequester = null; // this the ZeroMQ requester instance
ipcMain.on("start-ads", event => {
    let { requester, posSubscriber, signBitSubsriber } = ads.startADS(mainWindow);
    globalRequester = requester; // make it global to be able to use in other event handlers
    ipcMain.on("stop-ads", event => {
        ads.stopADS(mainWindow, globalRequester, posSubscriber, signBitSubsriber);
        ipcMain.removeAllListeners("stop-ads");
    });
});

/*
PLAY and SCENE HANDLERS
*/



ipcMain.on("choose-into-group", (event, hoist, groupID) => {
    //NOTE this is temorprary just to bypass the PLC answer
    mainWindow.send(`hoist${hoist}-into-group`, 0)
    //ads.chooseInHoistToGroup(globalRequester, hoist, groupID, resp => {
    //     mainWindow.send(`hoist${hoist}-into-group`, resp);
    // });
});

