let webpack = require("webpack");
let path = require("path");
let fs = require("fs");
let HtmlWebpackPlugin = require("html-webpack-plugin");
let ExtractTextPlugin = require("extract-text-webpack-plugin");
var EncodingPlugin = require("webpack-encoding-plugin");
const UglifyJsPlugin = require("uglifyjs-webpack-plugin");

let nodeModules = {};
fs.readdirSync("node_modules")
  .filter(function (x) {
    return [".bin"].indexOf(x) === -1;
  })
  .forEach(function (mod) {
    nodeModules[mod] = "commonjs " + mod;
  });

const GLOBALS = {
  "process.env.NODE_ENV": JSON.stringify("production")
};

module.exports = {
  bail: true, // dont try to continue if there is an error here
  mode: "production",
  externals: {
    nodeModules,
    // "react": "React",
    // "react-dom": "ReactDOM"
  },
  devtool: "source-map",
  entry: [
    "./src/index.tsx"
  ],
  target: "electron-renderer",
  output: {
    path: __dirname + "/dist",
    publicPath: "/",
    filename: "bundle.js"
  },
  module: {
    rules: [
      { test: /\.(ts|tsx)$/, loader: "ts-loader" },
      {
        test: /\.html$/,
        use: [{
          loader: 'html-loader',
          options: {
            minimize: true
          }
        }],
      },
      { enforce: "pre", test: /\.js$/, loader: "source-map-loader" },
      // { test: /\js$/, include: path.join(__dirname, "src"), loader: "babel-loader"},
      // { test: /\jsx$/, include: path.join(__dirname, "src"), loader: "babel-loader"},
      {
        test: /\.(woff|woff2|eot|ttf|svg)$/,
        loader: 'file?name=fonts/[name].[ext]'
      },
      {
        test: /\.css$/, include: [path.join(__dirname, "src"), path.resolve(__dirname, 'node_modules/typeface-robot')], use: ExtractTextPlugin.extract({
          fallback: "style-loader",
          use: [
            {
              loader: "css-loader",
              options: {
                importLoaders: 1,
                modules: true,
                localIdentName: "[name]_[local]_[hash:base64:5]"
              }
            },
            "postcss-loader"
          ]
        })
      }
      //   { test: /\.css$/, include: path.join(__dirname, 'src'), loaders: ['style-loader','css-loader']}
    ]
  },
  resolve: {
    extensions: [".js", ".jsx", ".ts", ".tsx", ".json"]
  },
  devServer: {
    contentBase: "./dist"
  },
  plugins: [
    // eza plugin már eltűnt elvileg  
    //new webpack.optimize.OccurrenceOrderPlugin(),
    new UglifyJsPlugin({
      cache: true,
      parallel: true,
      sourceMap: true
    }),
    new webpack.DefinePlugin(GLOBALS),
    new HtmlWebpackPlugin(),
    new ExtractTextPlugin("styles.css"),
    new EncodingPlugin({
      encoding: "iso-8859-1"
    })
    //new webpack.optimize.UglifyJsPlugin() // hve Eroor with it 
  ]
};