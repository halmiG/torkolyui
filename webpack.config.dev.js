let webpack = require("webpack");
let path = require("path");
let fs = require("fs");

let nodeModules = {};
fs.readdirSync("node_modules")
  .filter(function (x) {
    return [".bin"].indexOf(x) === -1;
  })
  .forEach(function (mod) {
    nodeModules[mod] = "commonjs " + mod;
  });

module.exports = {
  mode: "development",
  externals: {
    nodeModules,
    //TODO itt gond van?  mia csöcsért van ez itt akkor? 
    //  "react": "React",
    //  "react-dom": "ReactDOM"
  },
  devtool: "cheap-module-eval-source-map",
  entry: [
    "./src/index.tsx"
  ],
  target: "electron-renderer",
  output: {
    path: __dirname,
    publicPath: "/",
    filename: "bundle.js"
  },
  module: {
    rules: [
      { test: /\.(ts|tsx)$/, loader: "ts-loader" },
      {
        test: /\.(woff|woff2|eot|ttf|svg)$/,
        loader: 'file?name=fonts/[name].[ext]'
      },

      // { test: /\js$/, include: path.join(__dirname, "src"), loader: "babel-loader"},
      // { test: /\jsx$/, include: path.join(__dirname, "src"), loader: "babel-loader"},
      {
        test: /\.css$/, include: [path.join(__dirname, "src"), path.join(__dirname, "node_modules/typeface-robot")], loaders: ["style-loader", {
          loader: "css-loader",
          options: {
            importLoaders: 1,
            modules: true,
            localIdentName: "[name]_[local]_[hash:base64:5]"
          }
        },
          "postcss-loader"]
      },
      { enforce: "pre", test: /\.js$/, loader: "source-map-loader" },
    ]
  },
  resolve: {
    extensions: [".js", ".jsx", ".ts", ".tsx", ".json"]
  },
  devServer: {
    historyApiFallback: true,
    contentBase: "./",
    port: 4172
  }
};