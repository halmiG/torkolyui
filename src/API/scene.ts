import { ipcRenderer } from "electron";
import { SceneConfig } from "../components/pages/play/components/sceneArea/models/PlayConfig";

export const getNumberOfScenes = () => {
    return new Promise((resolve, reject) => {
        ipcRenderer.send("get-scene-number");
        ipcRenderer.once("get-scene-number-result", (event, error, sceneNumber) => {
            if (error) {
                reject(error);
            } else {
                resolve(sceneNumber);
            }
        });
    });
}

export const getAllSceneData = () => {
    return new Promise((resolve, reject) => {
        ipcRenderer.send("get-all-scene-data");
        ipcRenderer.once("get-all-scene-data-result", (event, error, allSceneData: Array<SceneConfig>) => {
            if (error) {
                reject(error);
            } else {
                resolve(allSceneData);
            }
        });
    });
}

export const saveAllSceneData = (allSceneData: Array<SceneConfig>) => {
    return new Promise((resolve, reject) => {
        ipcRenderer.send("save-all-scene-data", allSceneData);
        ipcRenderer.once("save-all-scene-data-return", (event, error) => {
            if (error) {
                reject(error);
            } else {
                resolve();
            }
        });
    });
}