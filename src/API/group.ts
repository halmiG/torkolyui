import { ipcRenderer } from "electron";

export const setMovingJoy = (sceneIndex: number, groupIndex: number, joystick: number) => {
    return new Promise((resolve, reject) => {
        ipcRenderer.send("set-joystick-group", sceneIndex, groupIndex, joystick);
        ipcRenderer.once("set-joystick-group-resp", (event, error) => {
            if (error) {
                reject(error);
            } else {
                resolve();
            }
        });
    });
}