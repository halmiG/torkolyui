import { ipcRenderer } from "electron";
import { HoistConfig } from "../components/pages/play/components/sceneArea/models/PlayConfig";

export const listPlays = () => {
    return new Promise((resolve, reject) => {
        ipcRenderer.send("list-plays");
        ipcRenderer.once("list-plays-result", (event, error, list) => {
            if (error) {
                reject(error);
            } else {
                resolve(list);
            }
        });
    });
};

export const createPlay = (playName: string) => {
    console.log("create play:", playName)
    return new Promise((resolve, reject) => {
        ipcRenderer.send("add-play", playName);
        ipcRenderer.once("add-play-result", (event, error) => {
            if (error) {
                reject(error);
            } else {
                resolve();
            }
        });
    });
};

export const setPlay = (playName: string) => {
    return new Promise((resolve, reject) => {
        //TODO finish it right
        //NOTE szükség van egyáltalán erre? mert valahol mséhol beolasoma cachebe így is a playt
        resolve();
    });
}

export const deletePlay = (playName: string) => {
    return new Promise((resolve, reject) => {
        ipcRenderer.send("delete-play", playName);
        ipcRenderer.once("delete-play-result", (event, error) => {
            if (error) {
                reject(error);
            } else {
                resolve();
            }
        });
    });
}

export const setEndTargets = (hoistWithTarget: HoistConfig) => {
    return new Promise((resolve, reject) => {
        console.log("CALL")
        ipcRenderer.send("set-hoist-targets", hoistWithTarget);
        ipcRenderer.once("set-hoist-targets-result", (event, error) => {
            if (error) {
                reject(error);
            } else {
                resolve();
            }
        });
    });
}

export const getEndTargets = () => {
    return new Promise((resolve, reject) => {
        ipcRenderer.send("get-end-targets");
        ipcRenderer.once("get-end-targets-result", (event, error, endTargets: Array<HoistConfig>) => {
            if (error) {
                reject(error);
            } else {
                resolve(endTargets);
            }
        });
    });
}

export const clearPlayCache = () => {
    ipcRenderer.send("clear-cache");
}