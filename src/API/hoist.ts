import { ipcRenderer } from "electron";

export const setMovingJoy = (hoistID, joystick) => {
    return new Promise((resolve, reject) => {
        ipcRenderer.send("set-joystick", hoistID, joystick);
        ipcRenderer.once("set-joystick", (event, error) => {
            if (error) {
                reject(error);
            } else {
                resolve();
            }
        });
    });
}