import * as React from "react";
import { Component } from 'react';
// import { Link } from "react-router-dom";

interface State {
  children?: any
}

export default class App extends Component<any, State> {

  render() {
    return (
      <div>
        {/* <Link to="/">
          <h1>AppHeader</h1>
        </Link> */}
        {this.props.children}
      </div>
    );
  }
}