import * as React from "react";
import { MenuItemModel } from "./DDMenu";
import Icon from "../Icon/Icon";
import { ICONS } from "../Icon/iconList";
const style = require("./joyListDDContent.css");

const joyListDDContent = (onJoySelected) => {
    const content: Array<MenuItemModel> = [
        {
            content: (
                <div className={style.defaultJoy}>
                    <Icon icon={ICONS.JOYSTICK.d} fill={ICONS.JOYSTICK.fill} size={30} />
                </div>
            ),
            onClickHandler: onJoySelected(0)
        },
        {
            content: (
                <div className={style.redJoy} >
                    <Icon icon={ICONS.JOYSTICK.d} fill={ICONS.JOYSTICK.fill} size={30} />
                </div>
            ),
            onClickHandler: onJoySelected(1)
        },
        {
            content: (
                <div className={style.greenJoy} >
                    <Icon icon={ICONS.JOYSTICK.d} fill={ICONS.JOYSTICK.fill} size={30} />
                </div>
            ),
            onClickHandler: onJoySelected(2)
        },
        {
            content: (
                <div className={style.blueJoy}>
                    <Icon icon={ICONS.JOYSTICK.d} fill={ICONS.JOYSTICK.fill} size={30} />
                </div>
            ),
            onClickHandler: onJoySelected(3)
        },
        {
            content: (
                <div className={style.yellowJoy}>
                    <Icon icon={ICONS.JOYSTICK.d} fill={ICONS.JOYSTICK.fill} size={30} />
                </div>
            ),
            onClickHandler: onJoySelected(4)
        }
    ]
    return content;
}

export default joyListDDContent;