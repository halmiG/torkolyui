import * as React from "react";
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';

export interface MenuItemModel {
    content: any
    onClickHandler: (event) => void
}

interface Props {
    open: boolean
    title?: string
    onClose: () => void
    anchorEl: any
    menuItems: Array<MenuItemModel>
    selected?: any
}

const DDMenu = (props: Props) => {

    const { open, title, onClose, anchorEl, menuItems, selected } = props;

    return (
        <div>
            <Menu
                open={open}
                title={title}
                onClose={onClose}
                anchorEl={anchorEl}
            >
                {menuItems.map((item, i) => (
                    <MenuItem key={i} onClick={item.onClickHandler} selected={selected === i} >
                        {item.content}
                    </MenuItem>
                ))}
            </Menu>
        </div>
    )
}

export default DDMenu;