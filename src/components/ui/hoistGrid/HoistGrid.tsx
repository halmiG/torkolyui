import * as React from "react";
import Hoist from "../Hoist/hoist";
const style = require("./HoistGrid.css");


interface Props {
}

const HoistGrid = (props: Props) => {
    const numberOfHoists = 38

    const renderHoists = () => {
        let hoists = []

        for (let i = 1; i < numberOfHoists + 1; i++) {
            hoists.push(<li key={i} className={style.hoistElements} ><Hoist name={i} place={"page"} /></li>)
        }
        return hoists;
    }

    return (
        <ul className={style.hoistContainer}>
            {renderHoists()}
        </ul>
    )
}

export default HoistGrid;