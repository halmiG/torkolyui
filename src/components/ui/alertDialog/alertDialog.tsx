import * as React from "react";
import { Component } from 'react';
import FlatButton from "material-ui/FlatButton";
import Dialog from "material-ui/Dialog";

interface Props {
    open: boolean
    handleClose: any
}

interface State {
}

export default class AlertDialog extends Component<Props, State> {
    constructor(props: Props) {
        super(props);

        this.state = {
        }
    } 

    render() {
        const action = <FlatButton label="Vettem!" onClick={this.props.handleClose}/>
        return(
            <div>
                <Dialog
                    actions={action}
                    modal={false}
                    open={this.props.open}
                    onRequestClose={this.props.handleClose}
                >
                Action is permitted!
                </Dialog>
            </div>
        )
    }
}