import * as React from "react";
import { Component } from 'react';
//import style from './DataPage.css'; 
const style = require("./positionBar.css")

interface Props {
  actValue: number
  destPos?: number
}

interface State {
}

export default class PositionBar extends Component<Props, State> {
  constructor(props: Props) {
    super(props);

    this.state = {

    }
  }

  render() {
    const {actValue, destPos}= this.props

    return (
        <div className={style.base}>
            <div className={style.bar} style={{height: actValue}} />
            {destPos &&  <div className={style.destMark} style={{bottom: destPos}} />}
        </div>
    );
  }
}