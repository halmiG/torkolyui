import * as React from "react";
import { Component } from 'react';
//import style from './DataPage.css'; 
const style = require("./hoistAlarm.css")

interface Props {
  severity: number
  name: string
}

interface State {
}

export default class HoistAlarm extends Component<Props, State> {
  constructor(props: Props) {
    super(props);

    this.state = {

    }
  }

  defineStyle = (severity) => {
      if(severity === 1) {
        return style.warnRectangle;
      } else if(severity === 2) {
        return style.errRectangle;
      } else {
        return style.fatalRectangle;
      }
  }

  render() {
    const {name, severity} = this.props;
    let currentStyle = this.defineStyle(severity);
    return (
        <div className={currentStyle} >
          <p className={style.name} >{name}</p>
        </div>
    );
  }
}