import * as React from "react";
import { Component } from 'react';
import connect from "react-redux/lib/connect/connect";
import HoistAlarm from "./alarm/hoistAlarm";


interface Props {
    name: number,
    signBits?: string
}

interface State {
    signs: Array<number>
}


class StatusSigns extends Component<Props, State> {
    constructor(props: Props) {
        super(props);

        this.state = {
            signs: []
        }
    }

    static getDerivedStateFromProps(nextProps, prevState) {
        // if the sign array was empty, it could be possible during first render

        if(!nextProps.signBits){
            return 0;
        } else {
            let localSigns = []
            const flags = [
                1, 
                2,
                4, 
                8, 
                16, 
                32,
                64,
                124
            ];
        
            // mask the bits
            for(let i= 0; i < 8; i++) {
                localSigns[i] = flags[i] & Number(nextProps.signBits);
            }
            return { signs: localSigns};
        }
    }

    render() {
        const {signs} = this.state;
        return (
            <div>
                {signs[0] ? <HoistAlarm  severity={1} name={"HG"} /> : ""}
                {signs[1] ? <HoistAlarm  severity={2} name={"BJ"} /> : ""}
                {signs[2] ? <HoistAlarm  severity={3} name={"TJ"} /> : ""}
            </div>
        )
    }
}

function mapStateToProps(state, ownProps) {
    return {
        signBits: state.adsReducer.signBits[ownProps.name]
    }
}

export default connect(mapStateToProps)(StatusSigns);