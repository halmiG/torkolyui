import * as React from "react";
//import { Component } from 'react';
import connect from "react-redux/lib/connect/connect";
const style = require("../hoist.css");

const PositionText = ({ name, actPosValue }) => (
    <p className={style.actPos}>{actPosValue !== undefined ? actPosValue : ""}</p>
)

function mapStateToProps(state, ownProps) {
    return {
        actPosValue: state.adsReducer.actPosValues[ownProps.name]
    }
}

export default connect(mapStateToProps)(PositionText);