import * as React from "react";
import connect from "react-redux/lib/connect/connect"
import { createSelector } from "reselect";
import { generateColor } from "../../../utils/general";
import { HoistConfig, SceneConfig } from "../../pages/play/components/sceneArea/models/PlayConfig";

const style = require("./hoistWrapper.css");

interface BorderInfoFromGroup {
    inGroup: boolean
    groupColor: any
}

interface Props {
    children?: any
    groupSettingsMode: boolean
    hoistFromGroupDesc?: Array<HoistConfig> // in group settings shows the hoists in the selected group
    addHoistModeGroupID: string
    borderInfoFromGroups?: BorderInfoFromGroup
    name: number
    onClick: () => void
}

interface State {
    inGroup: boolean
    borderStyle: any
}

class HoistWrapper extends React.Component<Props, State> {
    constructor(props: Props) {
        super(props);

        this.state = {
            inGroup: false,
            borderStyle: style.border
        }
    }

    static getDerivedStateFromProps(nextProps: Props, prevState: State) {
        // this means that we are in GroupSettings, becasue this only exists there
        if (nextProps.groupSettingsMode) {
            if (nextProps.hoistFromGroupDesc.find(hoist => hoist.id === nextProps.name)) {
                return {
                    inGroup: true,
                    borderStyle: style.inGroup,
                    chosen: true
                };
            } else if (!nextProps.addHoistModeGroupID) { // we also shouldnt be in add hoist mode in groupSettings
                return {
                    inGroup: false,
                    borderStyle: style.border
                }
            } else { // if there is not in the group desc, we just want the border to be normal
                return {
                    borderStyle: style.border
                }
            }
            // this means that we are in the sceneEditor, becasue thats where we have the scene description loaded
        } else if (nextProps.borderInfoFromGroups.inGroup) {
            //TODO  add scene mode here, parabably there must also see the joy color
            return {
                inGroup: true,
                borderStyle: style.inGroup
            }
        } else {
            style.borderSize = 5
            return {
                inGroup: false,
                borderStyle: style.border
            }
        }
    }

    render() {
        const { borderStyle } = this.state;
        const { onClick } = this.props;

        return (
            <div className={borderStyle} onClick={onClick}>
                {...this.props.children}
            </div>
        )
    }
}

const getAllScenesData = (store): Array<SceneConfig> => store.playDataReducer.allSceneData;
const getCurrentScene = (store): number => store.playDataReducer.currentScene;

const makeGetBorderInfoFromGroups = () => {
    return createSelector(
        [getAllScenesData, getCurrentScene, (state, currentName) => currentName],
        (allSceneData, currentScene, currentName): BorderInfoFromGroup => {
            let groups = allSceneData.length ? allSceneData[currentScene].groups : [];
            let indexOfGroup = null;
            groups.find((group, index) => {
                if (group.hoists.find(hoist => hoist.id === currentName)) {
                    indexOfGroup = index;
                    return true;
                } else {
                    return false;
                }
            });
            // if we found the group generate a color to it
            if (indexOfGroup != null) {
                return {
                    inGroup: true,
                    groupColor: generateColor(indexOfGroup)
                }
            } else {
                return {
                    inGroup: false,
                    groupColor: null
                }
            }
        }
    )
}

const makeMapToProps = () => {
    const getBorderInfoFromGroups = makeGetBorderInfoFromGroups();
    const mapStateToProps = (state, ownProps) => {
        return {
            borderInfoFromGroups: getBorderInfoFromGroups(state, ownProps.name),
            hoistFromGroupDesc: state.groupDescReducer.groupDescBuffer.hoists
        }
    }
    return mapStateToProps;
}

export default connect(makeMapToProps)(HoistWrapper);