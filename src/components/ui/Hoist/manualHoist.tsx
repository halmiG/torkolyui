import * as React from "react";
import { Component } from 'react';
import { ipcRenderer } from "electron";
import connect from "react-redux/lib/connect/connect"
import Hoist from "./hoist"
//import style from './DataPage.css'; 

interface Props {
    dispatch?: any
    name: number
    connected: boolean
}

interface State {
    value: string
    destPos: number
    gettingData: boolean
    actPos: number
}

@connect((store) => {
    return {
    }
})

export default class ManualHoist extends Component<Props, State> {
    constructor(props: Props) {
        super(props);

        this.state = {
            value: "",
            destPos: null,
            gettingData: false,
            actPos: null
        }
    }

    componentDidMount () {
        //read dest pos
        if(!this.state.destPos && this.props.connected) {
            let {name} = this.props
            let readId = `DestPos${name}`
            ipcRenderer.send("opc-read-node", `ns=4;s=GVL_HOIST.stHoistOpc[${name}].DestPos`, readId);
            ipcRenderer.once(`opc-read-return-${readId}`, (event, error, result) => {
                if(!error) {
                    this.setState({destPos: result.value.value})
                    this.readActPos();
                } else {
                    console.log("couldn't read the dest pos of", name, "error:", error)
                }
            });
        }
    }

    readActPos = () => {
        let {gettingData} = this.state
        let {name} = this.props
        let readId = `ActPos${name}`
        setTimeout(() => {
            if(!gettingData) {
                this.setState({gettingData: true});
                ipcRenderer.send("opc-read-node", `ns=4;s=GVL_HOIST.stHoistPlc[${name}].ActPos`, readId)
                ipcRenderer.once(`opc-read-return-${readId}`, (event, error, result) => {
                    if(!error) {
                        this.setState({actPos: result.value.value, gettingData: false})
                        ///console.log("actPios: ", result.value.value)
                    } else {
                        console.log("couldn't read the dest pos of", name, "error:", error)
                    }
                })
            }
            this.readActPos();
        }, 300)
    }

    handleChange = (event) => {
        this.setState({ value: event.target.value });
    }

    handleSubmit = (event) => {
        let {name} = this.props
        let writeId = `wActPos${name}`
        let nodeToWrite = `ns=4;s=GVL_HOIST.stHoistOpc[${name}].DestPos`;
        let valueToWrite = {dataType: "Float" ,value: this.state.value};
        console.log('A name was submitted:  ', this.state.value);
        event.preventDefault();
        console.log("write: ", nodeToWrite, valueToWrite)
        ipcRenderer.send("opc-write-node", nodeToWrite, valueToWrite, writeId);
        ipcRenderer.on(`opc-write-return-${writeId}`, (event, error, result) => {
            if(!error) {
                let stateObj = Object.assign({}, this.state)
                this.setState({destPos: Number(stateObj.value)})
                console.log("write was succesfull", result)
            } else {
                console.log("error during the writing of the dest postion", error)
            }
        })
    }

    render() {
        const { destPos, actPos } = this.state;
        return (
            <div>
                <Hoist name={this.props.name} destPos={destPos} actPos={actPos} />
                <form onSubmit={this.handleSubmit}>
                    <label>
                        destPos:
                <input type="text" value={this.state.value} onChange={this.handleChange} />
                    </label>
                    <input type="submit" value="Submit" />
                </form>
            </div>

        );
    }
}