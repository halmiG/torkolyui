import * as React from "react";
import { Component } from 'react';
import connect from "react-redux/lib/connect/connect"
import StatusSigns from "./statusSigns/statusSigns";
import PositionBar from "./positionBar/positionBar";
import PositionText from "./positionText/positionText";
import * as groupDescActions from "../../../actions/groupDescActions";
import AlertDialog from "../alertDialog/alertDialog";
import { createSelector } from "reselect";
import { ipcRenderer } from "electron";
import { TargetConfig, HoistConfig, SceneConfig } from "../../pages/play/components/sceneArea/models/PlayConfig";
import HoistWrapper from "./hoistWrapper";
let style = require("./hoist.css")

interface TargetValues {
    EPT?: TargetConfig
    EPB?: TargetConfig
}

interface Props {
    name: number // name of the hoist, main displayed number
    dispatch?: any
    destPos?: number
    addHoistModeGroupID?: string // Ez határpzza meg melyik csoportba van hoist hozzáadás módban
    endTargets?: TargetValues
    groupSettingsMode?: boolean
}

interface State {
    chosen: boolean
    chooseError: boolean
}


class Hoist extends Component<Props, State> {
    constructor(props: Props) {
        super(props);

        this.state = {
            chosen: false,
            chooseError: false,
        }
    }

    addRemoveHoistToGroupIPC = (groupID: string) => {
        const { name, addHoistModeGroupID } = this.props;
        ipcRenderer.send("choose-into-group", name, groupID);
        ipcRenderer.once(`hoist${name}-into-group`, (event, error) => {
            if (!error) {
                this.setState({
                    chosen: groupID ? true : false
                    // borderStyle: groupID ? style.borderWideChosen : style.border
                });
                // if there is a groupID, it means we choose in, otherwise take out
                if (groupID !== null) {
                    this.props.dispatch(groupDescActions.addHoistToGroup(name));
                } else {
                    this.props.dispatch(groupDescActions.removeHoistFromGroup(name, addHoistModeGroupID));
                }
            } else {
                console.error("error in the chsoe!!!")
                this.setState({ chooseError: true });
            }
        });
    }

    onHoistClicked = () => {
        const { chosen } = this.state;
        const { addHoistModeGroupID } = this.props;
        // if (addHoistModeGroupID && !inGroup) { // if we are in edit mode, and the hoist is not already ina group
        // if we are in edit more, so in GroupSettings
        if (addHoistModeGroupID) {
            if (chosen) { // if it was chosen already
                this.addRemoveHoistToGroupIPC(null);
            } else {
                this.addRemoveHoistToGroupIPC(addHoistModeGroupID);
            }
        }
        // }
    }

    onChooseErrorConfirmed = () => {
        this.setState({ chooseError: false });
    }

    render() {
        const { name, destPos, endTargets, groupSettingsMode, addHoistModeGroupID } = this.props;
        const { chooseError } = this.state;
        const destPosValue = destPos !== null ? destPos : "";
        return (
            <HoistWrapper name={name} groupSettingsMode={groupSettingsMode} addHoistModeGroupID={addHoistModeGroupID} onClick={this.onHoistClicked} >
                <AlertDialog open={chooseError} handleClose={this.onChooseErrorConfirmed} />
                <p className={style.name} >{name}</p>
                <PositionText name={name} />
                <div className={style.separator} />
                {/* in groupSettings we dont want to show the destpos value */}
                <p className={style.destPos} >{groupSettingsMode ? "" : destPosValue}</p>
                <p className={style.maxPos} >{endTargets.EPT ? endTargets.EPT.value : ""}</p>
                <p className={style.minPos} >{endTargets.EPB ? endTargets.EPB.value : ""}</p>
                <PositionBar actValue={57} destPos={23} />
                <div className={style.warning} >
                    <StatusSigns name={name} />
                </div>
            </HoistWrapper>
        );
    }
}

const getAllScenesData = (store): Array<SceneConfig> => store.playDataReducer.allSceneData;
const getCurrentScene = (store): number => store.playDataReducer.currentScene;

const makeGetDestPos = () => {
    return createSelector(
        [getAllScenesData, getCurrentScene, (state, currentName) => currentName],
        (allSceneData, currentScene, currentName) => {
            let deads = allSceneData.length ? allSceneData[currentScene].deads : [];
            for (let deadIndex in deads) {
                // look for the dead that contains the current Hoist
                let posData = deads[deadIndex].posHoists.find(posHoist => posHoist.id === currentName);
                if (posData) {
                    // if I found the posData of this hoist, I return the pos value
                    return posData.value;
                }
            }
            return null;
        }
    )
}



const getEndTargetsFromReducer = (state) => state.serviceReducer.endTargets;

const makeGetEndTargets = () => {
    return createSelector(
        [getEndTargetsFromReducer, (state, currentName) => currentName],
        (endTargets: Array<HoistConfig>, currentName: number): TargetValues => {
            const myHoistConfig = endTargets.find(config => config.id === currentName);
            if (myHoistConfig) {
                return {
                    "EPT": myHoistConfig.targets.EPT,
                    "EPB": myHoistConfig.targets.EPB
                }
            } else {
                return {
                    "EPT": null,
                    "EPB": null
                }
            }
        }
    )
}

const makeMapToProps = () => {
    const getDestPos = makeGetDestPos();
    const getEndTargets = makeGetEndTargets();
    const mapStateToProps = (state, ownProps) => {
        return {
            addHoistModeGroupID: state.groupConfigReducer.addHoistModeGroupID,
            destPos: getDestPos(state, ownProps.name),
            endTargets: getEndTargets(state, ownProps.name),
            groupSettingsMode: state.groupDescReducer.groupSettingsMode
        }
    }
    return mapStateToProps;
}

export default connect(makeMapToProps)(Hoist);
