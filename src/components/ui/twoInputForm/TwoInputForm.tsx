import * as React from "react";

interface Props {
    handleSubmit: () => void
    firstValue: string
    secondValue: string
    firstChangeHandler: (event: any) => void
    secondChangeHandler: (event: any) => void
}

const TwoInputForm = (props: Props) => (
    <form id="targetForm" onSubmit={props.handleSubmit}>
        <label>
            Name:
            <input type="text" value={props.firstValue} onChange={props.firstChangeHandler} />
        </label>
        <label>
            Value:
            <input type="text" value={props.secondValue} onChange={props.secondChangeHandler} />
        </label>
        <input type="submit" value="Submit" />
    </form>
)

export default TwoInputForm;
