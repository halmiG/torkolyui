import * as React from "react";

interface Props {
  icon: string
  onClick?: any
  size?: number
  color?: string
  fill?: string
}

const Icon: React.SFC<Props> = props => {
  const styles = {
    svg: {
      display: 'inline-block',
      verticalAlign: 'middle',
    },
    path: {
      fill: props.color,
    },
  };

  return (
    <svg
      onClick={props.onClick}
      style={styles.svg}
      width={`${props.size}px`}
      height={`${props.size}px`}
      viewBox="0 0 300 300"
    >
      <path
        style={styles.path}
        d={props.icon}
        fill={props.fill}
      />
    </svg>
  );
};

Icon.defaultProps = {
  size: 16
}

export default Icon;