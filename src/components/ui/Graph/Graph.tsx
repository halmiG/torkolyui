import * as React from "react";
import { Label, LineChart, XAxis, YAxis, Tooltip, Legend, Line, ReferenceLine } from "recharts";

interface Props {
    /** Data to be plot */
    plotData: any //TODO make some kind of object type!!
    /** Refernce value on the y-axis */
    referenceValue?: number
    /** Label(s) of the x-axis */
    xAxisLabels?: Array<string>
    /** The datakey of the x-axis, whih will be used to scale */
    xAxisDataKey?: string
    /** Indicates whether the graph(s) have dots at the datapoints or not */
    isDotted?: boolean
    /** Optional colors of the lines */
    colors?: Array<string>
}

interface State {

}

export default class Graph extends React.Component<Props, State> {
    public static defaultProps: Partial<Props> = {
            isDotted: false,
            colors: ["#9b58b5", "#46b9f3", "green", "red"],
            referenceValue: null,
            xAxisLabels: [],
            xAxisDataKey: null 
    }

    constructor(props: Props) {
        super(props)
    }

    renderLines = (lineNumber: any) : any => {
        const { isDotted, plotData, colors } = this.props;
        const currentDataKey = Object.keys(plotData[0])[lineNumber * 2 + 1];
        return(
            <Line key={lineNumber} dot={isDotted} type="monotone" dataKey={currentDataKey} stroke={colors[lineNumber]} />
        );
    }

    render(){
        const { referenceValue, plotData, xAxisDataKey, xAxisLabels } = this.props;
        const renderLegend = () => {
            return (
              <ul>
                {xAxisLabels.map((entry, index) => (
                    <li key={`item-${index}`} className={`legend-items-${index}`} style={{textAlign:"center", color: `${this.props.colors[index]}`, fontSize: "12px"}} >{entry}</li>
                  ))}
              </ul>
            );
        };
        const defaultKey = Object.keys(plotData[0])[0];
        const numberOfGraphLines = Object.keys(plotData[0]).length / 2;
        //const maxXValue = plotData[plotData.length - 1][xAxisDataKey ? xAxisDataKey : defaultKey];
        const lines = [];
        for (let i = 0; i < numberOfGraphLines; i += 1) {
            lines.push(this.renderLines(i));
        }
        return(
            <div className="line-chart">
                <LineChart width={312} height={200} data={plotData}>
                    <XAxis dataKey={xAxisDataKey ? xAxisDataKey : defaultKey} stroke="gray" />
                    <YAxis stroke="gray"/>
                    <ReferenceLine y={referenceValue} stroke="#727e83" strokeDasharray="3 3">
                        <Label value={referenceValue} offset={5} position="left" stroke="#727e83"/>
                    </ReferenceLine>
                    <Tooltip/>
                    {lines}
                    <Legend content={renderLegend}/>
                </LineChart>
            </div>
        ); 
    }
}