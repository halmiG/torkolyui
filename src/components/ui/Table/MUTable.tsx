import * as React from "react";
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import { IconButton } from "@material-ui/core";
import DeleteIcon from '@material-ui/icons/Delete';
const style = require("./MUTable.css");

export interface RowAction {
    action: (rowData) => void
    name: string
}

interface Props {
    headers: Array<string>
    bodyRows: Array<any>
    bodyDataNames: Array<string>
    onRowDelete?: (rowData) => (event) => void // this is a basic function shouldnt be part of the actionList
    onRowEdit?: (rowData) => (event) => void // this could be a basic function next to Delete, hence it can come separately
    actionList?: Array<RowAction> // these are all the actions that should be displayed ina a dropdown NOTE not implemented yet
    title?: string
}

const MUTable = (props: Props) => {
    const renderBodyRowCells = (rowData, j) => {
        let rowToReturn = [];

        rowToReturn = props.bodyDataNames.map((dataName, i) => (
            <TableCell onClick={props.onRowEdit(rowData)} key={"" + i + j} numeric={i === 0 ? false : true} >{rowData[dataName]}</TableCell>
        ));
        //now we add the action functionalities
        if (props.onRowDelete != undefined || props.onRowEdit != undefined || props.actionList) {
            rowToReturn.push((
                <TableCell key={"delete"} numeric={true}>
                    {props.onRowDelete != undefined &&
                        <IconButton onClick={props.onRowDelete(rowData)} >
                            <DeleteIcon />
                        </IconButton>}
                    {/* //TODO make the drop down for the list of actions */}
                </TableCell>
            ));
        }
        return rowToReturn;
    }

    return (
        <div>
            {props.title &&
                <div className={style.tableTitle} >{props.title}</div>}
            <Table>
                <TableHead>
                    <TableRow>
                        {props.headers.map((header, i) => (
                            <TableCell key={header} numeric={i === 0 ? false : true}>{header}</TableCell>
                        ))}
                    </TableRow>
                </TableHead>
                <TableBody>
                    {props.bodyRows.map((row, i) => (
                        <TableRow hover={true} key={"" + row[0] + i}>
                            {renderBodyRowCells(row, i)}
                        </TableRow>
                    ))}
                </TableBody>
            </Table>
        </div>
    )
}

export default MUTable;