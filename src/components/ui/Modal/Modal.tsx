import * as React from "react";
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
// import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import Button from '@material-ui/core/Button';

interface ModalAction {
    name: string
    handlerFunc: any
}

interface Props {
    open: boolean
    title: string
    children?: any
    onClose: any
    actions?: Array<ModalAction>
}

const Modal = (props: Props) => {
    const renderActions = () => {
        return (
            props.actions.map(action => {
                return (
                    <Button key={action.name} onClick={action.handlerFunc} >
                        {action.name}
                    </Button>
                )
            })
        )
    }
    return (
        <Dialog
            open={props.open}
            onClose={props.onClose}
        >
            <DialogTitle>{props.title}</DialogTitle>
            <DialogContent>
                <DialogContent>
                    {props.children}
                </DialogContent>
            </DialogContent>
            <DialogActions>
                {renderActions()}
            </DialogActions>
        </Dialog>
    );
}

export default Modal;

