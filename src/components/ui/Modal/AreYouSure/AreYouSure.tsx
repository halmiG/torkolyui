import * as React from "react";
import Modal from "../Modal";

interface Props {
    onYes: () => void
    onNo: () => void
    open: boolean
    body: any
}

const AreYouSure = (props: Props) => {
    const actions = [
        {
            "name": "Igen",
            "handlerFunc": props.onYes
        },
        {
            "name": "Mégse",
            "handlerFunc": props.onNo
        }
    ];
    return (
        <Modal
            open={props.open}
            title={"Figyelem!"}
            onClose={props.onNo}
            actions={actions}
        >
            <div>
                {props.body}
            </div>
        </Modal>
    );
}

export default AreYouSure;