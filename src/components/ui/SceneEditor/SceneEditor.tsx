import * as React from "react";
import connect from "react-redux/lib/connect/connect";
import * as playDataActions from "../../../actions/playDataActions";
import SceneList from "./components/SceneList/SceneList";
import DownArrowIcon from "@material-ui/icons/ArrowDownward";
import UpArrowIcon from "@material-ui/icons/ArrowUpward";
import Button from '@material-ui/core/Button';
import NewSceneButton from "./components/NewSceneButton/NewSceneButton";
import ErrorIcon from "@material-ui/icons/Error";
const style = require("./SceneEditor.css");

interface Props {
    dispatch?: any
    numberOfScenes?: number
    configState: boolean
    scenesUnsaved?: boolean
    currentScene?: number
}

interface State {
    currentScene: number
}

@connect((store) => {
    return {
        numberOfScenes: store.playDataReducer.numberOfScenes,
        scenesUnsaved: store.playDataReducer.scenesUnsaved,
        currentScene: store.playDataReducer.currentScene
    }
})


export default class SceneEditor extends React.Component<Props, State> {
    constructor(props: Props) {
        super(props);

        this.state = {
            currentScene: 0
        }
    }

    static getDerivedStateFromProps(props, state) {
        if (state.currentScene !== props.currentScene) {
            return {
                currentScene: props.currentScene
            }
        }
        return null;
    }

    componentDidMount() {
        this.props.dispatch(playDataActions.getNumberOfScenes());
    }

    onPrevScene = () => {
        const { currentScene } = this.state;
        if (currentScene !== 0) {
            this.setState({ currentScene: currentScene - 1 }, () => {
                this.props.dispatch(playDataActions.setCurrentScene(currentScene - 1));
            });
        }
    }

    onNextScene = () => {
        const { currentScene } = this.state;
        const { numberOfScenes } = this.props;
        if (currentScene !== numberOfScenes - 1) {
            this.setState({ currentScene: currentScene + 1 }, () => {
                this.props.dispatch(playDataActions.setCurrentScene(currentScene + 1));
            });
        }
    }


    onNewSceneClicked = () => {
        const { numberOfScenes } = this.props;
        // set to the last scene first befor update the length 
        this.setState({ currentScene: numberOfScenes }, () => {
            this.props.dispatch(playDataActions.addNewEmptyScene());
        });
    }

    onSaveModifs = () => {
        this.props.dispatch(playDataActions.saveAllSceneData());
    }

    render() {
        const { currentScene } = this.state;
        const { numberOfScenes, configState, scenesUnsaved } = this.props;

        return (
            <div>
                <div className={style.saveButton}>
                    {configState &&
                        <Button variant="outlined" onClick={this.onSaveModifs} color={scenesUnsaved ? "secondary" : "default"}>
                            {scenesUnsaved &&
                                <ErrorIcon className={style.errorIcon} />}
                            Mentés
                    </Button>}
                </div>
                <SceneList configState={configState} />
                <Button variant="extendedFab" onClick={this.onNextScene} disabled={currentScene === numberOfScenes - 1}>
                    <DownArrowIcon />
                    Következő
                </Button>
                <Button variant="extendedFab" onClick={this.onPrevScene} disabled={currentScene === 0}>
                    <UpArrowIcon />
                    Előző
                    </Button>
                {configState &&
                    <NewSceneButton onClick={this.onNewSceneClicked} />}
            </div>
        )
    }
}

