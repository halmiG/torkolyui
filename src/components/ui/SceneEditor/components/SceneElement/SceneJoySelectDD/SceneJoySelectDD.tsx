import * as React from "react";
import DDMenu from "../../../../DDMenu/DDMenu";
import { IconButton } from "@material-ui/core";
import joyListDDContent from "../../../../DDMenu/joyListDDContent";
import * as APIGroup from "../../../../../../API/group";
import connect from "react-redux/lib/connect/connect";
import * as playDataActions from "../../../../../../actions/playDataActions";

interface Props {
    sceneNumber: number
    groupIndex: number
    dispatch?: any
    selectedJoy?: number
}

interface State {
    selectedJoy: number
    joySelectAnchor: any
}

@connect((store, ownProps: Props) => {
    return {
        selectedJoy: store.playDataReducer.allSceneData[ownProps.sceneNumber].groups[ownProps.groupIndex].joystick
    }
})

export default class SceneJoySelectDD extends React.Component<Props, State> {
    constructor(props: Props) {
        super(props);

        this.state = {
            selectedJoy: 0,
            joySelectAnchor: null
        }
    }

    static getDerivedStateFromProps(props: Props, state: State) {
        if (props.selectedJoy != null && state.selectedJoy !== props.selectedJoy) {
            return {
                selectedJoy: props.selectedJoy
            }
        }
        return null;
    }

    onJoySelected = (targetJoy: number) => (event) => {
        const { sceneNumber, groupIndex } = this.props;
        APIGroup.setMovingJoy(sceneNumber, groupIndex, targetJoy)
            .then(() => {
                this.setState({ selectedJoy: targetJoy, joySelectAnchor: null });
                this.props.dispatch(playDataActions.addJoyToGroup(sceneNumber, groupIndex, targetJoy));
            }).catch((error) => {
                console.error(`cant send joy ${targetJoy} to scene ${sceneNumber} group ${groupIndex}, error:`, error);
            });
    }

    onJoySelectClose = () => {
        this.setState({ joySelectAnchor: null });
    }

    onJoySelectClicked = (event) => {
        this.setState({ joySelectAnchor: event.currentTarget });
    }

    render() {
        const { selectedJoy, joySelectAnchor } = this.state;
        const DDContent = joyListDDContent(this.onJoySelected);
        return (
            <div style={{ float: "right" }}>
                <IconButton onClick={this.onJoySelectClicked}>
                    {DDContent[selectedJoy].content}
                </IconButton>
                <DDMenu
                    open={Boolean(joySelectAnchor)}
                    onClose={this.onJoySelectClose}
                    anchorEl={joySelectAnchor}
                    menuItems={DDContent}
                    selected={selectedJoy}
                />
            </div>
        )
    }
}