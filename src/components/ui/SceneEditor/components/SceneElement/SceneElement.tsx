import * as React from "react";
// import SceneEditorDD from "../SceneEditDD/SceneEditDD";
import { GroupConfig, DeadConfig } from "../../../../pages/play/components/sceneArea/models/PlayConfig";
// import { IconButton } from "@material-ui/core";
// import DeleteIcon from '@material-ui/icons/Delete';
import * as playDataActions from "../../../../../actions/playDataActions";
import connect from "react-redux/lib/connect/connect";
import SceneEditActions from "./SceneEditActions/SceneEditActions";
const style = require("./SceneElement.css");

interface Props {
    groupsDesc: Array<GroupConfig>
    deads: Array<DeadConfig>
    sceneNumber: number
    config: boolean
    removeDeadFromScene?: any
    setCurrentScene?: any
}

const SceneElement = (props: Props) => {
    const { config, sceneNumber } = props;

    const onDeleteDead = (index) => () => {
        props.removeDeadFromScene(props.sceneNumber, index)
    }

    const onSceneElementClicked = () => {
        props.setCurrentScene(props.sceneNumber);
    }

    return (
        <div className={style.wrapper}>
            <div className={style.dataWrapper} onClick={onSceneElementClicked} >
                <div className={style.sceneTitle}>
                    Jelenet
                </div>
                <div className={style.sceneName}>{props.sceneNumber}</div>
                <div className={style.groupTitle}>Csoport</div>
                <div className={style.groups}>
                    <ul>
                        {props.groupsDesc.map(group => (
                            <li className={style.custom_li} key={group.id}>{group.name}</li>))}
                    </ul>
                </div>
                <div className={style.deadTitle}>Végcél</div>
                <div className={style.deads}>
                    <ul>
                        {props.deads.map(dead => (
                            <li className={style.custom_li} key={dead.id}>{dead.name}</li>))}
                    </ul>
                </div>
                <div className={style.hoistTitle}>Ponthúzó</div>
                <div className={style.hoists}>
                    <ul>
                        {props.groupsDesc.map(group => (
                            <li className={style.custom_li} key={group.id}>{`${group.hoists.map(hoist => hoist.id)}`}</li>))}
                    </ul>
                </div>

            </div>
            <SceneEditActions
                onClick={onSceneElementClicked}
                config={config}
                deads={props.deads}
                onDeleteDead={onDeleteDead}
                sceneNumber={sceneNumber}
            />
        </div>

    );
}

export default connect(null, playDataActions)(SceneElement);