import * as React from "react";
import SceneEditorDD from "../../SceneEditDD/SceneEditDD";
import { IconButton } from "@material-ui/core";
import DeleteIcon from '@material-ui/icons/Delete';
import { DeadConfig } from "../../../../../pages/play/components/sceneArea/models/PlayConfig";
import SceneJoySelectDD from "../SceneJoySelectDD/SceneJoySelectDD";
const style = require("../SceneElement.css");

interface Props {
    config: boolean
    deads: Array<DeadConfig>
    onDeleteDead: (index: number) => () => void
    sceneNumber: number
    onClick: () => void
}

const SceneEditActions = (props: Props) => {
    const { config, deads, onDeleteDead, sceneNumber, onClick } = props;
    return (
        <div className={style.editArea} onClick={config ? null : onClick} >
            {config &&
                <div className={style.editActions} >
                    <SceneEditorDD sceneNumber={sceneNumber} />
                </div>}
            {config &&
                <div className={style.deadIcons} >
                    <ul>
                        {deads.map((dead, i) => (
                            <li key={dead.id}>
                                <IconButton onClick={onDeleteDead(i)} >
                                    <DeleteIcon />
                                </IconButton>
                                {/* NOTE lets call it groupIndex, however it is the deadIndex, but it is the same as the group */}
                                <SceneJoySelectDD sceneNumber={sceneNumber} groupIndex={i} />
                            </li>
                        ))}
                    </ul>
                </div>}
        </div>
    )
}

export default SceneEditActions;

