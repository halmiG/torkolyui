import * as React from "react";
import DDMenu, { MenuItemModel } from "../../../DDMenu/DDMenu";
import MoreVertIcon from "@material-ui/icons/MoreVert";
import DeadSelectorList from "../../../../pages/play/components/sceneArea/sceneField/component/DeadSelectorList";
import { IconButton } from "@material-ui/core";
import connect from "react-redux/lib/connect/connect";
import * as playDataActions from "../../../../../actions/playDataActions";

interface Props {
    sceneNumber: number
    dispatch?: any
}

interface State {
    sceneEditAnchor: any
    choosingDead: boolean
}

@connect()

export default class SceneEditorDD extends React.Component<Props, State> {
    constructor(props: Props) {
        super(props);

        this.state = {
            sceneEditAnchor: null,
            choosingDead: false
        }
    }

    onClose = () => {
        this.setState({ sceneEditAnchor: null });
    }

    onMoreClicked = (event) => {
        this.setState({ sceneEditAnchor: event.currentTarget });
    }

    onAddDeadClicked = () => {
        this.setState({ choosingDead: true });
    }

    onChoosingDeadClose = () => {
        this.setState({ choosingDead: false });
    }

    onSceneRemove = () => {
        const { sceneNumber } = this.props;
        this.props.dispatch(playDataActions.removeScene(sceneNumber));
    }

    render() {
        const { sceneEditAnchor, choosingDead } = this.state;
        const { sceneNumber } = this.props;

        const DDContent: Array<MenuItemModel> = [
            {
                content: "Jelenet törlése",
                onClickHandler: this.onSceneRemove
            },
            {
                content: "Végcél Hozzáadása",
                onClickHandler: this.onAddDeadClicked
            }
        ]

        return (
            <div>
                <IconButton onClick={this.onMoreClicked}>
                    <MoreVertIcon />
                </IconButton>
                <DDMenu
                    open={Boolean(sceneEditAnchor)}
                    onClose={this.onClose}
                    anchorEl={sceneEditAnchor}
                    menuItems={DDContent}
                />
                <DeadSelectorList sceneNumber={sceneNumber} open={choosingDead} onChoosingDeadClose={this.onChoosingDeadClose} />
            </div>
        )
    }
}