import * as React from "react";
import AddIcon from "@material-ui/icons/AddCircleOutline"
import Button from '@material-ui/core/Button';

interface Props {
    onClick: () => void
}

const NewSceneButton = (props: Props) => {
    const onAddNewScene = () => {
        props.onClick();
    }

    return (
        <Button onClick={onAddNewScene}>
            <AddIcon />
            Új Jelenet
        </Button>
    )
}

export default NewSceneButton;