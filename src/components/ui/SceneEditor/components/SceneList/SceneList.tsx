import * as React from "react";
import { Element, scroller } from 'react-scroll';
const style = require("./SceneList.css");
import SceneElement from "../SceneElement/SceneElement";
import Button from '@material-ui/core/Button';
import FirstPageIcon from "@material-ui/icons/FirstPage";
import { SceneConfig } from "../../../../pages/play/components/sceneArea/models/PlayConfig";
import * as playDataActions from "../../../../../actions/playDataActions";
import connect from "react-redux/lib/connect/connect";

interface Props {
    dispatch?: any
    currentScene?: number
    configState: boolean
    allSceneData?: Array<SceneConfig>
}

interface State {
    error: boolean
}

@connect((store) => {
    return {
        allSceneData: store.playDataReducer.allSceneData,
        currentScene: store.playDataReducer.currentScene
    }
})

export default class SceneList extends React.Component<Props, State> {
    constructor(props: Props) {
        super(props);

        this.state = {
            error: false
        }
    }

    componentDidMount() {
        this.props.dispatch(playDataActions.getAllSceneData());
    }

    componentDidUpdate() {
        const { currentScene } = this.props;
        scroller.scrollTo(currentScene, {
            duration: 250,
            delay: 100,
            smooth: true,
            containerId: 'sceneContainer',
        });
    }

    onJumpToCurrentScene = () => {
        const { currentScene } = this.props;
        scroller.scrollTo(currentScene, {
            duration: 250,
            delay: 100,
            smooth: true,
            containerId: 'sceneContainer',
        });
    }

    renderScenes = () => {
        const { currentScene, configState, allSceneData } = this.props;
        return (allSceneData.map((scene, i) => (
            <Element className={currentScene === i ? style.activeSceneElement : style.sceneElement} name={i.toString()} key={i} >
                <SceneElement config={configState} groupsDesc={scene.groups} deads={scene.deads} sceneNumber={i} />
            </Element>)
        ))
    }

    render() {
        return (
            <div>{this.state.error &&
                <div>ERROR!!!</div>}
                <div className={style.currentSceneButton}>
                    <Button variant="fab" onClick={this.onJumpToCurrentScene}>
                        <FirstPageIcon />
                    </Button>
                </div>
                <Element className={style.sceneContainer} id="sceneContainer" >
                    {this.renderScenes()}
                </Element>
            </div>
        )
    }
}