import * as React from "react";
import { Component } from "react";
import HoistGrid from "../../../../ui/hoistGrid/HoistGrid";
import * as adsActions from "../../../../../actions/adsActions";
import { Route, Switch } from "react-router";
import connect from "react-redux/lib/connect/connect"
import SceneEditor from "../../../../ui/SceneEditor/SceneEditor";
import ConfigArea from "./components/ConfigArea/ConfigArea";

interface Props {
    dispatch?: any
    match?: any
}

interface State {
}

@connect()

export default class SceneArea extends Component<Props, State> {
    constructor(props: Props) {
        super(props);
        this.state = {
        }
    }

    componentDidMount() {
        this.props.dispatch(adsActions.listenToADSData());
        this.props.dispatch(adsActions.startADS());
        // this.props.dispatch(deadDescActions.loadDeads());
    }

    componentWillMount() {
        this.props.dispatch(adsActions.clearADSListener());
        this.props.dispatch(adsActions.stopADS());
    }

    render() {
        const PlaySceneEditor = () => (
            <SceneEditor configState={false} />
        )
        return (
            <div id="config_page">
                <HoistGrid />

                <Switch>
                    <Route exact path={this.props.match.path} component={PlaySceneEditor} />
                    <Route path={`${this.props.match.path}/config`} component={ConfigArea} />
                </Switch>
            </div>
        )
    }
}