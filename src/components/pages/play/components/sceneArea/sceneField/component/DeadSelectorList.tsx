import * as React from "react";
import { Component } from "react";
import connect from "react-redux/lib/connect/connect";
import { DeadConfig, SceneConfig } from "../../models/PlayConfig";
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableRow from '@material-ui/core/TableRow';
import TableHead from '@material-ui/core/TableHead';
import Button from '@material-ui/core/Button';
import Radio from '@material-ui/core/Radio';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import * as playDataActions from "../../../../../../../actions/playDataActions";
import { createSelector } from "reselect";
import Modal from "../../../../../../ui/Modal/Modal";

interface Props {
    deadsArray?: Array<DeadConfig>
    dispatch?: any
    sceneDesc?: SceneConfig
    open: boolean
    onChoosingDeadClose: () => void
    sceneNumber: number
    conflictedGroups?: Array<{ name: string, id: number }>

}

interface State {
    selectedDead: any
}

class DeadSelectorList extends Component<Props, State> {
    constructor(props: Props) {
        super(props);

        this.state = {
            selectedDead: null
        }
    }

    handleDeadSelect = event => {
        this.setState({ selectedDead: event.target.value })
    }

    onRowClicked = (deadID: string) => () => {
        this.setState({ selectedDead: deadID })
    }

    onChoose = () => {
        const { selectedDead } = this.state;
        const { sceneDesc } = this.props;
        this.setState({ selectedDead: null })
        this.props.dispatch(playDataActions.addGroupWithDead(selectedDead, sceneDesc));
        this.props.onChoosingDeadClose();
    }

    onClose = () => {
        this.setState({ selectedDead: null })
        this.props.onChoosingDeadClose();
    }

    renderDeads = () => {
        const { deadsArray, sceneDesc } = this.props;
        const { selectedDead } = this.state;
        if (deadsArray) {
            return deadsArray.map(dead => {
                let disabledRow = sceneDesc.deads.find(sceneDead => sceneDead.id === dead.id) ? true : false;
                return (
                    <TableRow key={dead.id} hover={true} onClick={disabledRow ? null : this.onRowClicked(dead.id)} >
                        {/* checkbox cell */}
                        <TableCell>
                            <Radio
                                checked={selectedDead === dead.id}
                                value={dead.id}
                                onChange={this.handleDeadSelect}
                                disabled={disabledRow}
                            />
                        </TableCell>
                        <TableCell numeric={true}>
                            {dead.name}
                        </TableCell>
                        <TableCell numeric={true}>
                            {dead.group.groupName}
                        </TableCell>
                        <TableCell numeric={true}>
                            {`${dead.posHoists.map(hoist => hoist.id)}`}
                        </TableCell>
                    </TableRow >)
            })
        } else {
            return [];
        }
    }
    onAlertDialogClose = () => {
        this.props.dispatch(playDataActions.clearConflictedGroup());
    }

    render() {
        const { open, conflictedGroups } = this.props;
        const modalActions = [
            {
                "name": "Rendben!",
                "handlerFunc": this.onAlertDialogClose
            }
        ]
        return (
            <div>
                <Dialog
                    open={open}
                    onClose={this.props.onChoosingDeadClose}
                    disableBackdropClick={true}
                    disableEscapeKeyDown={true}
                    maxWidth="md"
                >
                    <DialogTitle>{"Végcél Lista"}</DialogTitle>
                    <DialogContent>
                        <Table>
                            <TableHead>
                                <TableRow>
                                    <TableCell />
                                    <TableCell numeric={true}>Név</TableCell>
                                    <TableCell numeric={true}>Csoport</TableCell>
                                    <TableCell numeric={true}>Ponthúzók</TableCell>
                                </TableRow>
                            </TableHead>
                            <TableBody>
                                {this.renderDeads()}
                            </TableBody>
                        </Table>
                    </DialogContent>
                    <DialogActions>
                        <Button variant="outlined" onClick={this.onChoose}>Kiválaszt</Button>
                        <Button variant="outlined" onClick={this.onClose}>Mégse</Button>
                    </DialogActions>
                </Dialog>
                <Modal
                    open={conflictedGroups.length ? true : false}
                    onClose={this.onAlertDialogClose}
                    title={"HIBA!"}
                    actions={modalActions}
                >
                    Az alábbi csoportok a jelentben, már tartalmazzák a ponthúzókat, amit be akar illeszteni!
                            {conflictedGroups.map(group => (`${group.name} (${group.id}) \n`))}
                </Modal>
            </div>
        )
    }
}

const getAllScenesData = (store) => store.playDataReducer.allSceneData;

const makeGetSceneDesc = () => {
    return createSelector(
        [getAllScenesData, (store, sceneNumber) => sceneNumber],
        (allSceneData, sceneNumber): SceneConfig => {
            return allSceneData[sceneNumber];
        }
    )
}

const makeMapTpProps = () => {
    const getSceneDesc = makeGetSceneDesc();
    const mapStateToProps = (store, ownProps) => {
        return {
            deadsArray: store.deadDescReducer.deadDescBuffer,
            sceneDesc: getSceneDesc(store, ownProps.sceneNumber),
            conflictedGroups: store.playDataReducer.conflictedGroups
        }
    }
    return mapStateToProps;
}

export default connect(makeMapTpProps)(DeadSelectorList);
