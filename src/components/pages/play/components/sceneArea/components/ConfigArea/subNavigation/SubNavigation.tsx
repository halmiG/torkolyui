import * as React from "react";
import { Button } from "@material-ui/core";
const style = require("./SubNavigation.css");

interface Props {
    groupNav: () => void
    sceneNav: () => void
    activeNav: number
}

const SubNavigation = (props: Props) => {
    return (
        <div className={style.navBar} >
            <div className={props.activeNav === 0 ? style.navButtonActive : style.navButtonPassive}>
                <Button
                    onClick={props.sceneNav}
                    variant={"flat"}
                    fullWidth={true}
                >
                    Jelenet Szerkesztő
                </Button>
            </div>
            <div className={props.activeNav === 1 ? style.navButtonActive : style.navButtonPassive}>
                <Button
                    onClick={props.groupNav}
                    variant={"flat"}
                    fullWidth={true}
                >
                    Csoport Szerkesztő
                </Button>
            </div>
        </div>
    )
}

export default SubNavigation;