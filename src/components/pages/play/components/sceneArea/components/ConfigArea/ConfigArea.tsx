import * as React from "react";
import SubNavigation from "./subNavigation/SubNavigation";
import SceneEditor from "../../../../../../ui/SceneEditor/SceneEditor";
import connect from "react-redux/lib/connect/connect";
import { Route, Switch } from "react-router";
import * as pageActions from "../../../../../../../actions/pageActions";
import GroupHandler from "../../groupHandler/GroupHandler";
const style = require("./ConfigArea.css");

interface Props {
    match?: any
    dispatch?: any
    path?: any
}

interface State {
    activeSubNav: number
}

@connect()

export default class ConfigArea extends React.Component<Props, State> {
    constructor(props: Props) {
        super(props);

        this.state = {
            activeSubNav: 0
        }
    }

    onNavToGroupEditor = () => {
        console.log("nav!")
        this.props.dispatch(pageActions.navigateTo(`${this.props.match.url}/groupEditor`, "Konfig/Csoport Szerkesztő"));
        this.setState({ activeSubNav: 1 });
    }

    onNavToSceneEditor = () => {
        this.props.dispatch(pageActions.navigateTo(this.props.match.url, "Konfig/Csoport Szerkesztő"));
        this.setState({ activeSubNav: 0 });
    }

    render() {
        const { activeSubNav } = this.state;
        const ConfigSceneEditor = () => (
            <SceneEditor configState={true} />
        )
        return (
            <div className={style.configAreaWrapper}>
                <div className={style.subNav}>
                    <SubNavigation
                        groupNav={this.onNavToGroupEditor}
                        sceneNav={this.onNavToSceneEditor}
                        activeNav={activeSubNav}
                    />
                </div>
                <div className={style.bottomEditor}>
                    <Switch>
                        <Route exact path={this.props.match.path} component={ConfigSceneEditor} />
                        <Route path={`${this.props.match.path}/groupEditor`} component={GroupHandler} />
                    </Switch>
                </div>
            </div >
        )
    }
}