import FlatButton from 'material-ui/FlatButton';
import { RadioButton, RadioButtonGroup } from 'material-ui/RadioButton';
import * as React from "react";
import { Component } from "react";
import connect from "react-redux/lib/connect/connect";
import { createSelector } from "reselect";
import * as groupConfigActions from "../../../../../../../../actions/groupConfigActions";
import * as groupDescActions from "../../../../../../../../actions/groupDescActions";
import * as deadDescActions from "../../../../../../../../actions/deadDescActions";
import { GroupConfig, defaultGroupConfig } from "../../../models/PlayConfig";
import { HoistConflict } from "../../../../../../../../reducers/groupDescReducer";
import Input from '@material-ui/core/Input';
import HoistList from "./hoistList/HoistList";
import DeadsList from "./deadsList/DeadsList";
import ConflictModal from "./conflictModal/ConflictModal";
import { v4 } from 'uuid';
const style = require("./GroupSettings.css");

interface Props {
    dispatch?: any
    groupToRender?: GroupConfig
    globalGroups?: Array<GroupConfig>
    groupDesc?: GroupConfig
    match?: any
    history?: any
}

interface State {
}

class GroupSettings extends Component<Props, State> {
    constructor(props: Props) {
        super(props);

        this.state = {
        }
    }

    componentDidMount() {
        const { groupToRender } = this.props;

        if (this.props.match.params.groupToEdit) { // hance not a new group
            this.props.dispatch(groupDescActions.loadGroup(groupToRender));
            this.props.dispatch(deadDescActions.loadDeads());
        } else { // new group, then we have to generate an ID for it
            this.props.dispatch(groupDescActions.loadGroup({ ...defaultGroupConfig, id: v4() }));
        }
    }
    componentWillUnmount() {
        this.props.dispatch(groupDescActions.clearGroup());
    }

    handleGroupNameChange = (event) => {
        const { groupDesc } = this.props;
        this.props.dispatch(groupDescActions.changeParamOfGroup("name", event.target.value));
        this.props.dispatch(deadDescActions.changeGroupNameInDead(groupDesc.id, event.target.value));
    }

    onSave = () => {
        const { groupDesc } = this.props;
        const { groupToEdit } = this.props.match.params;
        this.props.history.goBack();
        this.props.dispatch(deadDescActions.saveDeads());
        this.props.dispatch(groupConfigActions.saveGroupForPlay(groupDesc, groupToEdit));

    }

    onCloseSettings = () => {
        this.props.history.goBack();
    }

    onSyncModeChange = (event, value) => {
        this.props.dispatch(groupDescActions.changeParamOfGroup("syncMode", value));
    }

    onTargetItemEdit = (targetType: string) => {
        this.setState({ listedTargetType: targetType })
    }

    onCloseTargetList = () => {
        this.setState({ listedTargetType: "" });
    }

    onConflictApprove = (groupConflicts: Array<HoistConflict>) => {
        this.props.dispatch(groupDescActions.approveGroupConflict(groupConflicts));
    }

    onConflictDeny = () => {

        this.props.dispatch(groupDescActions.clearGroupConflict());
    }

    render() {
        const { groupDesc } = this.props;

        return (
            <div>
                <div className={style.settingsWrapper} >
                    <div className={style.nameInput}>
                        <div className={style.nameInputTitle}>Csoport Név</div>
                        <Input
                            value={groupDesc.name}
                            onChange={this.handleGroupNameChange}
                        />
                    </div>
                    <div className={style.modeRadioGroup}>
                        <div className={style.modeTitle}>Csoport Mód</div>
                        <RadioButtonGroup name="syncMode" valueSelected={groupDesc.syncMode} onChange={this.onSyncModeChange} >
                            <RadioButton value="út" label="útszinkron" />
                            <RadioButton value="idő" label="időszinkron" />
                        </RadioButtonGroup>
                    </div>
                    <div className={style.hoistListContainer}>
                        <HoistList groupID={groupDesc.id} />
                    </div>
                    <div className={style.deadList}>
                        <DeadsList />
                    </div>
                </div>
                <FlatButton label="Mentés" fullWidth={true} onClick={this.onSave} />
                <FlatButton label="Mégse" fullWidth={true} onClick={this.onCloseSettings} />
                <ConflictModal
                    onConflictDeny={this.onConflictDeny}
                    onConflictApprove={this.onConflictApprove}
                />
            </div>
        )
    }
}

const getGroups = (state) => state.groupConfigReducer.globalGroups;

const makeGetGroup = () => {
    return createSelector(
        [getGroups, (state, groupID) => groupID],
        (groups, groupID) => {
            let groupToReturn = groups.find(group => {
                if (group.id === groupID) {
                    return group;
                }
            });
            return groupToReturn;
        }
    )
}

const makeMapStateToProps = () => {
    const getGroup = makeGetGroup();
    const mapStateToProps = (state, ownProps) => {
        return {
            groupToRender: getGroup(state, ownProps.match.params.groupToEdit),
            globalGroups: state.groupConfigReducer.globalGroups,
            groupDesc: state.groupDescReducer.groupDescBuffer
        }
    }
    return mapStateToProps;
}

export default connect(makeMapStateToProps)(GroupSettings);