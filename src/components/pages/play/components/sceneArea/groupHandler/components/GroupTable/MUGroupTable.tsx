import DialogContentText from '@material-ui/core/DialogContentText';
// import TableSortLabel from '@material-ui/core/TableSortLabel';
import IconButton from '@material-ui/core/IconButton';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TablePagination from "@material-ui/core/TablePagination";
import TableRow from '@material-ui/core/TableRow';
import DeleteIcon from '@material-ui/icons/Delete';
import RaisedButton from "material-ui/RaisedButton";
import * as React from "react";
import { Component } from "react";
import connect from "react-redux/lib/connect/connect";
import { Link } from 'react-router-dom';
import * as groupConfigActions from "../../../../../../../../actions/groupConfigActions";
import AreYourSure from "../../../../../../../ui/Modal/AreYouSure/AreYouSure";
import { GroupConfig } from "../../../models/PlayConfig";
import EnhancedTableHead from "./components/EnhancedTableHead";
const style = require("./MUGroupTable.css");

interface Props {
    dispatch?: any
    globalGroups?: Array<GroupConfig>
    match?: any
    location?: any
    history?: any
}

interface State {
    order: any
    orderBy: any
    page: number
    sureOpen: boolean
    groupToDelete: string
}

@connect((store) => {
    return {
        globalGroups: store.groupConfigReducer.globalGroups,
        location: store.routerReducer.location
    }
})

export default class GroupTable extends Component<Props, State> {
    constructor(props: Props) {
        super(props);

        this.state = {
            order: "asc",
            orderBy: "calories",
            page: 0,
            sureOpen: false,
            groupToDelete: null
        }
    }

    handleRequestSort = (event, property) => {
        console.log("handle sort of: ", property);
    }

    handleChangePage = (event, page) => {
        this.setState({ page });
    }

    onGroupDelete = (groupName: string) => () => {
        this.setState({ sureOpen: true, groupToDelete: groupName })
        // this.props.dispatch(groupConfigActions.deleteGroupFromPlay(groupName));
    }

    resetDeleteStates = () => {
        this.setState({ sureOpen: false, groupToDelete: null });
    }

    onGroupDeleteSure = () => {
        const { groupToDelete } = this.state;
        this.props.dispatch(groupConfigActions.deleteGroupFromPlay(groupToDelete));
        this.resetDeleteStates();
    }

    navToEdit = (groupID) => () => {
        // this.props.history.push(`${this.props.location.pathname}/settings/${groupID}`)
        this.props.history.push(`${this.props.match.url}/settings/${groupID}`)
    }

    render() {
        const { globalGroups } = this.props;
        const { order, orderBy, page, sureOpen } = this.state;

        return (

            <div>
                <div className={style.groupTableTitle}>Csoportok</div>
                <Link to={`${this.props.match.url}/settings/`} >
                    <RaisedButton label={"Új csoport"} />
                </Link>
                <Table>
                    <EnhancedTableHead
                        order={order}
                        orderBy={orderBy}
                        createSortHandler={this.handleRequestSort}
                    />
                    <TableBody>
                        {globalGroups
                            .slice(page * 5, page * 5 + 5)
                            .map((group, index) => {
                                return (
                                    <TableRow hover={true} key={group.name}>
                                        <TableCell onClick={this.navToEdit(group.id)} component="th" scope="row" >
                                            {group.name}
                                        </TableCell>
                                        <TableCell onClick={this.navToEdit(group.id)} numeric={true}>{group.syncMode}</TableCell>
                                        <TableCell onClick={this.navToEdit(group.id)} numeric={true}>{group.hoists.map(hoist => `${hoist.id}, `)}</TableCell>
                                        <TableCell numeric={true}>
                                            <IconButton onClick={this.onGroupDelete(group.id)}>
                                                <DeleteIcon />
                                            </IconButton>
                                        </TableCell>
                                    </TableRow>
                                )
                            })}
                    </TableBody>
                </Table>
                <TablePagination
                    component="div"
                    count={globalGroups.length}
                    rowsPerPage={5}
                    page={page}
                    backIconButtonProps={{
                        "aria-label": "Previous Page"
                    }}
                    nextIconButtonProps={{
                        "aria-label": "Next Page"
                    }}
                    onChangePage={this.handleChangePage}
                    rowsPerPageOptions={[5]}
                />
                <AreYourSure
                    onYes={this.onGroupDeleteSure}
                    onNo={this.resetDeleteStates}
                    open={sureOpen}
                    body={(
                        <DialogContentText>
                            Biztos törölni akarj a csoportot, az összes mentett adatával együtt?
                        </DialogContentText>
                    )}
                />
            </div>
        )
    }
}