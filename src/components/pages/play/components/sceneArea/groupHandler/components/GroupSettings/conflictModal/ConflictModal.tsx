import * as React from "react";
import Modal from '../../../../../../../../ui/Modal/Modal';
import connect from "react-redux/lib/connect/connect";
import { HoistConflict } from "../../../../../../../../../reducers/groupDescReducer";

interface Props {
    groupConflicts?: Array<HoistConflict>
    onConflictDeny: () => void
    onConflictApprove: (groupConflicts: Array<HoistConflict>) => void
}

const ConflictModal = (props: Props) => {

    const onApprove = () => {
        props.onConflictApprove(props.groupConflicts);
    }

    const groupConflictActions = [
        {
            "name": "Igen",
            "handlerFunc": onApprove
        },
        {
            "name": "Nem",
            "handlerFunc": props.onConflictDeny
        }
    ];

    return (
        <Modal
            open={props.groupConflicts.length ? true : false}
            title={"Figyelem!"}
            onClose={props.onConflictDeny}
            actions={groupConflictActions}
        >
            {JSON.stringify(props.groupConflicts)}
        </Modal>
    );
}

const mapStateToProps = (state, ownProps) => {
    return {
        groupConflicts: state.groupDescReducer.groupConflicts
    }
}

export default connect(mapStateToProps)(ConflictModal);