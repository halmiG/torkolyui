import { Component } from "react";
import * as React from "react";
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableRow from '@material-ui/core/TableRow';
import TableHead from '@material-ui/core/TableHead';
import TablePagination from "@material-ui/core/TablePagination";
import IconButton from 'material-ui/IconButton';
import DeleteIcon from '@material-ui/icons/Delete';
import AddIcon from 'material-ui/svg-icons/content/add';
import ErrorIcon from "@material-ui/icons/Error";
import FormControlLabel from '@material-ui/core/FormControlLabel';
import connect from "react-redux/lib/connect/connect";
import DeadEditor from "./deadEditor/DeadEditor";
import * as deadDescActions from "../../../../../../../../../actions/deadDescActions";
import * as groupDescActions from "../../../../../../../../../actions/groupDescActions";
import { createSelector } from "reselect";
import { DeadConfig, HoistConfig } from "../../../../models/PlayConfig";
import AreYouSure from "../../../../../../../../ui/Modal/AreYouSure/AreYouSure";
import { DialogContentText } from "@material-ui/core";
const style = require("./DeadsList.css");


interface Props {
    deads?: Array<{ id: string, name: string }>
    errorAddDead?: boolean
    dispatch?: any
    groupID?: string
    groupName?: string
    missingPosSigns?: Array<number | string>
    location?: any,
}

interface State {
    page: number
    editDead: boolean
    deadToEdit: { id: string, name: string }
    sureOpen: boolean
    deadToDelete: { id: string, name: string }
}



class DeadsList extends Component<Props, State> {
    constructor(props: Props) {
        super(props);

        this.state = {
            page: 0,
            editDead: false,
            deadToEdit: { id: null, name: "" },
            sureOpen: false,
            deadToDelete: { id: null, name: "" }
        }
    }

    // componentDidMount() {
    //     this.props.dispatch(deadDescActions.loadDeads());
    // }

    onDeleteClicked = (deadToDelete: { id: string, name: string }) => (event) => {
        this.setState({ sureOpen: true, deadToDelete: deadToDelete });
    }

    onDeleteSure = () => {
        const { deadToDelete } = this.state;
        this.props.dispatch(groupDescActions.removeDeadFromGroup(deadToDelete));
        this.props.dispatch(deadDescActions.removeDead(deadToDelete));
        this.resetDeleteStates();
    }

    resetDeleteStates = () => {
        this.setState({ sureOpen: false, deadToDelete: { id: null, name: "" } });
    }

    handleChangePage = (event, page) => {
        this.setState({ page });
    }

    onEditClicked = (deadToEdit: { id: string, name: string }) => (event) => {
        this.setState({ editDead: true, deadToEdit: deadToEdit })
    }

    onAddDead = () => {
        this.setState({ editDead: true, deadToEdit: { id: null, name: "" } });
    }

    onEditorClose = () => {
        this.setState({ editDead: false, deadToEdit: { id: null, name: "" } });
    }

    renderDeadsRows = () => {
        const { deads, missingPosSigns } = this.props;
        const { page } = this.state;
        if (deads) {
            return deads
                .slice(page * 5, page * 5 + 5)
                .map(dead => (
                    <TableRow hover={true} key={dead.id}>
                        <TableCell onClick={this.onEditClicked(dead)} >
                            {missingPosSigns.find(sign => sign === dead.id) &&
                                <ErrorIcon className={style.errorIcon} />}
                            {dead.name}
                        </TableCell>
                        <TableCell numeric={true}>
                            <IconButton onClick={this.onDeleteClicked(dead)} >
                                <DeleteIcon />
                            </IconButton>
                        </TableCell>
                    </TableRow>
                ))
        } else {
            return [];
        }
    }

    render() {
        const { deads, groupID, groupName } = this.props;
        const { page, editDead, deadToEdit, sureOpen } = this.state;
        return (
            <div>
                <div className={style.deadsListTitle}>Végcélok</div>
                {editDead &&
                    <DeadEditor deadToEdit={deadToEdit} onEditorClose={this.onEditorClose} creatorGroup={{ groupID, groupName }} />
                    ||
                    <div>
                        <FormControlLabel
                            control={
                                <IconButton onClick={this.onAddDead} >
                                    <AddIcon />
                                </IconButton>}
                            label={"Hozzáadás"}
                        />
                        <Table>
                            <TableHead>
                                <TableRow>
                                    <TableCell>Név</TableCell>
                                    <TableCell numeric={true} />
                                </TableRow>
                            </TableHead>
                            <TableBody>
                                {this.renderDeadsRows()}
                            </TableBody>
                        </Table>
                        <TablePagination
                            component="div"
                            count={deads.length}
                            rowsPerPage={5}
                            page={page}
                            backIconButtonProps={{
                                "aria-label": "Previous Page"
                            }}
                            nextIconButtonProps={{
                                "aria-label": "Next Page"
                            }}
                            onChangePage={this.handleChangePage}
                            rowsPerPageOptions={[5]}
                        />
                    </div>}
                <AreYouSure
                    onYes={this.onDeleteSure}
                    onNo={this.resetDeleteStates}
                    open={sureOpen}
                    body={(
                        <DialogContentText>
                            Biztos törölni akarj a végcélt, az összes mentett adatával együtt?
                        </DialogContentText>
                    )}
                />
            </div>
        );
    }
}


const getHoists = (state) => state.groupDescReducer.groupDescBuffer.hoists;

const getDeadsDesc = (state) => state.deadDescReducer.deadDescBuffer;

const getDeadsOfGroupDesc = (state) => state.groupDescReducer.groupDescBuffer.deads;

const makeGetMissingPosSigns = () => {
    return createSelector(
        [getHoists, getDeadsDesc, getDeadsOfGroupDesc],
        (hoists: Array<HoistConfig>, deadDescArray: Array<DeadConfig>, groupDeads: Array<{ id: string, name: string }>) => {
            let filteredDeads: Array<DeadConfig> = [];
            groupDeads.forEach(groupDead => {
                let foundDeadDesc = deadDescArray.find(deadDesc => deadDesc.id === groupDead.id);
                //if loaded in properly
                foundDeadDesc ? filteredDeads.push(foundDeadDesc) : null;
            })
            let missingSigns = [];
            if (filteredDeads.length) { // the list of deads loaded in
                filteredDeads.forEach(dead => {
                    hoists.forEach(hoist => {
                        //if it is undefined, so it didnt find position to any of the hoists
                        if (!dead.posHoists.find(posHoist => posHoist.id === hoist.id)) {
                            // if it is not there already
                            if (!missingSigns.find(sign => sign === dead.id)) {
                                missingSigns.push(dead.id);
                            }
                        }
                    })
                });
            }
            return missingSigns;
        }
    )
}

const makeMapStateToProps = () => {
    const getMissingPosSigns = makeGetMissingPosSigns();
    const mapStateToProps = (state, ownProps) => {
        return {
            deads: state.groupDescReducer.groupDescBuffer.deads,
            groupID: state.groupDescReducer.groupDescBuffer.id,
            groupName: state.groupDescReducer.groupDescBuffer.name,
            errorAddDead: state.groupDescReducer.errorAddDead,
            missingPosSigns: getMissingPosSigns(state),
            location: state.routerReducer.location
        }
    }
    return mapStateToProps;
}

export default connect(makeMapStateToProps)(DeadsList);