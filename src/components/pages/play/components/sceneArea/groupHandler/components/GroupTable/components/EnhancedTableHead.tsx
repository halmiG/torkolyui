import * as React from "react";
import { TableHead, TableRow } from "@material-ui/core";
import TableSortLabel from "@material-ui/core/TableSortLabel";
import TableCell from "@material-ui/core/TableCell";

interface Props {
    order: any
    orderBy: any
    createSortHandler: any
}

const EnhancedTableHead = (props: Props) => {
    const columnNames: Array<string> = ["Név", "Szinkron mód", "Ponthúzók"];
    const {order, orderBy} = props;
    return (
        <TableHead>
            <TableRow>
                {columnNames.map(column => {
                    return (
                        <TableCell
                            key={column}
                            numeric={column === "Név" ? false : true}
                            sortDirection={orderBy === column ? order : false}
                        >
                            <TableSortLabel
                                active={orderBy === column}
                                direction={order}
                                onClick={props.createSortHandler(column)}
                            >
                                {column}
                            </TableSortLabel>
                        </TableCell>
                    );
                })}
                <TableCell numeric={true}>Szerkesztés</TableCell>
            </TableRow>
        </TableHead>
    );
}

export default EnhancedTableHead;