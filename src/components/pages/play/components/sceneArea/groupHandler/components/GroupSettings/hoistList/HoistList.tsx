import * as React from "react";
import { Component } from "react";
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableRow from '@material-ui/core/TableRow';
import TableHead from '@material-ui/core/TableHead';
import IconButton from 'material-ui/IconButton';
import DeleteIcon from '@material-ui/icons/Delete';
import * as groupConfigActions from "../../../../../../../../../actions/groupConfigActions";
import * as groupDescActions from "../../../../../../../../../actions/groupDescActions";
import JoySelectDD from "./components/JoySelectDD/JoySelectDD";
import connect from "react-redux/lib/connect/connect";
import { HoistConfig } from "../../../../models/PlayConfig";
const style = require("./HoistList.css");

interface Props {
    hoists?: Array<HoistConfig>
    groupID: string
    dispatch?: any
}

interface State {
    addHoistMode: boolean
}

@connect((store) => {
    return {
        hoists: store.groupDescReducer.groupDescBuffer.hoists
    }
})

export default class HoistList extends Component<Props, State> {
    constructor(props: Props) {
        super(props);

        this.state = {
            addHoistMode: false
        }
    }

    componentDidUpdate() {
        const { addHoistMode } = this.state;
        const { groupID } = this.props;
        //if there is no add hoist mode, then swithc it on
        //the reason becasue it is not in the componentDidMount(), that the groupID is not available at the time of mount
        if (!addHoistMode && groupID !== null) {
            this.props.dispatch(groupConfigActions.switchHoistAddMode(groupID));
            this.setState({ addHoistMode: true });
        }
    }

    componentWillUnmount() {
        this.props.dispatch(groupConfigActions.switchHoistAddMode(null));
    }

    onDeleteClicked = (hoistID: number) => (event) => {
        this.props.dispatch(groupDescActions.removeHoistFromGroup(hoistID, this.props.groupID));
    }

    // handleChangePage = (event, page) => {
    //     this.setState({ page });
    // }

    renderHoistRows = () => {
        const { hoists } = this.props;
        if (hoists) {
            return hoists
                .map(hoist => (
                    <TableRow key={hoist.id}>
                        <TableCell>
                            {hoist.id}
                        </TableCell>
                        {/* TODO add functionality to these */}
                        <TableCell>
                            <JoySelectDD hoistID={hoist.id} />
                        </TableCell>
                        <TableCell numeric={true}>
                            <IconButton onClick={this.onDeleteClicked(hoist.id)} >
                                <DeleteIcon />
                            </IconButton>
                        </TableCell>
                    </TableRow>
                ))
        } else {
            return [];
        }
    }

    onAddHoist = () => {
        const { groupID } = this.props;
        const { addHoistMode } = this.state;

        if (addHoistMode) {
            this.props.dispatch(groupConfigActions.switchHoistAddMode(null));
        } else {
            this.props.dispatch(groupConfigActions.switchHoistAddMode(groupID));
        }
        this.setState({ addHoistMode: !addHoistMode });
    }

    render() {
        return (
            <div>
                <div className={style.hoistListTitle}>Ponthúzók</div>
                <Table>
                    <TableHead>
                        <TableRow>
                            <TableCell>Név</TableCell>
                            <TableCell>Joystick</TableCell>
                            <TableCell numeric={true} />
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {this.renderHoistRows()}
                    </TableBody>
                </Table>
            </div>
        );
    }
}