import * as React from "react";
import { Component } from "react";
import Input from '@material-ui/core/Input';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableRow from '@material-ui/core/TableRow';
import TableHead from '@material-ui/core/TableHead';
import TablePagination from "@material-ui/core/TablePagination";
import Button from '@material-ui/core/Button';
import * as groupDescActions from "../../../../../../../../../../actions/groupDescActions";
import * as deadDescActions from "../../../../../../../../../../actions/deadDescActions";
import { DeadConfig, defaultDeadConfig, HoistConfig } from "../../../../../models/PlayConfig";
import { v4 } from 'uuid';
import produce from "immer";
import connect from "react-redux/lib/connect/connect";

interface Props {
    deadToEdit?: { id: string, name: string }
    hoists?: Array<HoistConfig>
    dispatch?: any
    deadsArray?: Array<DeadConfig>
    creatorGroup: { groupID: string, groupName: string }
    onEditorClose: () => void
}

interface State {
    deadDesc: DeadConfig
    page: number
    changed: boolean
}

@connect((store) => {
    return {
        hoists: store.groupDescReducer.groupDescBuffer.hoists,
        deadsArray: store.deadDescReducer.deadDescBuffer
    }
})

export default class DeadEditor extends Component<Props, State> {
    constructor(props: Props) {
        super(props);

        this.state = {
            deadDesc: defaultDeadConfig,
            page: 0,
            changed: false
        }
    }

    static getDerivedStateFromProps(props: Props, state: State) {
        let stateToReturn = null
        if (props.hoists.length !== state.deadDesc.posHoists.length) {
            let filterResult = true;
            // look for hoists that are not on the list
            //BUG, ha egy hoist lecserélődött pl az 5dbálló listán, akkor az újanek nem írja át az értékét erre a számra
            let newHoists = props.hoists.filter(hoist => {
                state.deadDesc.posHoists.find(posHoist => posHoist.id === hoist.id) ?
                    filterResult = false :
                    filterResult = true;
                return filterResult;
            });
            let newPosHoists = newHoists.map(newHoist => {
                return {
                    id: newHoist.id,
                    value: ""
                }
            })
            let removedIndeces = null;
            if (props.hoists.length) {
                // remove the ones that are not on the hoists list anymore, hence they were removed, during the dead edit
                //first find the index of the one, that is deleted
                state.deadDesc.posHoists.forEach((posHoist, i) => {
                    if (props.hoists.find(hoist => hoist.id === posHoist.id) == null) {
                        removedIndeces = i;
                    }
                });
            }

            stateToReturn = {
                deadDesc: {
                    ...state.deadDesc,
                    posHoists: [
                        ...state.deadDesc.posHoists
                            //remove from the state, the missing index
                            .filter((item, index) => index !== removedIndeces),
                        ...newPosHoists
                    ]
                }
            }
        }
        return stateToReturn;
    }

    componentDidMount() {
        const { deadToEdit, deadsArray, creatorGroup, hoists } = this.props;
        const { deadDesc } = this.state;
        //TODO ha itt átállok eg ynormálisabb ID-ra akkor nem kell az összes jelenetet tárolnom ebben a komponensben, 
        // mert csak arr ahazsnálom, hogy ellenőrizzem jó ID-t csinálok e
        if (!deadToEdit.id) {
            this.setState({
                deadDesc: {
                    ...deadDesc,
                    group: creatorGroup,
                    id: v4()
                }
            });
        }
        else {
            let hoistWithValues = hoists.map(hoist => {
                //look for the dead that is edited now, and take its posHoists list
                const currentPosHoists = deadsArray.find(dead => dead.id === deadToEdit.id).posHoists;
                // find the hoist in the list
                const currentHoist = currentPosHoists.find(posHoist => posHoist.id === hoist.id);
                // search for their values in the pos hosits
                let hoistWithValue = {
                    id: hoist.id,
                    value: currentHoist ? currentHoist.value : "" //if the value didnt exist in the list add value empty to it
                }
                return hoistWithValue;
            });
            this.setState({
                deadDesc: {
                    ...deadsArray.find(dead => dead.id === deadToEdit.id),
                    group: creatorGroup,
                    id: deadToEdit.id,
                    posHoists: [...hoistWithValues]
                }
            });
        }
    }

    onDeadNameChange = (event: any) => {
        this.setState({ changed: true, deadDesc: { ...this.state.deadDesc, name: event.target.value } })
    }

    handleChangePage = (event, page) => {
        this.setState({ page });
    }

    onHoistPosChange = (hoistID: number) => (event: any) => {
        this.setState(
            produce(this.state, draft => {
                draft.deadDesc.posHoists = draft.deadDesc.posHoists.map(hoist => {
                    if (hoist.id === hoistID) {
                        hoist.value = event.target.value;
                    }
                    return hoist;
                })
            })
        )
        this.setState({ changed: true });
    }

    onSave = () => {
        const { deadDesc } = this.state;
        const { deadToEdit } = this.props;

        if (deadToEdit.id) { // a dead were edited
            //TODO update with new value
            this.props.dispatch(deadDescActions.updateDead(deadDesc));
            this.props.dispatch(groupDescActions.updateDeadInGroup(deadDesc));
        } else { // new dead was created
            this.props.dispatch(groupDescActions.addDeadToGroup(deadDesc));
            this.props.dispatch(deadDescActions.addDead(deadDesc));
        }
        this.props.onEditorClose();
        this.setState({ changed: false });
    }

    onClose = () => {
        //Clear the deadDesc
        this.setState({ changed: false, deadDesc: defaultDeadConfig });
        this.props.onEditorClose();
    }

    getActualPosition = (hoistID) => () => {
        this.setState(
            produce(this.state, draft => {
                draft.deadDesc.posHoists = draft.deadDesc.posHoists.map(hoist => {
                    if (hoist.id === hoistID) {
                        hoist.value = Math.floor(Math.random() * Math.floor(32000));
                    }
                    return hoist;
                })
            })
        )
        this.setState({ changed: true });
    }

    renderRows = () => {
        const { hoists } = this.props;
        const { page, deadDesc } = this.state;
        if (hoists.length) {
            return hoists
                .slice(page * 5, page * 5 + 5)
                .map(hoist => {
                    let hoistOfRow = deadDesc.posHoists.length ? deadDesc.posHoists.find(posHoist => posHoist.id === hoist.id) : "";

                    return (
                        <TableRow key={hoist.id}>
                            <TableCell>
                                {hoist.id}
                            </TableCell>
                            <TableCell numeric={true}>
                                <Input
                                    value={hoistOfRow ? hoistOfRow["value"] : ""}
                                    onChange={this.onHoistPosChange(hoist.id)}
                                    type="number"
                                />

                            </TableCell>
                            <TableCell numeric={true}>
                                <Button variant="outlined" onClick={this.getActualPosition(hoist.id)}>
                                    Aktuális
                            </Button>
                            </TableCell>
                        </TableRow>)
                })
        } else {
            return [];
        }
    }

    render() {
        const { deadDesc, page, changed } = this.state;
        const { hoists } = this.props;
        return (
            <div>
                <Input
                    value={deadDesc.name}
                    onChange={this.onDeadNameChange}
                />
                <Table>
                    <TableHead>
                        <TableRow>
                            <TableCell>Név</TableCell>
                            <TableCell numeric={true} >Pozíció</TableCell>
                            <TableCell numeric={true} />
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {this.renderRows()}
                    </TableBody>
                </Table>
                <TablePagination
                    component="div"
                    count={hoists.length}
                    rowsPerPage={5}
                    page={page}
                    backIconButtonProps={{
                        "aria-label": "Previous Page"
                    }}
                    nextIconButtonProps={{
                        "aria-label": "Next Page"
                    }}
                    onChangePage={this.handleChangePage}
                    rowsPerPageOptions={[5]}
                />
                <Button variant="outlined" disabled={!changed} onClick={this.onSave}>Mentés</Button>
                <Button variant="outlined" onClick={this.onClose}>Mégse</Button>
                {/* //TODO create the editor */}
            </div>
        )
    }
}