import * as React from "react";
import DDMenu from "../../../../../../../../../../ui/DDMenu/DDMenu";
import * as APIhoist from "../../../../../../../../../../../API/hoist";
import joyListDDContent from "../../../../../../../../../../ui/DDMenu/joyListDDContent";


interface Props {
    hoistID: number
}

interface State {
    joySelectAnchor: any
    selectedJoy: number
}

export default class JoySelectDD extends React.Component<Props, State> {
    constructor(props: Props) {
        super(props);

        this.state = {
            joySelectAnchor: null,
            selectedJoy: 0
        }
    }

    onJoySelectClicked = (event) => {
        this.setState({ joySelectAnchor: event.currentTarget });
    }

    onJoySelectClose = () => {
        this.setState({ joySelectAnchor: null });
    }

    onJoySelected = (targetJoy: any) => (event) => {
        const { hoistID } = this.props;
        APIhoist.setMovingJoy(hoistID, targetJoy)
            .then(() => {
                this.setState({ selectedJoy: targetJoy, joySelectAnchor: null });
            }).catch((error) => {
                console.error(`cant send joy ${targetJoy} to hoist ${hoistID}, error:`, error);
            });
    }

    render() {
        const DDContent = joyListDDContent(this.onJoySelected);
        const { joySelectAnchor, selectedJoy } = this.state;
        return (
            <div>
                <div onClick={this.onJoySelectClicked}>
                    {DDContent[selectedJoy].content}
                </div>
                <DDMenu
                    open={Boolean(joySelectAnchor)}
                    onClose={this.onJoySelectClose}
                    anchorEl={joySelectAnchor}
                    menuItems={DDContent}
                    selected={selectedJoy}
                />
            </div>
        )
    }

}