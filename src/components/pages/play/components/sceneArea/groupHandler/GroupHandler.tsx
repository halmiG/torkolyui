import * as React from "react";
import { Route, Switch } from "react-router";
import MUGroupTable from "./components/GroupTable/MUGroupTable";
import GroupSettings from "./components/GroupSettings/GroupSettings";

interface Props {
    path?: string
    match?: any
}

const GroupHandler = (props: Props) => {
    return (
        <div>
            <Switch>
                {/* <Route exact path="/config/" component={GroupTable} /> */}
                <Route exact path={props.match.path} component={MUGroupTable} />
                <Route path={`${props.match.path}/settings/:groupToEdit`} component={GroupSettings} />
                <Route path={`${props.match.path}/settings/`} component={GroupSettings} />
            </Switch>
        </div>
    )
}

export default GroupHandler;

