export interface PlayConfig {
    name: string
    creatingDate?: any
    scenes: Array<SceneConfig>
}

export interface SceneConfig {
    id: string
    name: string
    groups: Array<GroupConfig>
    deads: Array<DeadConfig>
}

export interface GroupConfig {
    id: string
    name?: string,
    hoists?: Array<HoistConfig>
    deads?: Array<{
        id: string,
        name: string
    }>
    targetPosition?: TargetConfig
    maxSpeed?: number
    syncMode?: string
    sysMode?: number
    joystick?: number
}

export interface HoistConfig {
    id: number,
    targets: {
        EPT?: TargetConfig
        EPB?: TargetConfig
    }
}

export interface TargetConfig {
    value: number
}

export interface DeadConfig {
    id: string,
    name: string,
    group: { groupID?: string, groupName?: string },
    posHoists: Array<{
        id: number,
        value: number | string
    }>
}

export const defaultDeadConfig: DeadConfig = {
    id: null,
    name: "",
    group: {},
    posHoists: []
}

export const defaultGroupConfig: GroupConfig = {
    id: null,
    name: "",
    hoists: [],
    deads: [],
    maxSpeed: null,
    syncMode: "",
    sysMode: null,
    joystick: null
}
