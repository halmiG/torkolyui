import * as React from "react";
import { NavElement } from "./components/NavButtons";
import NavButtons from "./components/NavButtons";
import connect from "react-redux/lib/connect/connect"
import * as pageActions from "../../../../../actions/pageActions";

interface Props {
    playUrl: any
    history: any
    currentPageName?: string
    dispatch?: any
}

const MainNavigation = (props: Props) => {

    const onConfigNav = () => {
        const { playUrl } = props;
        props.dispatch(pageActions.navigateTo(`${playUrl}/config`, "Konfig"));
    }

    const onPlayingNav = () => {
        const { playUrl } = props;
        props.dispatch(pageActions.navigateTo(`${playUrl}`, "Lejátszás"));
    }

    const { currentPageName } = props;
    const navElements: Array<NavElement> = [
        {
            label: "Konfig",
            action: onConfigNav
        },
        {
            label: "Lejátszás",
            action: onPlayingNav
        },
        {
            label: "Szervíz",
            action: null
        }
    ];

    return (
        <div>
            <NavButtons navElements={navElements} currentPage={currentPageName} />
        </div>
    )
}

const mapStateToProps = (store) => {
    return {
        currentPageName: store.pageReducer.currentPageName
    }
}

export default connect(mapStateToProps)(MainNavigation)
