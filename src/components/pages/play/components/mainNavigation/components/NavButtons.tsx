import * as React from "react";
import Button from '@material-ui/core/Button';

export interface NavElement {
    label: string
    action: () => void
}

interface Props {
    navElements: Array<NavElement>
    currentPage: string
}


const NavButtons = (props: Props) => {

    const renderButtons = (elements: Array<NavElement>) => {
        return (
            elements.map(element => (
                <Button
                    variant={props.currentPage === element.label ? "contained" : "outlined"}
                    onClick={element.action}
                >
                    {element.label}
                </Button>
            ))
        )
    }

    return (
        <div>
            {renderButtons(props.navElements)}
        </div>
    );
}

export default NavButtons;