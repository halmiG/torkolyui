import * as React from "react";
import { Route, Switch } from "react-router";
import SceneArea from "./components/sceneArea/SceneArea";
import MainNavigation from "./components/mainNavigation/MainNavigation";
// import PlayingPage from "./components/pages/playing/PlayingPage";
import Button from '@material-ui/core/Button';
import HomeIcon from '@material-ui/icons/Home';
import connect from "react-redux/lib/connect/connect"
import * as pageActions from "../../../actions/pageActions";


const PlayPage = (props) => {
    console.log("props.ofpage", props)
    const onHomeClick = () => {
        props.dispatch(pageActions.navigateTo(`/`, "Előadás Lista"))
    }
    return (
        <div>
            <Button onClick={onHomeClick}>
                <HomeIcon />
            </Button>
            <MainNavigation playUrl={props.match.url} history={props.history} />
            <Switch>
                {/* <Route exact path="/config/" component={GroupTable} /> */}
                <Route path={props.match.path} component={SceneArea} />
                {/* <Route path={`/play/${props.match.params.playName}/config`} component={ConfigPage} /> */}
                {/* <Route path="/config/settings/:groupToEdit" component={GroupSettings} />
            <Route path="/config/settings/" component={GroupSettings} /> */}
            </Switch>
        </div>
    )
}

export default connect()(PlayPage);

