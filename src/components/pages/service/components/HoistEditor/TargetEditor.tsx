import * as React from "react";
import Input from '@material-ui/core/Input';
import InputLabel from '@material-ui/core/InputLabel';
import FormControl from '@material-ui/core/FormControl';
import connect from "react-redux/lib/connect/connect"
import RaisedButton from "material-ui/RaisedButton";
import * as serviceActions from "../../../../../actions/serviceActions";
import { ipcRenderer } from "electron";

interface Props {
    dispatch?: any
}

interface State {
    hoistName: string
    topValue: string
    bottomValue: string
}

@connect()

export default class TargetEditor extends React.Component<Props, State> {
    constructor(props: Props) {
        super(props);

        this.state = {
            hoistName: "",
            topValue: "",
            bottomValue: ""
        }
    }

    onHoistChange = (event) => {
        this.setState({ hoistName: event.target.value });
    }

    onTopChange = (event) => {
        this.setState({ topValue: event.target.value });
    }

    onBottomChange = (event) => {
        this.setState({ bottomValue: event.target.value });
    }

    setTargetValues = () => {
        const { hoistName, topValue, bottomValue } = this.state;
        this.props.dispatch(serviceActions.setEndTargets(hoistName, topValue, bottomValue));
    }

    init = () => {
        ipcRenderer.send("init-service");
    }

    render() {
        const { hoistName, topValue, bottomValue } = this.state;
        return (
            <div>
                <RaisedButton label={"Init"} onClick={this.init} />
                <FormControl>
                    <InputLabel>Ponthúzó: </InputLabel>
                    <Input value={hoistName} onChange={this.onHoistChange} />
                </FormControl>
                <FormControl>
                    <InputLabel>VHF: </InputLabel>
                    <Input value={topValue} onChange={this.onTopChange} />
                </FormControl>
                <FormControl>
                    <InputLabel>VHL: </InputLabel>
                    <Input value={bottomValue} onChange={this.onBottomChange} />
                </FormControl>
                <RaisedButton label={"Beállítás"} onClick={this.setTargetValues} />
            </div>
        )
    }
}