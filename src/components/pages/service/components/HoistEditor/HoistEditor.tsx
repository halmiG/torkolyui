import * as React from "react";
import TargetEditor from "./TargetEditor";

interface Props {

}

interface State {

}

export default class HoistEditor extends React.Component<Props, State> {
    constructor(props: Props) {
        super(props);

        this.state = {

        }
    }

    render() {
        return (
            <div>
                <TargetEditor />
                <h1>HOIST Editor Here</h1>
            </div>
        )
    }
}