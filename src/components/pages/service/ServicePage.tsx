import * as React from "react";
import connect from "react-redux/lib/connect/connect"
import * as pageActions from "../../../actions/pageActions";
import Button from '@material-ui/core/Button';
import HomeIcon from '@material-ui/icons/Home';
import HoistEditor from "./components/HoistEditor/HoistEditor";

interface Props {
    history?: any
    dispatch?: any
}

const ServicePage = (props: Props) => {
    console.log(props)

    const navBackToLanding = () => {
        props.dispatch(pageActions.navigateTo("/", ""));
    }

    return (
        <div>
            <Button onClick={navBackToLanding}>
                <HomeIcon />
            </Button>
            <HoistEditor />
        </div>
    )
}
export default connect()(ServicePage);