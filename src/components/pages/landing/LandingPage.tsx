import * as React from "react";
import { Component } from "react";
import connect from "react-redux/lib/connect/connect"
import * as pageActions from "../../../actions/pageActions";
import * as serviceActions from "../../../actions/serviceActions";
// import { Button } from "../../../../node_modules/@material-ui/core";
import PlayList from "./components/playList/PlayList";
import RaisedButton from "material-ui/RaisedButton";

interface Props {
    dispatch?: any
    history?: any
}

interface State {

}

@connect()

export default class LandingPage extends Component<Props, State> {
    constructor(props: Props) {
        super(props);

        this.state = {

        }
    }

    componentDidMount() {
        this.props.dispatch(pageActions.listPlays());
        this.props.dispatch(serviceActions.getEndTargets());
    }

    onNavToPlay = (playName: string) => {
        this.props.dispatch(pageActions.navigateTo(`/play/${playName}`, "Lejátszás"))
    }

    onNavToService = () => {
        this.props.dispatch(pageActions.navigateTo("/service", "Szervíz"))
    }

    render() {
        return (
            <div>
                <PlayList onNavToPLay={this.onNavToPlay} />
                <RaisedButton label={"Szervíz"} onClick={this.onNavToService} />
            </div>
        )
    }
}
