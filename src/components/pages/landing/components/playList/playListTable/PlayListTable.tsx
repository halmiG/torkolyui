import * as React from "react";
import { PlayInfo } from "../../../../../../reducers/pageReducer";
import Table from "../../../../../ui/Table/MUTable";
import connect from "react-redux/lib/connect/connect"
import * as pageActions from "../../../../../../actions/pageActions";
import AreYouSure from "../../../../../ui/Modal/AreYouSure/AreYouSure";
import DialogContentText from '@material-ui/core/DialogContentText';
import * as groupConfigActions from "../../../../../../actions/groupConfigActions";


interface Props {
    playList: Array<PlayInfo>
    dispatch?: any
    onNavToPLay: (playName: string) => void
}

interface State {
    sureOpen: boolean
    playToDelete: PlayInfo
}

@connect()

export default class PlayListTable extends React.Component<Props, State>{
    constructor(props: Props) {
        super(props);

        this.state = {
            sureOpen: false,
            playToDelete: {}
        }
    }

    onRowDelete = (rowData: PlayInfo) => (event) => {
        this.setState({ sureOpen: true, playToDelete: rowData });
    }

    onRowEdit = (rowData: PlayInfo) => (event) => {
        this.props.onNavToPLay(rowData.name);
        this.props.dispatch(groupConfigActions.getPlayData(rowData.name));
        this.props.dispatch(pageActions.setPlay(rowData.name));
    }

    onDeleteSure = () => {
        const { playToDelete } = this.state;
        this.props.dispatch(pageActions.deletePlay(playToDelete.name));
        this.onResetStates();
    }

    onResetStates = () => {
        this.setState({ sureOpen: false, playToDelete: {} });
    }

    render() {
        const headers = ["Név", "Készítve", "Módosítva", "Szerkesztés"];
        const dataNames = this.props.playList[0] ? ["name", "created", "modified"] : [];
        const { sureOpen } = this.state;

        return (
            <div>
                <Table
                    headers={headers}
                    bodyRows={this.props.playList}
                    bodyDataNames={dataNames}
                    onRowDelete={this.onRowDelete}
                    onRowEdit={this.onRowEdit}
                    title={"Színdarabok"}
                />
                <AreYouSure
                    onYes={this.onDeleteSure}
                    onNo={this.onResetStates}
                    open={sureOpen}
                    body={(
                        <DialogContentText>
                            Biztos törölni akarj a színdarabot, az összes mentett adatával együtt?
                        </DialogContentText>
                    )}
                />
            </div>
        );
    }
}
