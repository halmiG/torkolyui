import * as React from "react";
import Modal from "../../../../../ui/Modal/Modal";
import Input from '@material-ui/core/Input';

interface Props {
    open: boolean
    onClose: () => void
    onSave: (name: string) => void
}

interface State {
    newPlayName: string
    modalError: boolean
}

export default class PlayAddModal extends React.Component<Props, State> {
    constructor(props: Props) {
        super(props);

        this.state = {
            newPlayName: "",
            modalError: false
        }
    }

    handlePlaySave = () => {
        const { newPlayName } = this.state;
        if (newPlayName !== "") {
            this.props.onSave(newPlayName);
            this.setState({ modalError: false, newPlayName: "" });
        } else {
            this.setState({ modalError: true });
        }
    }

    handlePlayNameChange = (event) => {
        this.setState({ newPlayName: event.target.value, modalError: false });
    }

    onModalClose = () => {
        this.setState({ modalError: false, newPlayName: "" });
        this.props.onClose();
    }

    render() {
        const { open } = this.props;
        const { newPlayName, modalError } = this.state;
        const newPlayModalAtions = [
            {
                "name": "Mégse",
                "handlerFunc": this.onModalClose
            },
            {
                "name": "Kész",
                "handlerFunc": this.handlePlaySave
            }
        ];

        return (
            <Modal
                title={"Új darab hozzáadása!"}
                open={open}
                onClose={this.onModalClose}
                actions={newPlayModalAtions}
            >
                <Input
                    value={newPlayName}
                    onChange={this.handlePlayNameChange}
                    error={modalError}
                />
            </Modal>
        )
    }
}