import * as React from "react";
import RaisedButton from "material-ui/RaisedButton";
import connect from "react-redux/lib/connect/connect"
import * as pageActions from "../../../../../actions/pageActions";
import { PlayInfo } from "../../../../../reducers/pageReducer";
import PlayListTable from "./playListTable/PlayListTable";
import PlayAddModal from "./PlayAddModal/PlayAddModal";

interface Props {
    playList?: Array<PlayInfo>
    dispatch?: any
    onNavToPLay: (playName: string) => void
}

interface State {
    newPlayOpen: boolean
}

@connect((store) => {
    return {
        playList: store.pageReducer.playList
    }
})

export default class PlayList extends React.Component<Props, State> {
    constructor(props: Props) {
        super(props);

        this.state = {
            newPlayOpen: false
        }
    }

    onNewPlayClicked = () => {
        this.setState({ newPlayOpen: true });
    }

    addNewPlay = (name: string) => {
        this.props.dispatch(pageActions.addPlay(name));
        this.onNewPlayModalClose();
    }

    onNewPlayModalClose = () => {
        this.setState({ newPlayOpen: false });
    }

    render() {
        const { playList } = this.props;
        const { newPlayOpen } = this.state;

        return (
            <div>
                <RaisedButton label={"Új Színdarab"} onClick={this.onNewPlayClicked} />
                <PlayListTable playList={playList} onNavToPLay={this.props.onNavToPLay} />
                <PlayAddModal
                    open={newPlayOpen}
                    onClose={this.onNewPlayModalClose}
                    onSave={this.addNewPlay}
                />
            </div>
        )
    }
}