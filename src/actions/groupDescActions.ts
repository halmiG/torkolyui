//Actions for handling the group description of the edit group   functionality

import * as types from "./actionTypes";
import { ipcRenderer } from "electron";
import { HoistConflict } from "../reducers/groupDescReducer";
import *  as deadDescActions from "./deadDescActions";
import { GroupConfig, DeadConfig } from "../components/pages/play/components/sceneArea/models/PlayConfig";

//This is only called when Group Settings is mounted
export const loadGroup = (groupToLoad: GroupConfig) => dispatch => {
    dispatch({ type: types.GROUP_DESC_LOAD_GROUP, groupToLoad });
}

export const changeParamOfGroup = (paramName: any, newValue: any) => dispatch => {
    dispatch({ type: types.GROUP_DESC_CHANGE_PARAMETER, paramName, newValue });
}

export const addHoistToGroup = (hoistName: number) => dispatch => {
    //TODO make call to the bakend here, to check if the hoist exist in an other group too or not? 
    ipcRenderer.send("check-hoist-conflict", hoistName);
    ipcRenderer.once(`check-hoist-conflict-result-${hoistName}`, (event, error, conflictedGroups) => {
        if (error) {
            console.error("Error in checking hoist conflict:", error);
        } else if (conflictedGroups.length) {
            console.warn("There is conflict dude", ...conflictedGroups)
            let hoistConflict = {
                "hoist": hoistName,
                "groups": [...conflictedGroups]
            };
            dispatch({ type: types.GROUP_DESC_ADD_HOIST_TO_GROUP_CONFLICT, hoistConflict });
        } else { // everything is okay there is no conflict
            dispatch({ type: types.GROUP_DESC_ADD_HOIST_TO_GROUP, hoistName });
        }
    });
}

// this is called when there was a conflict, meanin the chosen hoist(s) were already 
// in another group, but the user approved this situation
export const approveGroupConflict = (conflictHoistList: Array<HoistConflict>) => dispatch => {
    conflictHoistList.forEach(conflict => {
        let hoistName = conflict.hoist;
        dispatch({ type: types.GROUP_DESC_ADD_HOIST_TO_GROUP, hoistName });
    });
    dispatch(clearGroupConflict());
}

export const clearGroupConflict = () => dispatch => {
    dispatch({ type: types.GROUP_DESC_ADD_HOIST_TO_GROUP_CLEAR_CONFLICT });
}

export const removeHoistFromGroup = (hoistName: number, groupID: string) => dispatch => {
    // TODO ADS send remove message to the PLC
    dispatch(deadDescActions.removeHositFromDead(hoistName, groupID));
    dispatch({ type: types.GROUP_DESC_REMOVE_HOIST, hoistName });
}

// this is called when the GroupSettings in umounting
export const clearGroup = () => dispatch => {
    dispatch({ type: types.GROUP_DESC_CLEAR_GROUP });
}

export const addDeadToGroup = (dead: DeadConfig) => dispatch => {
    const deadToUpdate = {
        id: dead.id,
        name: dead.name
    };
    dispatch({ type: types.GROUP_DESC_ADD_DEAD, deadToUpdate });
}

export const updateDeadInGroup = (dead: DeadConfig) => dispatch => {
    const deadToUpdate = {
        id: dead.id,
        name: dead.name
    };
    dispatch({ type: types.GROUP_DESC_UPDATE_DEAD, deadToUpdate });
}

export const removeDeadFromGroup = (deadToUpdate: { id: string, name: string }) => dispatch => {
    dispatch({ type: types.GROUP_DESC_REMOVE_DEAD, deadToUpdate });
}
