import * as types from "./actionTypes";
import * as APIscene from "../API/scene";
import { ipcRenderer } from "electron";
import { SceneConfig } from "../components/pages/play/components/sceneArea/models/PlayConfig";

export const getNumberOfScenes = () => async dispatch => {
    try {
        let numberOfScenes = await APIscene.getNumberOfScenes();
        dispatch({ type: types.GET_NUMBER_OF_SCENES_SUCCES, numberOfScenes });
    } catch (error) {
        console.error("could not get the number of scenes");
        dispatch({ type: types.GET_NUMBER_OF_SCENES_ERROR });
    }
}

export const addNewEmptyScene = () => dispatch => {
    dispatch({ type: types.ADD_NEW_EMPTY_SCENE });
}

export const getAllSceneData = () => async dispatch => {
    try {
        dispatch({ type: types.GET_ALL_SCENE_DATA_PROGRESS });
        let allSceneData = await APIscene.getAllSceneData();
        dispatch({ type: types.GET_ALL_SCENE_DATA_SUCCESS, allSceneData });
    } catch {
        dispatch({ type: types.GET_ALL_SCENE_DATA_ERROR });
        console.error("Couldnt get all scene data!");
    }
}

export const saveAllSceneData = () => async (dispatch, getState) => {
    try {
        dispatch({ type: types.SAVE_ALL_SCENE_DATA_PROGRESS });
        await APIscene.saveAllSceneData(getState().playDataReducer.allSceneData);
        dispatch({ type: types.SAVE_ALL_SCENE_DATA_SUCCESS });
    } catch (error) {
        dispatch({ type: types.SAVE_ALL_SCENE_DATA_ERROR });
        console.log("Could not save all the scene data!", error);
    }
}

export const setCurrentScene = (currentScene: number) => dispatch => {
    dispatch({ type: types.SET_CURRENT_SCENE, currentScene });
}

export const removeDeadFromScene = (sceneIndex: number, deadIndex: number) => dispatch => {
    dispatch({ type: types.REMOVE_DEAD_FROM_SCENE, sceneIndex, deadIndex });
}

export const removeScene = (sceneIndex: number) => dispatch => {
    dispatch({ type: types.REMOVE_SCENE, sceneIndex });
}

export const addJoyToGroup = (sceneIndex: number, groupIndex: number, targetJoy: number) => dispatch => {
    dispatch({ type: types.ADD_JOY_TO_GROUP_IN_SCENE, sceneIndex, groupIndex, targetJoy });
}

export const addGroupWithDead = (deadID: string, sceneDesc: SceneConfig) => dispatch => {
    ipcRenderer.send("get-group-from-dead", deadID, sceneDesc);
    ipcRenderer.once("get-group-from-dead-result", (event, error, groupDesc, chosenDead, conflictedGroups) => {
        if (error) {
            console.error("nem találta meg sjanson")
        } else {
            if (conflictedGroups.length) {
                dispatch({ type: types.SCENE_DESCRIPTION_GROUP_CONFLICT, conflictedGroups })
            } else {
                dispatch({ type: types.SCENE_DECRIPTION_GROUP_ADDED, groupDesc, chosenDead, sceneID: sceneDesc.id });
            }
        }
    });
}

export const clearConflictedGroup = () => dispatch => {
    dispatch({ type: types.SCENE_DESCRIPTION_GROUP_CONFLICT_CLEAR });
}