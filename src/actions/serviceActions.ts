import * as types from "./actionTypes";
import * as api from "../API/api";
import { HoistConfig } from "../components/pages/play/components/sceneArea/models/PlayConfig";

export const setEndTargets = (hoist: string, topPos: string, botPos: string) => async dispatch => {
    const newHoistTargets: HoistConfig = {
        "id": Number(hoist),
        "targets": {
            "EPT": {
                "value": Number(topPos)
            },
            "EPB": {
                "value": Number(botPos)
            }
        }
    };
    try {
        await api.setEndTargets(newHoistTargets);
        console.log("TARGETS SET!")
        dispatch({ type: types.SET_TARGETS_SUCCES });
    } catch (error) {
        console.error("Could not set the hois targets:", error);
    }
}

export const getEndTargets = () => async dispatch => {
    try {
        let endTargets = await api.getEndTargets();
        dispatch({ type: types.GET_END_TARGETS_SUCCESS, endTargets });
    } catch (error) {
        console.error("could not get the ned targets of hoists");
    }
}