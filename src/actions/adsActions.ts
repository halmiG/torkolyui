import * as types from './actionTypes';
import { ipcRenderer } from 'electron';
//import ipcPromise from 'ipc-promise';

export const startADS = () => dispatch => {
    console.log("ADS connect launched");
    ipcRenderer.send("start-ads");
    dispatch({type: types.ADS_INIT_LOADING});
    ipcRenderer.once("ads-connected", event => {
        console.log("ADS is connected");
        dispatch({type: types.ADS_INIT_SUCCESS});
        //this.setState({connectResult: true}); // use this in the Reducer
        // now subscribe here for the array ? 
    });
}

export const stopADS = () => dispatch => {
    console.log("ADS disconnect is launched");
    ipcRenderer.send("stop-ads");
    dispatch({type: types.ADS_STOP_LOADING});
    ipcRenderer.once("ads-disconnected", event => {
        console.log("ADS is disconnected");
        dispatch({type: types.ADS_STOP_SUCCESS});
        //this.setState({connectResult: false});
        // now subscribe here for the array ? 
    });
}

export const listenToADSData = () => dispatch => {
    ipcRenderer.on("ads-data", (event, dataArray) => {

        dispatch(dataReceived(dataArray));
        //console.timeEnd("send")
        // this.setState({ ...this.state, actPoses: dataArray });
     });
     ipcRenderer.on("ads-bit-signs", (event, bitsData) => {
         dispatch(signBitsReceived(bitsData));
     })
}

export const clearADSListener = () => dispatch => {
    console.log("clear listening to data from ADS");
    ipcRenderer.removeAllListeners("ads-data");
    ipcRenderer.removeAllListeners("ads-bit-signs");
}

export const dataReceived = (dataArray: Array<string>) => dispatch => {
    dispatch({type: types.ADS_ACTPOS_RECEIVED, dataArray})
}

export const signBitsReceived = (signBitsArray: Array<string>) => dispatch => {
    dispatch({type: types.ADS_SIGNBITS_RECEIVED, signBitsArray});
}
