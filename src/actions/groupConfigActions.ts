import * as types from './actionTypes';
import { ipcRenderer } from 'electron';
import * as deadDescActions from "./deadDescActions";
import { GroupConfig } from '../components/pages/play/components/sceneArea/models/PlayConfig';

export const getPlayData = (play) => dispatch => {
    ipcRenderer.send("read-play", play);
    dispatch({ type: types.GETTING_PLAY_LOADING });
    ipcRenderer.once("play-data-read-result", (event, error) => {
        if (error) {
            dispatch({ type: types.GETTING_PLAY_FAILURE, error });
        } else {
            dispatch(getGroupsForPlay());
            dispatch({ type: types.GETTING_PLAY_SUCCES })
        }
    });

}

export const getGroupsForPlay = () => dispatch => {
    dispatch({ type: types.GET_GROUPS_LOADING });
    ipcRenderer.send("get-groups-for-play");
    ipcRenderer.once("groups-for-play-result", (event, groupsObj) => {
        dispatch({ type: types.GET_GROUPS_SUCCESS, groupsObj });
        dispatch(deadDescActions.loadDeads());
    })
}

export const saveGroupForPlay = (groupDesc: GroupConfig, groupToEdit: string) => dispatch => {
    dispatch({ type: types.SAVE_GROUP_LOADING });
    ipcRenderer.send("save-groups-for-play", groupDesc, groupToEdit);
    ipcRenderer.once("save-groups-for-play-result", (event, globalGroups) => {
        if (globalGroups) {
            //if we made changes on the group, call the getSceneData to be updated
            dispatch({ type: types.SAVE_GROUP_SUCCESS, globalGroups });
        } else {
            //TODO MAKE ERROR HANDLER
            dispatch({ type: types.SAVE_GROUP_FAIL });
        }
    });
}

export const deleteGroupFromPlay = (groupToDelete: string) => dispatch => {
    dispatch({ type: types.DELETE_GROUP_PROGRESS });
    ipcRenderer.send("delete-group-from-play", groupToDelete);
    ipcRenderer.once("delete-group-from-play-result", (event, error, globalGroups, globalDeads) => {
        if (error) {
            dispatch({ type: types.DELETE_GROUP_FAIL });
            console.error("Delete group failed with error: ", error);
        } else {
            dispatch({ type: types.DELETE_GROUP_SUCCESS, globalGroups });
            dispatch({ type: types.DEAD_UPDATE_AFTER_DELETE, globalDeads });
        }
    });
}

export const switchHoistAddMode = (groupID: string) => dispatch => {
    dispatch({ type: types.GROUP_CONFIG_SWITCH_HOIST_ADD_MODE, groupID });
}