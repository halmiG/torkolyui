import * as types from "./actionTypes";
import { ipcRenderer } from "electron";
import { DeadConfig } from "../components/pages/play/components/sceneArea/models/PlayConfig";

export const addDead = (deadToAdd: DeadConfig) => dispatch => {
    dispatch({ type: types.DEAD_DESC_ADD_DEAD, deadToAdd });
}

export const updateDead = (updatedDeadDesc: DeadConfig) => dispatch => {
    //TODO somehow indicate this to be able to update in the play too
    dispatch({ type: types.DEAD_DESC_UPDATE_DEAD, updatedDeadDesc });
}

export const removeDead = (deadToRemove: { id: string, name: string }) => dispatch => {
    //TODO indicate dead delete to be able to delete from play too
    dispatch({ type: types.DEAD_DESC_REMOVE_DEAD, deadToRemove });
}

export const removeHositFromDead = (hoistID: number, groupID: string) => dispatch => {
    dispatch({ type: types.DEAD_DESC_REMOVE_HOIST, hoistID, groupID });
}

export const changeGroupNameInDead = (groupID: string, newName: string) => dispatch => {
    dispatch({ type: types.DEAD_DESC_CHANGE_GROUP_NAME, groupID, newName });
}

export const loadDeads = () => dispatch => {
    console.log("LOAD")
    ipcRenderer.send("get-deads");
    ipcRenderer.once("get-deads-result", (event, error, deadsArray: Array<DeadConfig>) => {
        if (error) {
            dispatch({ type: types.DEAD_DESC_GETTING_ERROR });
            console.error("Error in loading the deads");
        } else {
            dispatch({ type: types.DEAD_DESC_GETTING_SUCCESS, deadsArray });
        }
    });
}

export const saveDeads = () => (dispatch, getState) => {
    const deadDescArray = getState().deadDescReducer.deadDescBuffer;

    ipcRenderer.send("save-deads", deadDescArray);
    ipcRenderer.once("save-deads-result", (event, error) => {
        if (error) {
            console.error("Error in saving the deads", error);
            dispatch({ type: types.DEAD_DESC_SAVE_DEADS_ERROR });
        } else {
            dispatch({ type: types.DEAD_DESC_SAVE_DEADS_SUCCES });
        }
    });
}


