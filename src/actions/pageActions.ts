import * as types from './actionTypes';
import * as api from "../API/api";
import { history } from "../store/configureStore";

export const navigateTo = (path: string, title: string) => dispatch => {
    // if we go back to the landing page, we should clear the cache, hence they are related to a chosen play
    if (path === "/") {
        api.clearPlayCache();
    }
    history.push(path);
    dispatch({ type: types.PAGE_ROUTE, title });
}

export const listPlays = () => async dispatch => {
    try {
        const playList = await api.listPlays();
        dispatch({ type: types.GOT_PLAY_LIST, playList });
    } catch (error) {
        dispatch({ type: types.ERROR_PLAY_LIST });
        console.error("Error in reading the Plays!", error);
    }
}

export const addPlay = (name) => async dispatch => {
    try {
        await api.createPlay(name);
        dispatch(listPlays());
    } catch (error) {
        //TODO error dispatch
        console.error("couldnt add play", error);
    }
}

export const setPlay = (name: string) => async dispatch => {
    try {
        await api.setPlay(name);
        dispatch({ type: types.SET_PLAY_SUCCESS, playName: name });
    } catch {
        //TODO error dispatch
        console.error("for some reason it couldnt set the Play to", name);
    }
}

export const deletePlay = (name: string) => async dispatch => {
    try {
        await api.deletePlay(name);
        dispatch(listPlays());
        dispatch({ type: types.DELETE_PLAY_SUCCES });
    } catch (error) {
        //TODO error dispatch
        console.log("Error in deleting the page: ", error);
    }
}