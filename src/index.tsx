import * as React from 'react';
import * as ReactDOM from 'react-dom';
//import { HashRouter as Router, Route } from 'react-router-dom';
import { configureStore, history } from './store/configureStore';
import { Provider } from 'react-redux';
import { ConnectedRouter } from 'react-router-redux';
import Routes from './routes';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import getMuiTheme from 'material-ui/styles/getMuiTheme';
//import 'typeface-roboto/index.css'
require('typeface-roboto')

const store = configureStore();

//HOWTO: http://www.material-ui.com/#/customization/themes
const muiTheme = getMuiTheme({
    palette: {
        accent1Color: "black",
    }
});

ReactDOM.render(
    <MuiThemeProvider muiTheme={muiTheme}>
        {/* <React.StrictMode> */}
        <Provider store={store}>
            <ConnectedRouter history={history} >
                <Routes />
            </ConnectedRouter>
        </Provider>
        {/* </React.StrictMode> */}
    </MuiThemeProvider>,
    document.getElementById('root')
);
