/* eslint flowtype-errors/show-errors: 0 */
import * as React from 'react';
import { Switch, Route } from 'react-router';
import App from './components/App';
import LandingPage from './components/pages/landing/LandingPage';
import PlayPage from "./components/pages/play/PlayPage";
import ServicePage from "./components/pages/service/ServicePage";

export default () => (
  <App>
    <Switch>
      <Route exact path='/' component={LandingPage} />
      <Route path="/play/:playName" component={PlayPage} />
      {/* <Route path="/config" component={ConfigPage} /> */}
      <Route path="/landing" component={LandingPage} />
      <Route path="/service" component={ServicePage} />
    </Switch>
  </App>
);
