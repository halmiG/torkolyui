import * as types from "../actions/actionTypes";
import { SceneConfig, GroupConfig, DeadConfig } from "../components/pages/play/components/sceneArea/models/PlayConfig";
import produce from "immer";
import { v4 } from 'uuid';

interface State {
    numberOfScenes: number
    allSceneData: Array<SceneConfig>
    currentScene: number
    conflictedGroups: Array<{ name: string, id: number }>
    scenesUnsaved: boolean
}

interface Action {
    type: any
    numberOfScenes: number
    allSceneData: Array<SceneConfig>
    currentScene: number
    groupDesc: GroupConfig
    chosenDead: DeadConfig
    sceneID: string
    deadIndex: number
    sceneIndex: number
    groupIndex: number
    targetJoy: number
    conflictedGroups: Array<{ name: string, id: number }>
}

const defaultState: State = {
    numberOfScenes: null,
    allSceneData: [],
    currentScene: 0,
    conflictedGroups: [],
    scenesUnsaved: false
}

const defaultSceneData: SceneConfig = {
    id: "",
    name: "",
    groups: [],
    deads: []
}

const playDataReducer = (state: State = defaultState, action: Action): State =>
    produce(state, draft => {
        switch (action.type) {
            case types.GET_NUMBER_OF_SCENES_SUCCES:
                draft.numberOfScenes = action.numberOfScenes;
                return;
            case types.GET_ALL_SCENE_DATA_PROGRESS:
                //TODO            
                return;
            case types.GET_ALL_SCENE_DATA_ERROR:
                //TODO            
                return;
            case types.GET_ALL_SCENE_DATA_SUCCESS:
                draft.allSceneData = action.allSceneData;
                return;
            case types.ADD_NEW_EMPTY_SCENE:
                const newScene = {
                    ...defaultSceneData,
                    id: v4()
                }
                draft.allSceneData.push(newScene);
                draft.numberOfScenes = draft.allSceneData.length;
                draft.scenesUnsaved = true;
                return;
            case types.SAVE_ALL_SCENE_DATA_PROGRESS:
                return;
            case types.SAVE_ALL_SCENE_DATA_ERROR:
                return;
            case types.SAVE_ALL_SCENE_DATA_SUCCESS:
                draft.scenesUnsaved = false;
                return;
            case types.SET_CURRENT_SCENE:
                draft.currentScene = action.currentScene;
                return;
            case types.SCENE_DECRIPTION_GROUP_ADDED:
                let sceneIndex;
                let sceneToModify = draft.allSceneData.find((scene, i) => {
                    if (scene.id === action.sceneID) {
                        sceneIndex = i;
                        return true;
                    }
                    return false;
                });
                sceneToModify.groups.push(action.groupDesc);
                sceneToModify.deads.push(action.chosenDead);
                draft.allSceneData[sceneIndex] = sceneToModify;
                draft.scenesUnsaved = true;
                return;
            case types.REMOVE_DEAD_FROM_SCENE:
                draft.allSceneData[action.sceneIndex].groups.splice(action.deadIndex, 1);
                draft.allSceneData[action.sceneIndex].deads.splice(action.deadIndex, 1);
                draft.scenesUnsaved = true;
                return;
            case types.REMOVE_SCENE:
                draft.allSceneData.splice(action.sceneIndex, 1);
                draft.scenesUnsaved = true;
                return;
            case types.SCENE_DESCRIPTION_GROUP_CONFLICT:
                draft.conflictedGroups = action.conflictedGroups;
                return;
            case types.SCENE_DESCRIPTION_GROUP_CONFLICT_CLEAR:
                draft.conflictedGroups = [];
                return;
            case types.ADD_JOY_TO_GROUP_IN_SCENE:
                draft.allSceneData[action.sceneIndex].groups[action.groupIndex].joystick = action.targetJoy;
                draft.scenesUnsaved = true;
                return;
            default:
                return;
        }
    })
export default playDataReducer;