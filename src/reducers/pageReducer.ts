import * as types from '../actions/actionTypes';

export interface PlayInfo {
    name?: string
    created?: string
    modified?: string
}

interface State {
    currentPageName: string
    playList: Array<string>
    setPlay: string
}

interface Action {
    type: string
    title: string
    playList: Array<PlayInfo>
    playName: string
}

const defaultState: State = {
    currentPageName: "",
    playList: [],
    setPlay: ""
}

export default function pageReducer(state: State = defaultState, action: Action) {
    switch (action.type) {
        case types.PAGE_ROUTE:
            return {
                ...state,
                currentPageName: action.title
            }
        case types.GOT_PLAY_LIST:
            return {
                ...state,
                playList: action.playList
            }
        case types.SET_PLAY_SUCCESS:
            return {
                ...state,
                setPlay: action.playName
            }
        default:
            return state;
    }
}