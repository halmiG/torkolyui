import * as types from "../actions/actionTypes";
import produce from "immer";
import { GroupConfig, defaultGroupConfig, HoistConfig } from "../components/pages/play/components/sceneArea/models/PlayConfig";

export interface HoistConflict {
    hoist: string
    groups: Array<{
        name: string
        id: number
    }>
}

interface State {
    groupDescBuffer: GroupConfig
    errorAddDead: boolean
    groupConflicts: Array<HoistConflict>
    groupSettingsMode: boolean // this is the flag thtat shows of the UI is in the GroupSettings or not
}

interface Action {
    groupToLoad: GroupConfig
    hoistName: number
    type: any
    paramName: any
    newValue: any
    deadToUpdate: { id: string, name: string }
    hoistConflict: HoistConflict
}

const defaultState: State = {
    groupDescBuffer: defaultGroupConfig,
    errorAddDead: false,
    groupConflicts: [],
    groupSettingsMode: false
}


const groupDescReducer = (state: State = defaultState, action: Action): State =>
    produce(state, draft => {
        switch (action.type) {
            case types.GROUP_DESC_LOAD_GROUP:
                draft.groupDescBuffer = action.groupToLoad;
                draft.groupSettingsMode = true;
                return;
            case types.GROUP_DESC_ADD_HOIST_TO_GROUP:
                let newHoist: HoistConfig = {
                    id: action.hoistName,
                    targets: {}
                }
                draft.groupDescBuffer.hoists.push(newHoist);
                return;
            case types.GROUP_DESC_ADD_HOIST_TO_GROUP_CONFLICT:
                draft.groupConflicts.push(action.hoistConflict);
                return;
            case types.GROUP_DESC_ADD_HOIST_TO_GROUP_CLEAR_CONFLICT:
                draft.groupConflicts = [];
                return;
            case types.GROUP_DESC_CLEAR_GROUP:
                draft.groupDescBuffer = defaultState.groupDescBuffer;
                draft.groupSettingsMode = false;
                return;
            case types.GROUP_DESC_CHANGE_PARAMETER:
                draft.groupDescBuffer[action.paramName] = action.newValue;
                return;
            case types.GROUP_DESC_REMOVE_HOIST:
                draft.groupDescBuffer.hoists = draft.groupDescBuffer.hoists.filter(hoist => hoist.id !== action.hoistName);
                return;
            case types.GROUP_DESC_ADD_DEAD:
                if (draft.groupDescBuffer.deads.find(dead => dead.id === action.deadToUpdate.id)) { // if the name already exist
                    draft.errorAddDead = true;
                } else {
                    draft.errorAddDead = false;
                    draft.groupDescBuffer.deads.push(action.deadToUpdate);
                }
                return;
            case types.GROUP_DESC_UPDATE_DEAD:
                draft.groupDescBuffer.deads.map(dead => {
                    let resultDead = dead;
                    if (dead.id === action.deadToUpdate.id && dead.name !== action.deadToUpdate.name) { // if the namse of the dead updated
                        dead.name = action.deadToUpdate.name;
                    }
                    return resultDead;
                });
                return;
            case types.GROUP_DESC_REMOVE_DEAD:
                draft.groupDescBuffer.deads = draft.groupDescBuffer.deads.filter(dead => dead.id !== action.deadToUpdate.id);
                return;
        }
    })

export default groupDescReducer;