import { combineReducers } from 'redux';
import { routerReducer } from "react-router-redux";
import adsReducer from "./adsReducer";
import groupConfigReducer from "./groupConfigReducer";
import groupDescReducer from "./groupDescReducer";
import deadDescReducer from "./deadDescReducer";
import pageReducer from "./pageReducer";
import serviceReducer from "./serviceReducer";
import playDataReducer from "./playDataReducer";

const rootReducer = combineReducers({
    routerReducer,
    adsReducer,
    groupConfigReducer,
    groupDescReducer,
    deadDescReducer,
    pageReducer,
    serviceReducer,
    playDataReducer
});

export default rootReducer;