import * as types from '../actions/actionTypes';
import { GroupConfig, TargetConfig } from '../components/pages/play/components/sceneArea/models/PlayConfig';

interface State {
    playLoading: boolean
    gotPlayData: boolean
    addHoistModeGroupID: string
    savingGroup: boolean
    savingError: boolean
    globalGroups: Array<GroupConfig>
    targets: Array<TargetConfig>
}

const defaultState: State = {
    playLoading: false,
    gotPlayData: false,
    addHoistModeGroupID: "",
    savingGroup: false,
    savingError: false,
    globalGroups: [],
    targets: []
}

export default function groupConfigReducer(state: State = defaultState, action: any): State {
    switch (action.type) {
        case types.GETTING_PLAY_LOADING:
            return { ...state, playLoading: true }
        case types.GETTING_PLAY_SUCCES:
            return {
                ...state,
                playLoading: false,
                gotPlayData: true
            }
        case types.GETTING_PLAY_FAILURE:
            return {
                ...state
            }
        case types.GROUP_CONFIG_SWITCH_HOIST_ADD_MODE:
            if (state.addHoistModeGroupID && !action.groupID) { // this means switch OFF the addMode
                return {
                    ...state,
                    addHoistModeGroupID: ""
                }
            } else { // this is the switch ON case
                return {
                    ...state,
                    addHoistModeGroupID: action.groupID
                }
            }
        case types.GET_GROUPS_SUCCESS:
            return {
                ...state,
                globalGroups: action.groupsObj.groups
            }
        case types.SAVE_GROUP_LOADING:
            return {
                ...state,
                savingGroup: true,
                savingError: false
            }
        case types.SAVE_GROUP_SUCCESS:
            return {
                ...state,
                globalGroups: action.globalGroups.groups,
                savingGroup: false,
                savingError: false
            }
        case types.SAVE_GROUP_FAIL:
            //TODO ERROR HANDLING
            return {
                ...state,
                savingGroup: false,
                savingError: true
            };
        case types.GET_TARGETS_SUCCESS:
            return {
                ...state,
                targets: action.targetsObj
            }
        case types.GET_TARGETS_FAIL:
            //TODO error handling
            return state;
        case types.DELETE_GROUP_PROGRESS:
            //TODO
            return state;
        case types.DELETE_GROUP_FAIL:
            //TODO
            return state;
        case types.DELETE_GROUP_SUCCESS:
            return {
                ...state,
                globalGroups: action.globalGroups.groups
            }
        default:
            return state;
    }
}