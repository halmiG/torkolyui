import * as types from "../actions/actionTypes";
import produce from "immer";
import { DeadConfig } from "../components/pages/play/components/sceneArea/models/PlayConfig";

interface State {
    deadDescBuffer: Array<DeadConfig>
}

interface Action {
    deadsArray: Array<DeadConfig>
    type: any
    deadToAdd: DeadConfig
    updatedDeadDesc: DeadConfig
    deadToRemove: { id: string, name: string }
    groupID: string
    hoistID: number
    newName: string
    globalDeads: Array<DeadConfig>
}

const defaultState: State = {
    deadDescBuffer: []
}

const deadDescReducer = (state: State = defaultState, action: Action): State =>
    produce(state, draft => {
        switch (action.type) {
            case types.DEAD_DESC_GETTING_SUCCESS:
                draft.deadDescBuffer = action.deadsArray;
                //check if any of it is missing
                return;
            case types.DEAD_DESC_ADD_DEAD:
                draft.deadDescBuffer.push(
                    {
                        ...action.deadToAdd,
                        posHoists: action.deadToAdd.posHoists.filter(posHoist => posHoist.value !== "")
                    }
                );
                return;
            case types.DEAD_DESC_UPDATE_DEAD:
                draft.deadDescBuffer = draft.deadDescBuffer.map(deadDesc => {
                    let resultDesc = deadDesc;
                    if (deadDesc.id === action.updatedDeadDesc.id) { // search for the updated deadDesc
                        resultDesc = {
                            ...action.updatedDeadDesc,
                            posHoists: action.updatedDeadDesc.posHoists.filter(posHoist => posHoist.value !== "")
                        }
                    }
                    return resultDesc;
                });
                return;
            case types.DEAD_DESC_REMOVE_DEAD:
                draft.deadDescBuffer = draft.deadDescBuffer.filter(deadDesc => deadDesc.id !== action.deadToRemove.id);
                return;
            case types.DEAD_DESC_REMOVE_HOIST:
                draft.deadDescBuffer = draft.deadDescBuffer.map(dead => {
                    if (dead.group.groupID === action.groupID) {
                        dead.posHoists = dead.posHoists.filter(posHoist => posHoist.id !== action.hoistID);
                    }
                    return dead;
                });
                return;
            case types.DEAD_DESC_CHANGE_GROUP_NAME:
                draft.deadDescBuffer = draft.deadDescBuffer.map(deadDesc => {
                    if (deadDesc.group.groupID === action.groupID) {
                        deadDesc.group.groupName = action.newName;
                        return deadDesc;
                    } else {
                        return deadDesc;
                    }
                });
                return;
            case types.DEAD_UPDATE_AFTER_DELETE:
                draft.deadDescBuffer = action.globalDeads;
                return;
        }
    })

export default deadDescReducer;