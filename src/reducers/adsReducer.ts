import * as types from '../actions/actionTypes';

interface State {
    adsConnected: boolean,
    actPosValues: Array<string>,
    signBits: Array<string>
}

const defaultState: State = {
    adsConnected: false,
    actPosValues: [],
    signBits: []
}

export default function adsReducer(state: State = defaultState, action: any): State {
    switch(action.type) {
        case types.ADS_INIT_SUCCESS:
            console.log("ADS server is connected!");
            return {
                ...state,
                adsConnected: true
            }

        case types.ADS_STOP_SUCCESS:
            console.log("ADS server is disconnected!");
            return {
                ...state,
                adsConnected: false
            }
        
        case types.ADS_ACTPOS_RECEIVED:
        //console.log("ads data in reducer: ", action) 
            return {
                ...state,
                actPosValues: action.dataArray
            }
        case types.ADS_SIGNBITS_RECEIVED:
            return {
                ...state,
                signBits: action.signBitsArray
            }
        default: 
            return state;
    }
}