import * as types from '../actions/actionTypes';
import { HoistConfig } from '../components/pages/play/components/sceneArea/models/PlayConfig';

interface State {
    endTargets: Array<HoistConfig>
}

interface Action {
    type: string
    endTargets: Array<HoistConfig>
}

const defaultState: State = {
    endTargets: []
}

export default function serviceReducer(state: State = defaultState, action: Action) {
    switch (action.type) {
        case types.SET_TARGETS_SUCCES:
            return {
                ...state
            };
        case types.GET_END_TARGETS_SUCCESS:
            return {
                ...state,
                endTargets: action.endTargets
            }
        default:
            return state;
    }
}