/**
 * Generating ID randomly from 0 to 32000
 */
export const generateID = (compareArray: Array<any>): number => {
    let generateID = null;
    while (generateID === null) {
        let randomNumber = Math.floor(Math.random() * Math.floor(32000));
        if (compareArray.find(element => element.id === randomNumber)) {
            continue;
        } else {
            generateID = randomNumber;
        }
    }
    return generateID;
}

export const generateColor = (offSet: number): string => {
    //TODO expand with more colors, and not set it back
    while (offSet > 5) {
        offSet = offSet - 6;
    }
    const BASE_COLORS = [
        0xFF0000, //piros
        0xFFFF00, // sárga
        0x00FF00, // zöld
        0x00FFFF, // türkíz
        0x0000FF, // kék
        0xFF00FF // pink
    ]

    return "#" + BASE_COLORS[offSet].toString(16);
}