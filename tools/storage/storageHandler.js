const storage = require('fs-jetpack');

module.exports.readPlay = (playName, returnEvent) => {
    storage.readAsync(`storage/plays/${playName}/${playName}.json`, "json")
        .then((play) => {
            returnEvent(play)
        }).catch((error) => {
            returnEvent(null, error)
            console.error("Error in reading the play");
        });
}

module.exports.updatePlay = (playDescription, returnEvent) => {
    storage.writeAsync(`storage/plays/${playDescription.name}/${playDescription.name}.json`, playDescription)
        .then(() => {
            console.log('write is done');
            returnEvent(null);
        }).catch((error) => {
            returnEvent(error);
        });
}

//Groups handler
module.exports.readGroups = (playName, returnEvent) => {
    storage.readAsync(`storage/plays/${playName}/${playName}_groups.json`, "json")
        .then((groups) => {
            returnEvent(groups)
        }).catch((error) => {
            returnEvent(null, error)
            console.error("Error in reading the groups", error);
        });
}

module.exports.saveGroups = (playName, groupsToSave, returnEvent) => {
    storage.writeAsync(`storage/plays/${playName}/${playName}_groups.json`, groupsToSave)
        .then(() => {
            returnEvent(null);
        }).catch((error) => {
            console.error("Error in saving the groups");
            returnEvent(error);
        });
}

module.exports.readPlayList = (returnEvent) => {
    storage.readAsync("storage/plays/list.json", "json")
        .then((listData) => {
            if (listData !== undefined) {
                returnEvent(null, listData);
            } else {
                storage.writeAsync("storage/plays/list.json", [])
                    .then(() => {
                        returnEvent(null, []);
                    }).catch((error) => {
                        console.error("Couldnt create the new list.json");
                        returnEvent(error);
                    });
            }
        }).catch((error) => {
            console.error("Couldn't check if playList exists..", error);
            returnEvent(error);
        });
};

module.exports.writePlayList = (list, returnEvent) => {
    storage.writeAsync("storage/plays/list.json", list)
        .then(() => {
            returnEvent(null);
        }).catch((error) => {
            console.error("Couldnt create the new list.json");
            returnEvent(error);
        });
}

//Generic Read 
module.exports.readFile = (playName, fileType, returnEvent) => {
    storage.readAsync(`storage/plays/${playName}/${playName}_${fileType}.json`, "json")
        .then((content) => {
            returnEvent(content);
        }).catch((error) => {
            returnEvent(null, error);
            console.error("Error in reading the file: ", fileType, "with error: ", error);
        });
};

module.exports.updateFile = (playName, dataToWrite, dataType, returnEvent) => {
    storage.writeAsync(`storage/plays/${playName}/${playName}_${dataType}.json`, dataToWrite)
        .then(() => {
            returnEvent(null);
        }).catch((error) => {
            returnEvent(error);
        });
};

module.exports.createFile = (playName, dataType, data, returnEvent) => {
    // if we have datatype create the supper files, else the main play description
    const fileName = dataType ? `${playName}_${dataType}.json` : `${playName}.json`;
    storage.write(`storage/plays/${playName}/${fileName}`, data);
    returnEvent();
};

module.exports.deletePlayData = (name, callback) => {
    storage.removeAsync(`storage/plays/${name}`)
        .then(() => {
            callback();
        }).catch((error) => {
            callback(error);
        });
};

module.exports.initService = () => {
    storage.write("storage/service/hoists.json", []);
    return;
};

module.exports.serviceRead = (dataType, callback) => {
    storage.readAsync(`storage/service/${dataType}.json`, "json")
        .then((content) => {
            callback(null, content);
        }).catch((error) => {
            callback(error);
            console.error("Error in reading service file: ", dataType, "with error: ", error);
        });
}

module.exports.serviceWrite = (dataType, dataToWrite, callback) => {
    storage.writeAsync(`storage/service/${dataType}.json`, dataToWrite)
        .then(() => {
            callback(null);
        }).catch((error) => {
            callback(error);
        });
}