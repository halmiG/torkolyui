let fs = require("fs");
let cheerio = require("cheerio");
let colors = require("colors");

fs.readFile("src/index.html", "utf8", (err, markup) => {
    if(err) {
        return console.log(err);
    }

    const $ = cheerio.load(markup);

    $("head").prepend("<link rel=\"stylesheet\" href=\"styles.css\">");
    $("script").replaceWith("<script src=\"bundle.js\"></script>");

    fs.writeFile("dist/index.html", $.html(), "utf8", function(err) {
        if(err) {
            return console.log(err);
        }
        console.log("index.html written to /dist".green);
    });
});