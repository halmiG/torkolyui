const ads = require("./adsFunctions");


module.exports.startADS = (mainWindow) => {
    let res = ads.initConnection();
    const {signBitSubsriber, posSubscriber, requester} = res;
        
    posSubscriber.subscribe(""); // now I subscribe for all the messages
    posSubscriber.on("message", (dataBuffer) => {
        //console.log("called ")
        //console.time("msg")
        let dataArray = ads.hexToBytes(dataBuffer.toString("hex"));
        //console.time("send")
        mainWindow.send("ads-data", dataArray);
        //console.timeEnd("msg")
    });

    signBitSubsriber.subscribe("");
    signBitSubsriber.on("message", (dataBuffer) => {
        let bitsData = ads.hexToBytes(dataBuffer.toString("hex"));
        mainWindow.send("ads-bit-signs", bitsData);
    })

    ads.startMonitor(requester, (error) => {
        if(error) {
            console.error("Error in startMonitor", error);
        } else {
            mainWindow.send("ads-connected");
        }
    });
    
    return {requester, posSubscriber, signBitSubsriber};
}

module.exports.stopADS = (mainWindow, requester, posSubscriber, signBitSubsriber) => {
    ads.stopMonitor(requester, posSubscriber, signBitSubsriber, (error) => {
        if(error) {
        // ez nem törénhet meg, különben nagy bajok vannak
            console.error("Error in stopMonitor", error);
        } else {
            mainWindow.send("ads-disconnected");
        }
    });
}

module.exports.chooseInHoistToGroup = (requester, hoist, groupID, sendResult) => {
    ads.sendGroupRequestToServer(requester, "group", hoist, groupID, (resp) => {
        if(resp === -1) {
            //reject("could not choose in");
            sendResult(-1);
        } else if(resp) {
            //reject("Error occured in the chooseHoistToGroup");
            sendResult(-1);
        } else {
            //resolve(0);
            sendResult(0);
        }
    }); 
}
