let zmq = require("zeromq");

module.exports.initConnection = () => {

    //initate all the connections and return the requester and subscription
    let requester = zmq.socket("req"); // for the commands
    let posSubscriber = zmq.socket("sub"); // subscription for the actPos values
    let signBitSubsriber = zmq.socket("sub");

    console.log("Connect to the Publisher and the Responder endpoints!");
    try {
        requester.connect("tcp://localhost:5555");
        posSubscriber.connect("tcp://localhost:5556");
        signBitSubsriber.connect("tcp://localhost:5557");
    } catch(error) {
        console.error("Error occured during the connection! ", error);
        return {"error": error};
    }

    return {
        "posSubscriber": posSubscriber,
        "requester": requester,
        "signBitSubsriber": signBitSubsriber
    };
}

//The main subscribe for the specific messgae at this point

module.exports.startMonitor = (requester, callback) => {
    // Start the monitor of the variables on the PLC  
    console.log("Sending start request");
    requester.send("start");

    evaluateReply(requester, (error) => {
        if(error) {
            console.error(error);
            callback(error);
        }
        callback(0);
    });

}

module.exports.stopMonitor = (requester, posSubscriber, signBitSubsriber, callback) => {
    // send stop message and close all the connections
    console.log("Sending stop request!");
    requester.send("stopp");
    posSubscriber.removeListener("message");
    signBitSubsriber.removeListener("message");
    evaluateReply(requester, error => {
        if(error) {
            console.error(error);
            callback(error);
        } else { // everything wad fine
            requester.close();
            posSubscriber.close();
            signBitSubsriber.close();
        }
        callback(0);
    });
}

module.exports.sendGroupRequestToServer = (requester, reqMsg, hoist, groupID, callback) => {
    console.log("send req to server ")
    requester.send(reqMsg);
    
    evaluateReply(requester, error => {
        if(error) {
            console.error(error);
            callback(error);
        } else {
            sendBody(requester, hoist, error => {
                if(error) {
                    callback(-1);
                } else {
                    sendBody(requester, groupID, error => {
                        if(error) {
                            callback(-1);
                        } else {
                            callback(0);
                        }
                    });
                }
            });
        }
    });
}

function sendBody (requester, msg, callback) {
    requester.send(msg);
    if(requester.listenerCount("message")) { //if there is a hanging listener becasue of the timedOut message
        requester.removeAllListeners("message"); 
    }
    requester.once("message", replyBuffer => {
        let reply = String.fromCharCode(replyBuffer[0], replyBuffer[1]);
        if(reply === "ok") {
            callback(0);
        } else if(reply === "na"){
            callback(-1);
        }
    });
}

//convert the hex buffer content to signed dec numbers
module.exports.hexToBytes = (hex) => { 
    //console.log("buf: ", hex)
    //console.time("hex")
    for (var bytes = [], c = 0; c < hex.length; c += 8) // start from the 2 to cut down the firs byte, it is kinda dummy
    bytes.push(-(~parseInt(hex.substr(c, 8), 16)+1));
    //console.timeEnd("hex")
    return bytes;
}

let evaluateReply = (requester, callback) => {
    requester.once("message", (replyBuff) => {
        let reply = String.fromCharCode(replyBuff[0], replyBuff[1])
        console.log("reply:", reply)
        if (reply.includes("ok")) {
            callback(0);
        } else if (reply === "er") {
            let error = "Error occured in the server!";
            callback(error);
        } else if (reply === "uk") {
            let error = "Server didnt understand the message!";
            callback(error);
        } else {
            let error = "Unknown reply from the server";
            callback(error);    
        }
    });
}
