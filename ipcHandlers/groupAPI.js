const groupCache = require("./cache/groupCache");
const playCache = require("./cache/playCache");
const deadCache = require("./cache/deadCache");
const { ipcMain } = require("electron");
const storageHandler = require("../tools/storage/storageHandler");

class GroupAPI {
    constructor(mainWindow) {
        this.mainWindow = mainWindow;
        this.setListeners = this.setListeners.bind(this);
        this.getGroups = this.getGroups.bind(this);
        this.saveGroups = this.saveGroups.bind(this);
        this.deleteGroup = this.deleteGroup.bind(this);
        this.checkHoistConflict = this.checkHoistConflict.bind(this);
        // this.setJoyToGroup = this.setJoyToGroup.bind(this);
    }

    setListeners() {
        ipcMain.on("get-groups-for-play", () => {
            this.getGroups();
        });
        ipcMain.on("save-groups-for-play", (event, group, groupToUpdate) => {
            this.saveGroups(group, groupToUpdate);
        });
        ipcMain.on("delete-group-from-play", (event, groupID) => {
            this.deleteGroup(groupID);
        });
        ipcMain.on("check-hoist-conflict", (event, hoistID) => {
            this.checkHoistConflict(hoistID);
        });
        ipcMain.on("set-joystick-group", (event, sceneIndex, groupIndex, joystick) => {
            this.setJoyToGroup(sceneIndex, groupIndex, joystick);
        });
    }

    getGroups() {
        storageHandler.readFile(playCache.get().name, "groups", (groups, error) => {
            if (error) {
                this.mainWindow.send("groups-for-play-result", error);
            } else {
                groupCache.set(groups);
                this.mainWindow.send("groups-for-play-result", groups);
            }
        });
    }

    saveGroups(newGroup, groupToUpdate) {
        let globalGroups = groupCache.get();
        if (groupToUpdate) {
            //if we just update a groups
            //TODO also update in the play config
            globalGroups.groups = globalGroups.groups.map(currGroup => {
                if (currGroup.id === groupToUpdate) {
                    return newGroup;
                } else {
                    return currGroup;
                }
            });
            this.updatePlay(newGroup, groupToUpdate, error => {
                if (error) {
                    console.error("couldnt update the play, after the update of the group")
                    this.mainWindow.send("save-groups-for-play-result");
                }
            });
        } else {
            globalGroups.groups.push(newGroup);
        }

        storageHandler.saveGroups(playCache.get().name, globalGroups, (error) => {
            if (error) {
                this.mainWindow.send("save-groups-for-play-result");
            } else {
                groupCache.set(globalGroups);
                this.mainWindow.send("save-groups-for-play-result", globalGroups);
            }
        });
    }

    deleteGroup(groupID) {
        this.deleteDeadsOfGroup(groupID, (error) => {
            if (error) {
                this.mainWindow.send("delete-group-from-play-result", error);
            }
        });
        this.deleteFromPlay(groupID, error => {
            if (error) {
                this.mainWindow.send("delete-group-from-play-result", error);
            }
        });
        let globalGroups = groupCache.get();
        globalGroups.groups = globalGroups.groups.filter(group => {
            return group.id !== groupID;
        });
        storageHandler.updateFile(playCache.get().name, globalGroups, "groups", error => {
            if (error) {
                this.mainWindow.send("delete-group-from-play-result", error);
            } else {
                groupCache.set(globalGroups);
                this.mainWindow.send("delete-group-from-play-result", null, globalGroups, deadCache.get());
            }
        });
    }

    deleteDeadsOfGroup(groupID, callback) {
        let globalGroups = groupCache.get();
        let globalDeads = deadCache.get();
        let deadsToDelete = globalGroups.groups.find(group => group.id === groupID).deads;
        globalDeads = globalDeads.filter(dead => {
            let found = true;
            deadsToDelete.forEach(deadToDelete => {
                if (dead.id === deadToDelete.id) {
                    found = false;
                }
            });
            return found;
        });
        storageHandler.updateFile(playCache.get().name, globalDeads, "deads", error => {
            if (error) {
                callback(error);
            } else {
                deadCache.set(globalDeads);
                callback(null);
            }
        });
    }

    updatePlay(newGroup, groupToUpdate, callback) {
        let globalPlay = playCache.get();
        // also update in the play description
        globalPlay.scenes = globalPlay.scenes.map(scene => {
            let sceneToReturn = scene;
            // filter out the group we want to delete
            sceneToReturn.groups = scene.groups.map(group => {
                if (group.id === groupToUpdate) {
                    // take its deads, to use them to delete those too from the scene
                    return newGroup;
                } else {
                    return group;
                }
            });
            return sceneToReturn;
        });
        storageHandler.updatePlay(globalPlay, error => {
            if (error) {
                callback(error);
            } else {
                playCache.set(globalPlay);
                callback(null);
            }
        });
    }

    //Delet the group and the containing dead from the play too
    deleteFromPlay(groupID, callback) {
        let globalPlay = playCache.get();
        //TODO also delete delete fom the play
        globalPlay.scenes = globalPlay.scenes.map(scene => {
            let deadsOfGroupToDelete = [];
            let sceneToReturn = scene;
            // filter out the group we want to delete
            sceneToReturn.groups = scene.groups.filter(group => {
                if (group.id === groupID) {
                    // take its deads, to use them to delete those too from the scene
                    deadsOfGroupToDelete = group.deads;
                    return false;
                } else {
                    return true;
                }
            });
            // delete the deads that were belonged to the deleted group
            if (deadsOfGroupToDelete.length) {
                sceneToReturn.deads = scene.deads.filter(dead => {
                    for (let deadIndex in deadsOfGroupToDelete) {
                        if (deadsOfGroupToDelete[deadIndex].id !== dead.id) {
                            return true;
                        } else {
                            return false;
                        }
                    }
                });
            }
            return sceneToReturn;
        });
        storageHandler.updatePlay(globalPlay, error => {
            if (error) {
                callback(error);
            } else {
                playCache.set(globalPlay);
                callback(null);
            }
        });
    }

    checkHoistConflict(hoistID) {
        let globalGroups = groupCache.get();

        //list the groups that contain the selected hoist
        let conflictedGroups = globalGroups.groups.filter(group => {
            return ( // if we find hoist with same id return the true
                group.hoists.find(hoist => hoist.id === hoistID) ?
                    true :
                    false
            );
        });
        conflictedGroups = conflictedGroups.map(group => {
            return {
                "name": group.name,
                "id": group.id
            };
        });
        this.mainWindow.send(`check-hoist-conflict-result-${hoistID}`, null, conflictedGroups);
    }

    setJoyToGroup(sceneIndex, groupIndex, joystick) {
        //ADS finish ads here
        this.mainWindow.send("set-joystick-group-resp");
    }
}

module.exports = GroupAPI;