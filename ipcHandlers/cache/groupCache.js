//NOTE the sorce of the ide of caching this way:
//https://stackoverflow.com/questions/12957821/global-variable-vs-require-cache-in-nodejs
module.exports = function groupCache() {
    let cache = {};
    return {
        get: function () { return cache; },
        set: function (val) { cache = val; }
    }
}();
