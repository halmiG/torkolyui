const playCache = require("./cache/playCache");
const groupCache = require("./cache/groupCache");
const deadCache = require("./cache/deadCache");
const { ipcMain } = require("electron");
const storageHandler = require("../tools/storage/storageHandler");
const moment = require("moment");

class PlayAPI {
    constructor(mainWindow) {
        this.mainWindow = mainWindow;
        this.listPlays = this.listPlays.bind(this);
        this.addPlay = this.addPlay.bind(this);
        this.deletePlay = this.deletePlay.bind(this);
        this.setListeners = this.setListeners.bind(this);
        this.readPlay = this.readPlay.bind(this);
        this.saveAllSceneData = this.saveAllSceneData.bind(this);
        this.getNumberOfScenes = this.getNumberOfScenes.bind(this);
        this.getAllPlayData = this.getAllPlayData.bind(this);
    }

    setListeners() {
        ipcMain.on("list-plays", () => {
            this.listPlays();
        });
        ipcMain.on("add-play", (event, name) => {
            this.addPlay(name);
        });
        ipcMain.on("delete-play", (event, name) => {
            this.deletePlay(name);
        });
        ipcMain.on("read-play", (event, playName) => {
            this.readPlay(playName);
        });
        ipcMain.on("save-all-scene-data", (event, allSceneData) => {
            this.saveAllSceneData(allSceneData);
        });
        ipcMain.on("clear-cache", () => {
            this.clearCache();
        });
        ipcMain.on("get-scene-number", () => {
            this.getNumberOfScenes();
        });
        ipcMain.on("get-all-scene-data", () => {
            this.getAllPlayData();
        });
    }

    listPlays() {
        storageHandler.readPlayList((error, list) => {
            if (error) {
                this.mainWindow.send("list-plays-result", error);
            } else {
                this.mainWindow.send("list-plays-result", null, list);
            }
        });
    }

    addPlay(name) {
        const createDate = moment().format("YYYY-MMM-DD HH:mm");
        const defaultPlayConfig = {
            "name": name,
            "creatingDate": createDate,
            "scenes": []
        };
        storageHandler.createFile(name, null, defaultPlayConfig, (error) => {
            if (error) {
                this.mainWindow.send("add-play-result", error);
            }
        });
        storageHandler.createFile(name, "groups", { "groups": [] }, (error) => {
            if (error) {
                this.mainWindow.send("add-play-result", error);
            }
        });
        storageHandler.createFile(name, "deads", [], (error) => {
            if (error) {
                this.mainWindow.send("add-play-result", error);
            }
        });
        storageHandler.readPlayList((error, list) => {
            if (error) {
                this.mainWindow.send("add-play-result", error);
            } else {
                const newElem = { "name": name, "created": createDate, "modified": createDate };
                list.push(newElem);
                storageHandler.writePlayList(list, error => {
                    if (error) {
                        this.mainWindow.send("add-play-result", error);
                    } else {
                        this.mainWindow.send("add-play-result");
                    }
                });
            }
        });
    }

    deletePlay(name) {
        storageHandler.readPlayList((error, list) => {
            if (error) {
                this.mainWindow.send("delete-play-result", error);
            } else {
                const playToDelete = list.find((elem, i) => {
                    if (elem.name === name) {
                        list.splice(i, 1);
                        return true;
                    } else {
                        return false;
                    }
                });
                if (playToDelete) {
                    storageHandler.writePlayList(list, error => {
                        if (error) {
                            this.mainWindow.send("delete-play-result", error);
                        } else {
                            storageHandler.deletePlayData(playToDelete.name, (error) => {
                                if (error) {
                                    this.mainWindow.send("delete-play-result", error);
                                } else {
                                    this.mainWindow.send("delete-play-result");
                                }
                            });
                        }
                    });
                } else {
                    this.mainWindow.send("delete-play-result", "Couldn't find the play to be delted!");
                }
            }
        });
    }

    readPlay(playName) {
        storageHandler.readPlay(playName, (play, error) => {
            if (!error) {
                playCache.set(play);
                this.mainWindow.send("play-data-read-result");
            } else {
                this.mainWindow.send("play-data-read-result", error);
            }
        });
    }

    saveAllSceneData(allSceneData) {
        let globalPlay = playCache.get();
        globalPlay.scenes = allSceneData;
        storageHandler.updatePlay(globalPlay, (error) => {
            if (error) {
                console.error("Error in updateing play data");
                this.mainWindow.send("save-all-scene-data-return", error);
            } else {
                this.mainWindow.send("save-all-scene-data-return");
                playCache.set(globalPlay);
            }
        });
    }

    clearCache() {
        deadCache.set([]);
        playCache.set({});
        groupCache.set({ "groups": [] });
    }

    getNumberOfScenes() {
        const globalPlay = playCache.get();
        if (globalPlay) {
            this.mainWindow.send("get-scene-number-result", null, globalPlay.scenes.length);
        } else {
            this.mainWindow.send("get-scene-number-result", "Cache is empty , hence cant read play");
        }
    }

    getAllPlayData() {
        if (playCache.get()) {
            this.mainWindow.send("get-all-scene-data-result", null, playCache.get().scenes);
        } else {
            this.mainWindow.send("get-all-scene-data-result", "Cache is empty , hence cant read play");
        }
    }
}

module.exports = PlayAPI;