const groupCache = require("./cache/groupCache");
const playCache = require("./cache/playCache");
const deadCache = require("./cache/deadCache");
const { ipcMain } = require("electron");
const storageHandler = require("../tools/storage/storageHandler");

class DeadAPI {
    constructor(mainWindow) {
        this.mainWindow = mainWindow;
        this.setListeners = this.setListeners.bind(this);
        this.getDeads = this.getDeads.bind(this);
        this.saveDeads = this.saveDeads.bind(this);
        this.getGroupFromDead = this.getGroupFromDead.bind(this);
    }

    setListeners() {
        ipcMain.on("get-deads", () => {
            this.getDeads();
        });
        ipcMain.on("save-deads", (event, deadsArray) => {
            this.saveDeads(deadsArray);
        });
        ipcMain.on("get-group-from-dead", (event, deadID, sceneDesc) => {
            this.getGroupFromDead(deadID, sceneDesc);
        });
    }

    getDeads() {
        let globalDeads = deadCache.get();
        let playData = playCache.get();
        if (globalDeads.length) { //it is cached
            this.mainWindow.send("get-deads-result", null, globalDeads);
        } else {
            storageHandler.readFile(playData.name, "deads", (deadsArray, error) => {
                if (!error) {
                    //globalDeads = [...deadsArray];
                    deadCache.set(deadsArray);
                    this.mainWindow.send("get-deads-result", null, deadsArray);
                } else {
                    this.mainWindow.send("get-deads-result", error);
                    console.log("hiba a deadek kiolvasásakor: ", error);
                }
            });
        }
    }

    saveDeads(deadsArray) {
        //TODO somehow indicate if is an update, cause have to updat in the play too
        //clear chache first
        //Update dead info in the scenes
        this.updateDeadInScenes(deadsArray, error => {
            if (error) {
                this.mainWindow.send("save-deads-result", error);
            } else {
                this.mainWindow.send("save-deads-result");
            }
        });
        deadCache.set([]);
        // save new deads
        storageHandler.updateFile(playCache.get().name, deadsArray, "deads", error => {
            if (error) {
                this.mainWindow.send("save-deads-result", error);
            } else {
                deadCache.set(deadsArray);
                this.mainWindow.send("save-deads-result", null);
            }
        });
    }

    getGroupFromDead(deadID, sceneDesc) {
        //TODO communicate that with the ADS
        // ADS
        let globalDeads = deadCache.get();
        let globalGroups = groupCache.get();

        let conflictedGroups = [];
        conflictedGroups = this.checkIfHoistsAlreadyInScene(deadID, sceneDesc);

        let chosenDead = globalDeads.find(dead => dead.id === deadID);
        let groupDescReturn = {};
        if (chosenDead) {
            let groupToFind = chosenDead.group.groupID;
            groupDescReturn = globalGroups.groups.find(group => group.id === groupToFind);
        } else {
            console.error("Deads List doesnt contain the chosen group!");
        }
        if (groupDescReturn) {
            this.mainWindow.send("get-group-from-dead-result", null, groupDescReturn, chosenDead, conflictedGroups);
        } else {
            console.error("couldnt find group");
        }
    }

    checkIfHoistsAlreadyInScene(deadID, sceneDesc) {
        let globalDeads = deadCache.get();
        let chosenDead = globalDeads.find(dead => dead.id === deadID);

        let hoistIDs = chosenDead.posHoists.map(hoist => hoist.id);
        let conflictedGroups = sceneDesc.groups.filter(group => {
            let foundHoist = false;
            hoistIDs.forEach(hoistID => {
                if (group.hoists.find(hoist => hoist.id === hoistID)) {
                    foundHoist = true;
                    return; // exit from forEach when it is found
                }
            });
            return foundHoist;
        });
        return conflictedGroups.map(group => ({ "id": group.id, "name": group.name }));
    }

    updateDeadInScenes(newDeadsArray, callback) {
        let globalPlay = playCache.get();
        //TODO what happenes on deleteD?
        globalPlay.scenes = globalPlay.scenes.map(scene => {
            // check if the scene is not empty (New Scene)
            // if it is a new scee so it is empty, we just return it
            if (!Object.keys(scene).length) {
                return scene;
            }
            let sceneToReturn = scene;
            let deletedDeadIndex = null;
            sceneToReturn.deads = scene.deads.map((dead, index) => {
                for (let iNewArray in newDeadsArray) {
                    if (newDeadsArray[iNewArray].id === dead.id) {
                        return newDeadsArray[iNewArray];
                    }
                }
                // could find it in the newDead list, meaning: it is deleted
                //So note the to splice it out after the map
                deletedDeadIndex = index;
                return dead;
            });
            if (deletedDeadIndex !== null) {
                // also delete the corresponding group
                let groupToDelete = scene.deads[deletedDeadIndex].group;
                sceneToReturn.groups = scene.groups.filter(group => group.id !== groupToDelete.groupID);
                //delet the dead 
                sceneToReturn.deads.splice(deletedDeadIndex, 1);
            }
            return sceneToReturn;
        });
        storageHandler.updatePlay(globalPlay, error => {
            if (error) {
                callback(error);
            } else {
                playCache.set(globalPlay);
                callback(null);
            }
        });
    }
}

module.exports = DeadAPI;