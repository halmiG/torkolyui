const { ipcMain } = require("electron");
const storageHandler = require("../tools/storage/storageHandler");

class ServiceAPI {
    constructor(mainWindow) {
        this.mainWindow = mainWindow;
        this.setHoistTarget = this.setHoistTarget.bind(this);
        this.getEndTargets = this.getEndTargets.bind(this);
        this.setJoyToHoist = this.setJoyToHoist.bind(this);
    }

    setListeners() {
        ipcMain.on("init-service", () => {
            storageHandler.initService();
        });
        ipcMain.on("set-hoist-targets", (event, hoistData) => {
            this.setHoistTarget(hoistData);
        });
        ipcMain.on("get-end-targets", () => {
            this.getEndTargets();
        });
        ipcMain.on("set-joystick", (event, hoistID, joystick) => {
            this.setJoyToHoist(hoistID, joystick);
        });
    }

    setHoistTarget(hoistData) {
        storageHandler.serviceRead("hoists", (error, content) => {
            if (error) {
                this.mainWindow.send("set-hoist-target-result", error);
            } else {
                let hoistIndex = null;
                content.find((hoist, i) => {
                    if (hoist.id === hoistData.id) {
                        hoistIndex = i;
                        return true;
                    } else {
                        return false;
                    }
                });
                if (hoistIndex !== null) {
                    content[hoistIndex] = hoistData;
                } else {
                    content.push(hoistData);
                }
                storageHandler.serviceWrite("hoists", content, (error) => {
                    if (error) {
                        this.mainWindow.send("set-hoist-target-result", error);
                    } else {
                        this.mainWindow.send("set-hoist-target-result");
                    }
                });
            }
        });
    }

    getEndTargets() {
        storageHandler.serviceRead("hoists", (error, content) => {
            if (error) {
                this.mainWindow.send("get-end-targets-result", error);
            } else {
                this.mainWindow.send("get-end-targets-result", null, content);
            }
        });
    }

    setJoyToHoist(hoistID, joystick) {
        //TODO make the communication here
        this.mainWindow.send("set-joystick", null);
    }
}

module.exports = ServiceAPI;