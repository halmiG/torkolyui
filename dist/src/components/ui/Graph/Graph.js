"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var React = require("react");
var recharts_1 = require("recharts");
var Graph = /** @class */ (function (_super) {
    __extends(Graph, _super);
    function Graph(props) {
        var _this = _super.call(this, props) || this;
        _this.renderLines = function (lineNumber) {
            var _a = _this.props, isDotted = _a.isDotted, plotData = _a.plotData, colors = _a.colors;
            var currentDataKey = Object.keys(plotData[0])[lineNumber * 2 + 1];
            return (React.createElement(recharts_1.Line, { key: lineNumber, dot: isDotted, type: "monotone", dataKey: currentDataKey, stroke: colors[lineNumber] }));
        };
        return _this;
    }
    Graph.prototype.render = function () {
        var _this = this;
        var _a = this.props, referenceValue = _a.referenceValue, plotData = _a.plotData, xAxisDataKey = _a.xAxisDataKey, xAxisLabels = _a.xAxisLabels;
        var renderLegend = function () {
            return (React.createElement("ul", null, xAxisLabels.map(function (entry, index) { return (React.createElement("li", { key: "item-" + index, className: "legend-items-" + index, style: { textAlign: "center", color: "" + _this.props.colors[index], fontSize: "12px" } }, entry)); })));
        };
        var defaultKey = Object.keys(plotData[0])[0];
        var numberOfGraphLines = Object.keys(plotData[0]).length / 2;
        //const maxXValue = plotData[plotData.length - 1][xAxisDataKey ? xAxisDataKey : defaultKey];
        var lines = [];
        for (var i = 0; i < numberOfGraphLines; i += 1) {
            lines.push(this.renderLines(i));
        }
        return (React.createElement("div", { className: "line-chart" },
            React.createElement(recharts_1.LineChart, { width: 312, height: 200, data: plotData },
                React.createElement(recharts_1.XAxis, { dataKey: xAxisDataKey ? xAxisDataKey : defaultKey, stroke: "gray" }),
                React.createElement(recharts_1.YAxis, { stroke: "gray" }),
                React.createElement(recharts_1.ReferenceLine, { y: referenceValue, stroke: "#727e83", strokeDasharray: "3 3" },
                    React.createElement(recharts_1.Label, { value: referenceValue, offset: 5, position: "left", stroke: "#727e83" })),
                React.createElement(recharts_1.Tooltip, null),
                lines,
                React.createElement(recharts_1.Legend, { content: renderLegend }))));
    };
    Graph.defaultProps = {
        isDotted: false,
        colors: ["#9b58b5", "#46b9f3", "green", "red"],
        referenceValue: null,
        xAxisLabels: [],
        xAxisDataKey: null
    };
    return Graph;
}(React.Component));
exports.default = Graph;
//# sourceMappingURL=Graph.js.map