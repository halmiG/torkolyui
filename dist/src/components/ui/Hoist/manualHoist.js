"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var React = require("react");
var react_1 = require("react");
var connect_1 = require("react-redux/lib/connect/connect");
var hoist_1 = require("./hoist");
var ManualHoist = /** @class */ (function (_super) {
    __extends(ManualHoist, _super);
    function ManualHoist(props) {
        var _this = _super.call(this, props) || this;
        _this.handleChange = function (event) {
            _this.setState({ value: event.target.value });
        };
        _this.handleSubmit = function (event) {
            console.log('A name was submitted:  ', _this.state.value);
            event.preventDefault();
        };
        _this.state = {
            value: null
        };
        return _this;
    }
    ManualHoist.prototype.render = function () {
        return (React.createElement("div", null,
            React.createElement(hoist_1.default, { name: this.props.name }),
            React.createElement("form", { onSubmit: this.handleSubmit },
                React.createElement("label", null,
                    "destPos:",
                    React.createElement("input", { type: "text", value: this.state.value, onChange: this.handleChange })),
                React.createElement("input", { type: "submit", value: "Submit" }))));
    };
    ManualHoist = __decorate([
        connect_1.default(function (store) {
            return {};
        })
    ], ManualHoist);
    return ManualHoist;
}(react_1.Component));
exports.default = ManualHoist;
//# sourceMappingURL=manualHoist.js.map