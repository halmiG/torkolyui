"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var React = require("react");
var react_1 = require("react");
//import style from './DataPage.css'; 
var style = require("./hoistAlarm.css");
var HoistAlarm = /** @class */ (function (_super) {
    __extends(HoistAlarm, _super);
    function HoistAlarm(props) {
        var _this = _super.call(this, props) || this;
        _this.defineStyle = function (severity) {
            if (severity === 1) {
                return style.warnRectangle;
            }
            else if (severity === 2) {
                return style.errRectangle;
            }
            else {
                return style.fatalRectangle;
            }
        };
        _this.state = {};
        return _this;
    }
    HoistAlarm.prototype.render = function () {
        var _a = this.props, name = _a.name, severity = _a.severity;
        var currentStyle = this.defineStyle(severity);
        return (React.createElement("div", { className: currentStyle },
            React.createElement("p", { className: style.name }, name)));
    };
    return HoistAlarm;
}(react_1.Component));
exports.default = HoistAlarm;
//# sourceMappingURL=hoistAlarm.js.map