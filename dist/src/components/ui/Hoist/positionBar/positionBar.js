"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var React = require("react");
var react_1 = require("react");
//import style from './DataPage.css'; 
var style = require("./positionBar.css");
var PositionBar = /** @class */ (function (_super) {
    __extends(PositionBar, _super);
    function PositionBar(props) {
        var _this = _super.call(this, props) || this;
        _this.state = {};
        return _this;
    }
    PositionBar.prototype.render = function () {
        var _a = this.props, actValue = _a.actValue, destPos = _a.destPos;
        return (React.createElement("div", { className: style.base },
            React.createElement("div", { className: style.bar, style: { height: actValue } }),
            destPos && React.createElement("div", { className: style.destMark, style: { bottom: destPos } })));
    };
    return PositionBar;
}(react_1.Component));
exports.default = PositionBar;
//# sourceMappingURL=positionBar.js.map