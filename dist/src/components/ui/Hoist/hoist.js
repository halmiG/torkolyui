"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var React = require("react");
var react_1 = require("react");
var connect_1 = require("react-redux/lib/connect/connect");
var hoistAlarm_1 = require("./alarm/hoistAlarm");
var positionBar_1 = require("./positionBar/positionBar");
//import style from './DataPage.css'; 
var style = require("./hoist.css");
var Hoist = /** @class */ (function (_super) {
    __extends(Hoist, _super);
    function Hoist(props) {
        var _this = _super.call(this, props) || this;
        _this.state = {};
        return _this;
    }
    Hoist.prototype.render = function () {
        var name = this.props.name;
        return (React.createElement("div", { className: style.rectangle },
            React.createElement("p", { className: style.name }, name),
            React.createElement("p", { className: style.actPos }, "3800"),
            React.createElement("div", { className: style.separator }),
            React.createElement("p", { className: style.destPos }, "2400"),
            React.createElement("p", { className: style.maxPos }, "4000"),
            React.createElement("p", { className: style.minPos }, "40"),
            React.createElement(positionBar_1.default, { actValue: 57, destPos: 23 }),
            React.createElement("div", { className: style.warning },
                React.createElement(hoistAlarm_1.default, { severity: 1, name: "XX" }))));
    };
    Hoist = __decorate([
        connect_1.default(function (store) {
            return {};
        })
    ], Hoist);
    return Hoist;
}(react_1.Component));
exports.default = Hoist;
//# sourceMappingURL=hoist.js.map