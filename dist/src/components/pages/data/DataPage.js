"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var React = require("react");
var react_1 = require("react");
var react_router_dom_1 = require("react-router-dom");
var connect_1 = require("react-redux/lib/connect/connect");
var dataAction = require("../../../actions/dataHandlerActions");
//import style from './DataPage.css'; 
var style = require("./DataPage.css");
var Data = /** @class */ (function (_super) {
    __extends(Data, _super);
    function Data(props) {
        var _this = _super.call(this, props) || this;
        _this.readDataHandler = function () {
            console.log('read data handler pushed!');
            //ipcRenderer.send('read-data', 'file1');
            _this.props.dispatch(dataAction.readDataAction("dataToRead"));
            //this.props.actions.readDataAction('dataToRead');
        };
        _this.state = {
            number: null
        };
        return _this;
    }
    Data.prototype.render = function () {
        console.log('it is rendering with:', this.state, this.props);
        return (React.createElement("div", null,
            "Data",
            React.createElement(react_router_dom_1.Link, { to: '/' },
                React.createElement("button", { className: style.baseLink }, "to home")),
            React.createElement(react_router_dom_1.Link, { to: '/test' },
                React.createElement("button", { className: style.testLink }, "to test")),
            React.createElement("div", null,
                React.createElement("button", { onClick: this.readDataHandler }, "Read Data"),
                React.createElement("h2", null, this.props.number))));
    };
    Data = __decorate([
        connect_1.default(function (store) {
            console.log("store", store.dataReducer.number);
            return {
                number: store.dataReducer.number
            };
        })
    ], Data);
    return Data;
}(react_1.Component));
exports.default = Data;
//# sourceMappingURL=DataPage.js.map