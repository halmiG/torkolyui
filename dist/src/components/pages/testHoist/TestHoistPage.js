"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var React = require("react");
var react_1 = require("react");
var connect_1 = require("react-redux/lib/connect/connect");
var manualHoist_1 = require("../../ui/Hoist/manualHoist");
var electron_1 = require("electron");
//import style from './DataPage.css'; 
var style = require("./TestHoistPage.css");
var TestHoist = /** @class */ (function (_super) {
    __extends(TestHoist, _super);
    function TestHoist(props) {
        var _this = _super.call(this, props) || this;
        _this.handleChange = function (event) {
            _this.setState({ value: event.target.value });
        };
        _this.handleConnect = function (event) {
            console.log("connect launched! ", _this.state.value);
            electron_1.ipcRenderer.send("connect-opc", _this.state.value);
            electron_1.ipcRenderer.once("opc-connect-done", function (event, result) {
                console.log("opcConnect result", result);
                _this.setState({ connectResult: result });
            });
        };
        _this.renderHoists = function (count) {
            var hoists = [];
            for (var i = 0; i < 5; i++) {
                hoists.push(React.createElement("li", { className: style.hoistElement },
                    React.createElement(manualHoist_1.default, { key: i, name: i })));
            }
            return hoists;
        };
        _this.state = {
            value: "",
            connectResult: ""
        };
        return _this;
    }
    TestHoist.prototype.render = function () {
        return (React.createElement("div", null,
            React.createElement("h2", null, "Hello Joci!"),
            React.createElement("form", { onSubmit: this.handleConnect },
                React.createElement("label", null,
                    "Server:",
                    React.createElement("input", { type: "text", value: this.state.value, onChange: this.handleChange })),
                React.createElement("input", { type: "submit", value: "Connect" })),
            React.createElement("p", null, this.state.connectResult),
            React.createElement("ul", { className: style.hoistContainer }, this.renderHoists(5))));
    };
    TestHoist = __decorate([
        connect_1.default(function (store) {
            return {};
        })
    ], TestHoist);
    return TestHoist;
}(react_1.Component));
exports.default = TestHoist;
//# sourceMappingURL=TestHoistPage.js.map