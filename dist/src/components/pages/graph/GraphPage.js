"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var React = require("react");
var Graph_1 = require("../../ui/Graph/Graph");
//import "./App.css";
var react_router_dom_1 = require("react-router-dom");
//import  style from './GraphPage.css';
var style = require("./GraphPage.css");
var GraphPage = /** @class */ (function (_super) {
    __extends(GraphPage, _super);
    function GraphPage(props) {
        return _super.call(this, props) || this;
    }
    GraphPage.prototype.render = function () {
        var plotData = [
            { time: 1, pv: 12, x: 12, v: 122 },
            { time: 2, pv: 24, x: 24, v: 2 },
            { time: 3, pv: 13, x: 48, v: 22 },
            { time: 4, pv: 10, x: 64, v: 6 },
            { time: 5, pv: 30, x: 90, v: 10 }
        ];
        console.log('Hello');
        return (React.createElement("div", { className: 'app-body' },
            React.createElement(Graph_1.default, { plotData: plotData }),
            React.createElement("h1", null, "Test"),
            React.createElement(react_router_dom_1.Link, { to: '/data' }, "to data"),
            React.createElement(react_router_dom_1.Link, { to: '/test' },
                React.createElement("button", { className: style.linkButton }, "To Test")),
            React.createElement(react_router_dom_1.Link, { to: '/hoistTest' },
                React.createElement("button", null, "To Hoist"))));
    };
    return GraphPage;
}(React.Component));
exports.default = GraphPage;
//# sourceMappingURL=GraphPage.js.map