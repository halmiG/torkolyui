"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var redux_1 = require("redux");
var redux_immutable_state_invariant_1 = require("redux-immutable-state-invariant");
var redux_thunk_1 = require("redux-thunk");
//import createHistory from 'history/createBrowserHistory';
// 'routerMiddleware': the new way of storing route changes with redux middleware since rrV4.
var react_router_redux_1 = require("react-router-redux");
var reducers_1 = require("../reducers");
var history_1 = require("history");
var redux_devtools_extension_1 = require("redux-devtools-extension");
var redux_promise_middleware_1 = require("redux-promise-middleware");
exports.history = history_1.createHashHistory();
function configureStore() {
    var reactRouterMiddleware = react_router_redux_1.routerMiddleware(exports.history);
    var middlewares = [
        // Add other middleware on this line...
        // thunk middleware can also accept an extra argument to be passed to each thunk action
        // https://github.com/gaearon/redux-thunk#injecting-a-custom-argument
        redux_thunk_1.default,
        reactRouterMiddleware,
        redux_immutable_state_invariant_1.default(),
        redux_promise_middleware_1.default()
        // TODO what about the hot modules? 
    ];
    var store = redux_1.createStore(reducers_1.default, redux_devtools_extension_1.composeWithDevTools(redux_1.applyMiddleware.apply(void 0, middlewares)));
    return store;
}
exports.configureStore = configureStore;
// function configureStoreProd(initialState) {
//   const reactRouterMiddleware = routerMiddleware(history);
//   const middlewares = [
//     // Add other middleware on this line...
//     // thunk middleware can also accept an extra argument to be passed to each thunk action
//     // https://github.com/gaearon/redux-thunk#injecting-a-custom-argument
//     thunk,
//     reactRouterMiddleware,
//     reduxImmutableStateInvariant(),
//   ];
//   return createStore(rootReducer, initialState, compose(
//     applyMiddleware(...middlewares)
//     )
//   );
// }
// function configureStoreDev(initialState) {
//   const reactRouterMiddleware = routerMiddleware(history);
//   const middlewares = [
//     // Add other middleware on this line...
//     // Redux middleware that spits an error on you when you try to mutate your state either inside a dispatch or between dispatches.
//     reduxImmutableStateInvariant(),
//     // thunk middleware can also accept an extra argument to be passed to each thunk action
//     // https://github.com/gaearon/redux-thunk#injecting-a-custom-argument
//     thunk,
//     reactRouterMiddleware,
//   ];
//   const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose; // add support for Redux dev tools
//   const store = createStore(rootReducer, initialState, composeEnhancers(
//     applyMiddleware(...middlewares)
//     )
//   );
//   if (module.hot) {
//     // Enable Webpack hot module replacement for reducers
//     module.hot.accept('../reducers', () => {
//       const nextReducer = require('../reducers').default; // eslint-disable-line global-require
//       store.replaceReducer(nextReducer);
//     });
//   }
//   return store;
// }
// export const configureStore = process.env.NODE_ENV === 'production' ? configureStoreProd : configureStoreDev;
//# sourceMappingURL=configureStore.js.map