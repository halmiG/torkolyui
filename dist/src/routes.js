"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
/* eslint flowtype-errors/show-errors: 0 */
var React = require("react");
var react_router_1 = require("react-router");
var App_1 = require("./components/App");
var GraphPage_1 = require("./components/pages/graph/GraphPage");
var DataPage_1 = require("./components/pages/data/DataPage");
var TestPage_1 = require("./components/pages/test/TestPage");
var TestHoistPage_1 = require("./components/pages/testHoist/TestHoistPage");
exports.default = (function () { return (React.createElement(App_1.default, null,
    React.createElement(react_router_1.Switch, null,
        React.createElement(react_router_1.Route, { exact: true, path: '/', component: GraphPage_1.default }),
        React.createElement(react_router_1.Route, { path: '/data', component: DataPage_1.default }),
        React.createElement(react_router_1.Route, { path: '/test', component: TestPage_1.default }),
        React.createElement(react_router_1.Route, { path: '/hoistTest', component: TestHoistPage_1.default })))); });
//# sourceMappingURL=routes.js.map