"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var types = require("./actionTypes");
var electron_1 = require("electron");
//import ipcPromise from 'ipc-promise';
//Event listener way to read the file 
exports.readDataAction = function (dataName) { return function (dispatch) {
    console.log('actionis dipatched');
    electron_1.ipcRenderer.send('read-data', 'file1');
    electron_1.ipcRenderer.once('gotcha', function (event, data) {
        console.log('gotcha action', data);
        dispatch({ type: types.READ_DATA, data: data });
    });
    dispatch({ type: types.READ_DATA_LOADING });
}; };
// this si the pormise way to do it
// export const readDataAction = (dataName) => dispatch => {
//     console.log('actionis dipatched');
//     dispatch({type: types.READ_DATA_LOADING});
//     ipcPromise.send('read-data', dataName).then( function(data) {
//         console.log('gotcha action:', data);
//         dispatch({type: types.READ_DATA, data});
//     }).catch( error=> {
//         throw(error);
//     }); 
// };
//# sourceMappingURL=dataHandlerActions.js.map