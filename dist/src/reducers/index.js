"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var redux_1 = require("redux");
var dataHandlerReducer_1 = require("./dataHandlerReducer");
var rootReducer = redux_1.combineReducers({
    dataReducer: dataHandlerReducer_1.default
});
exports.default = rootReducer;
//# sourceMappingURL=index.js.map