"use strict";
var __assign = (this && this.__assign) || Object.assign || function(t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
        s = arguments[i];
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
            t[p] = s[p];
    }
    return t;
};
Object.defineProperty(exports, "__esModule", { value: true });
var types = require("../actions/actionTypes");
var defaultState = {
    number: null
};
function dataHandlerReducer(state, action) {
    if (state === void 0) { state = defaultState; }
    switch (action.type) {
        case types.READ_DATA:
            console.log('gotcha reducer', state, action);
            return __assign({}, state, { number: action.data.number });
        // return [...state,
        //         Object.assign({}, action.data)];
        case types.READ_DATA_LOADING:
            console.log('reading loading in reducer');
            return state;
        default:
            return state;
    }
}
exports.default = dataHandlerReducer;
//# sourceMappingURL=dataHandlerReducer.js.map