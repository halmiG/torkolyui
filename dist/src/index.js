"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var React = require("react");
var ReactDOM = require("react-dom");
//import { HashRouter as Router, Route } from 'react-router-dom';
var configureStore_1 = require("./store/configureStore");
var react_redux_1 = require("react-redux");
var react_router_redux_1 = require("react-router-redux");
var routes_1 = require("./routes");
require("typeface-roboto/index.css");
var store = configureStore_1.configureStore();
ReactDOM.render(React.createElement(react_redux_1.Provider, { store: store },
    React.createElement(react_router_redux_1.ConnectedRouter, { history: configureStore_1.history },
        React.createElement(routes_1.default, null))), document.getElementById('root'));
//# sourceMappingURL=index.js.map