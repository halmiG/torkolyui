var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var _this = this;
var electron = require("electron");
var ipcMain = require("electron").ipcMain;
var ipcPromise = require("ipc-promise");
var storageHandler = require("./tools/storageHandler");
var opcuaFunc = require("./tools/OPCUA/baseFunctions");
//type in the consol to enable devtron: 
//require('devtron').install()
var app = electron.app, BrowserWindow = electron.BrowserWindow;
var mainWindow;
var opcuaSession;
app.on("ready", function () { return __awaiter(_this, void 0, void 0, function () {
    return __generator(this, function (_a) {
        mainWindow = new BrowserWindow({ show: false, width: 1920, height: 1080 });
        mainWindow.loadURL("file://" + __dirname + "/src/index.html"); // How would this look like on Windows ??? SAME! so it is not an open question anymore I think
        //mainWindow.loadURL(`file://${__dirname}/dist/index.html`); // How would this look like on Windows ??? 
        console.log("app ready");
        mainWindow.once("ready-to-show", function () {
            console.log("app si ready to show!!!!!!!!!");
            mainWindow.show();
        });
        return [2 /*return*/];
    });
}); });
ipcMain.on("connect-opc", function (event, addr) { return __awaiter(_this, void 0, void 0, function () {
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                console.log("opc connect");
                return [4 /*yield*/, opcuaFunc.initialize(addr)
                        .catch(function (error) {
                        console.log("couldnt initialize the server");
                        console.log("ERROR in connect", error);
                    })];
            case 1:
                opcuaSession = _a.sent();
                mainWindow.send("opc-connect-done", "Connected!");
                return [2 /*return*/];
        }
    });
}); });
ipcMain.on("read-data", function (event, arg) {
    //console.log('data-reaevent is cought', arg, event);
    //event.returnValue = 'gotcha';
    storageHandler.getDataFromFile(function (data) {
        mainWindow.send("gotcha", data);
    });
    storageHandler.writeNewData();
    //setTimeout(() => {mainWindow.send('gotcha', 'read: ' + arg);}, 10000);
});
//# sourceMappingURL=index.js.map