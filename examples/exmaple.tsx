// import * as React from "react"
// import * as Table from "reactabular-table"
// import * as resolve from "table-resolver"
// import * as sort from "sortabular"
// import { orderBy } from "lodash"

// import "./index.css"
// import CheckBox from "../input/checkBox/index"

// export class CheckableListItem {
//     id: string | number // unique identifier 
//     checked: boolean // internal, part of selection
//     constructor(id: string | number, checked: boolean) {
//         this.id = id
//         this.checked = checked
//     }
// }

// export interface CheckableListProps {
//     rows?: CheckableListItem[]
//     columns: any[]
//     onSelectionChange?: (selection: Array<string | number>) => void
//     onRowClick?: (row: CheckableListItem) => void
//     checkable?: boolean
//     container?: any
//     sticky?: boolean
//     id?: string
//     customSortAction?: (ascending: boolean, column: string) => void
//     preventDefault?: boolean
//     initialSortingColumn?: {
//         [columnName: string]: {
//             direction: string, // asc or desc
//             position: number
//         }
//     }
// }

// interface CheckableListState {
//     sortingColumns: any
//     sortableTransformer: any,
//     sortiableFormatter: any
// }

// export default class CheckableList extends React.Component<CheckableListProps, CheckableListState> {
//     public static defaultProps: Partial<CheckableListProps> = {
//         preventDefault: true
//     }

//     tableBody: Table.Body
//     tableHeader: Table.Header
//     tableFixedHeader: Table.Header
//     tablePlaceholderHeader: HTMLElement
//     tableReferenceRow: any


//     constructor(props: CheckableListProps) {
//         super(props)

//         const getSortingColumns = () => this.state.sortingColumns || {}
//         const sortable = sort.sort({
//             getSortingColumns,
//             onSort: selectedColumn => {
//                 const sortingColumns = sort.byColumn({
//                     sortingColumns: this.state.sortingColumns,
//                     selectedColumn
//                 })
//                 this.setState({ sortingColumns })
//                 if (this.props.customSortAction) {
//                     if (Object.keys(sortingColumns).length > 0) {
//                         const key = Object.keys(sortingColumns)[0]
//                         this.props.customSortAction(sortingColumns[key].direction === "asc", key)
//                     } else {
//                         this.props.customSortAction(false, "")
//                     }
//                 }
//             },
//             strategy: sort.strategies.byProperty
//         })
//         const sortableFormatter = sort.header({ sortable, getSortingColumns })

//         this.state = {
//             sortingColumns: props.initialSortingColumn || null,
//             sortableTransformer: sortable,
//             sortiableFormatter: sortableFormatter
//         }
//     }

//     onRowClick(row: CheckableListItem) {
//         if (this.props.onRowClick) {
//             this.props.onRowClick(row)
//         }
//     }

//     onAllSelected(checked: boolean) {
//         let item = this.props.rows
//         item.forEach(a => {
//             a.checked = checked
//         })

//         if (this.props.onSelectionChange) {
//             this.props.onSelectionChange(checked ? this.props.rows.map(a => a.id) : [])
//         }
//     }

//     onAnySelected(checked: boolean, id: string | number) {

//         this.props.rows.filter(v => v.id === id)[0].checked = checked

//         if (this.props.onSelectionChange) {
//             var selection = this.props.rows.filter(i => i.checked).map(i => i.id)
//             this.props.onSelectionChange(selection)
//         }
//     }

//     componentDidMount() {
//         this.forceUpdate()
//         let ref = this
//         window.onresize = () => ref.calculateHeader()
//         if (this.props.container != null) {
//             this.props.container.onscroll = () => ref.calculateHeader()
//         }
//         this.calculateHeader()
//     }

//     componentDidUpdate() {
//         this.calculateHeader()
//     }

//     render() {
//         var { columns, rows } = this.props
//         var { sortableTransformer, sortiableFormatter, sortingColumns } = this.state
        
//         var selected = this.props.rows.filter(i => !i.checked).length
//         const checkboxRow = {
//             property: "checked",
//             header: {
//                 formatters: [
//                     value => {
//                         return (
//                             <CheckBox
//                                 checked={selected === 0 && this.props.rows.length > 0}
//                                 indeterminate={selected > 0 && selected < this.props.rows.length}
//                                 onChange={isChecked => this.onAllSelected(isChecked)}
//                                 style={CheckBox.Style.Light}
//                             />
//                         )
//                     }
//                 ]
//             },
//             cell: {
//                 formatters: [
//                     (value, { rowData }) => {
//                         return (
//                             <CheckBox
//                                 checked={value}
//                                 style={CheckBox.Style.Light}
//                             />
//                         )
//                     }
//                 ]
//             }
//         }

//         var sortableColumns = [...columns].map((entry) => {
//             const col = {...entry, header: {...entry.header}}
//             if (col.header != null) {
//                 if (!col.header.transforms || col.header.transforms.filter(t => t.default).length === 0) {
//                     col.header.transforms = [...col.header.transforms || [], sortableTransformer]
//                 }
//                 if (!col.header.formatters || col.header.formatters.filter(f => f.default).length === 0) {
//                     col.header.formatters = [...col.header.formatters || [], sortiableFormatter]
//                 }
//             }
//             return col
//         })

//         if (this.props.checkable) {
//             sortableColumns = [checkboxRow, ...sortableColumns]
//         }

//         let resolvedColumns = resolve.columnChildren({ columns: sortableColumns })
//         let resolvedRows = resolve.resolve({
//             columns: sortableColumns,
//             method: resolve.nested
//         })(rows)

//         if (!this.props.customSortAction) {
//             resolvedRows = sort.sorter({ columns: sortableColumns, sortingColumns, sort: orderBy, strategy: sort.strategies.byProperty })(resolvedRows)
//         }

//         return (
//             <div id={this.props.id ? this.props.id : ""} className={["checkableList", this.props.checkable ? "checkable" : ""].join(" ")}>
//                 <Table.Provider columns={resolvedColumns} >
//                     <Table.Header
//                         className="reactabular-table-header"
//                         style={this.props.sticky ? { visibility: "hidden" } : {}}
//                         headerRows={resolve.headerRows({ columns: sortableColumns })}
//                         ref={tableHeader => {
//                             this.tableFixedHeader = tableHeader && tableHeader.getRef()
//                         }}
//                     />
//                     {this.props.sticky &&
//                         <Table.Header
//                             className="reactabular-table-header sticky"
//                             headerRows={resolve.headerRows({ columns: sortableColumns })}
//                             ref={tableHeader => {
//                                 this.tableHeader = tableHeader && tableHeader.getRef()
//                             }}
//                         />
//                     }
//                     {this.props.sticky &&
//                         <thead className="reactabular-table-header placeholder">
//                             <tr ref={(tableHeader) => this.tablePlaceholderHeader = tableHeader} />
//                         </thead>
//                     }

//                     <Table.Body
//                         className="reactabular-table-body"
//                         rows={resolvedRows}
//                         rowKey="id"
//                         onRow={(item, row) => this.onRow(item, row)}
//                         ref={tableBody => {
//                             this.tableBody = tableBody && tableBody.getRef()
//                         }}
//                     />
//                 </Table.Provider>
//             </div>
//         )
//     }

//     private onRow(item: CheckableListItem, row: any) {
//         return {
//             onClick: (proxy) => {
//                 if (this.props.preventDefault) {
//                     proxy.preventDefault()
//                 }
//                 if (this.props.checkable) {
//                     this.onAnySelected(!item.checked, item.id)
//                 }
//                 this.onRowClick(item)
//             },
//             className: "checkableListRow " + (this.props.checkable && item.checked ? "checkableListRowSelected" : ""),
//             ref: (ref => this.tableReferenceRow = ref)
//         }
//     }

//     private calculateHeader() {
//         if (this.props.sticky) {
//             if (this.tableReferenceRow) {
//                 Array.from(this.tableHeader.children[0].children).forEach((item: HTMLTableCellElement, i) => {
//                     item.style.width = this.tableReferenceRow.children[i].clientWidth + "px"
//                 })
//             }
//             if (this.props.container && this.tableFixedHeader && this.props.container) {
//                 let fixedHeader = this.tableFixedHeader.getBoundingClientRect()
//                 let container = this.props.container.getBoundingClientRect()
//                 let stickyHeader = this.tableHeader.getBoundingClientRect()

//                 let leftOffset = (fixedHeader.left - container.left)
//                 let topOffset = (fixedHeader.bottom - container.top)
//                 let viewableWidth = (container.right - fixedHeader.left) - (this.props.container.offsetWidth - this.props.container.clientWidth)

//                 this.tableHeader.style.position = topOffset <= 0 ? "absolute" : "static"
//                 this.tableHeader.style.left = leftOffset + "px"
//                 this.tableHeader.style.width = viewableWidth + "px"
//                 this.tablePlaceholderHeader.style.height = topOffset <= 0 ? stickyHeader.height + "px" : "0px"
//             }
//         }
//     }
// }

